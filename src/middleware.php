<?php
require __DIR__ . '/../public/configuration/keys.php';

// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new Tuupola\Middleware\JwtAuthentication([
    "attribute" => "jwt",
    "secret" => keys(),
    "algorithm" => ["HS256", "HS384"],
    "relaxed" => ["localhost","api-pmn.eshardproject.com"],
]));
