<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$jenis_tagihan 	= R::getAll("SELECT * FROM `jenis_tagihan` ORDER BY `id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($jenis_tagihan);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$jenis_tagihan				= R::xdispense( 'jenis_tagihan' );
		$jenis_tagihan->name 		= $param['name'];
		$id					= R::store( $jenis_tagihan );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$jenis_tagihan 				= R::load( 'jenis_tagihan', $param['id'] );
		$jenis_tagihan->name 		= $param['name'];
		$id 						= R::store( $jenis_tagihan );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();