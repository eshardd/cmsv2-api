<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$principle = R::getAll("SELECT a.*,b.`name` AS `country_name` FROM `principle` a LEFT JOIN `country` b ON a.`id_country` = b.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($principle);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$principle					= R::xdispense( 'principle' );
		$principle->id_country 		= $param['country'];
		$principle->name			= $param['name'];
		$id							= R::store( $principle );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'principle List has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$principle 					= R::load( 'principle', $param['id'] );
		$principle->id_country 		= $param['country'];
		$principle->name			= $param['name'];
		$id 						= R::store( $principle );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'principle List Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$country = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `country`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($country);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->run();