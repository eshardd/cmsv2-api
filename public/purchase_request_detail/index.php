<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/list/{id}", function (Request $request, Response $response , $args){
	try {
		$id = $args['id'];
		$purchase_request 	= R::getRow("SELECT a.*,b.`name` AS `created_by_name`,DATE_FORMAT(a.`date`,'%d %M %Y') as `date` FROM `purchase_request` a
		LEFT JOIN `user` b ON a.`created_by` = b.`id` WHERE a.`id` = $id");

		$purchase_request_detail = R::getAll("SELECT a.*,d.`id` AS `id_product`,d.`id_principle`,b.`no_pr`,c.`name` AS `product_detail_name`,d.`name` AS `product_name`, c.`code` AS `code` FROM `purchase_request_detail` a
		LEFT JOIN `purchase_request` b ON a.`id_pr` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id`
		WHERE a.`id_pr` = $id");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(
			array('purchase_request'=>$purchase_request,'purchase_request_detail'=>$purchase_request_detail)
		);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'purchase_request_detail', $param['id'] );
		$data->id_product_detail	= $param['id_product_detail'];
		$data->quantity				= $param['quantity'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/log/{id}", function (Request $request, Response $response , $args){
	try {
		$id = $args['id'];
		$purchase_receive 		= R::getAll("SELECT a.* FROM `purchase_receive` a WHERE a.`id_purchase_request` = $id");
	
		$new_arr = array();
		foreach ($purchase_receive as $key => $value) {
			$new_arr[$key]['data'] = $value;
			$id_purchase_receive = $value['id'];
			$purchase_receive 		= R::getAll("SELECT a.* FROM `purchase_receive_detail` a WHERE a.`id_purchase_receive` = $id_purchase_receive");
			foreach ($purchase_receive as $key_ => $value_) {
				$new_arr[$key]['detail'][$key_] = $value_;
			}
		}
		print_r($new_arr);
		die();
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(
			array('purchase_request'=>$purchase_request,'purchase_request_detail'=>$purchase_request_detail)
		);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();