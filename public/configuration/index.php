<?php
date_default_timezone_set("Asia/Jakarta");
//define('HOST', 'http://localhost/api-scheduler/public/');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/../../thirdparty/rb.php';

include 'configuration-database.php';
include 'email_template.php';

$curl = new Curl\Curl();
$app = new \Slim\App([
	'settings' => [
		'determineRouteBeforeAppMiddleware' => true
	]
]);

$app->add(function ($req, $res, $next) {
	$response = $next($req, $res);
	return $response->withHeader('Access-Control-Allow-Origin', '*')
	->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
	->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

function sendmail($toEmail, $toName, $subject, $messages) {
	$mail = new PHPMailer(true);
	try {
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->Username = "hermaneshard@gmail.com";
		$mail->Password = "iwokvdkcgkncojmo";
		$mail->SetFrom('hermaneshard@gmail.com', 'Herman Eshard');
		$mail->FromName = "PMN Logistic System";
		$mail->AddAddress($toEmail, $toName);
		$mail->Subject = $subject;
		$mail->Body = $messages;
		$mail->IsHTML (true);
		$mail->Send();
	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}

function sendmailCC($toEmail, $addCC , $toName, $subject, $messages) {
	$mail = new PHPMailer(true);
	try {
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->Username = "hermaneshard@gmail.com";
		$mail->Password = "iwokvdkcgkncojmo";
		$mail->SetFrom('hermaneshard@gmail.com', 'Herman Eshard');
		$mail->FromName = "PMN Logistic System";
		$mail->AddAddress($toEmail, $toName);
		foreach($addCC as $email => $name){
			$mail->AddCC($email, $name);
		}
		$mail->Subject = $subject;
		$mail->Body = $messages;
		$mail->IsHTML (true);
		$mail->Send();
	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}