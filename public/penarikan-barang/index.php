<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response, $args){
	try {
		$spb = R::getAll("SELECT
		a.*,
		b.`no_spb`,
		c.`name` AS `courier_name`,
		d.`name` AS `created_by_name`,
		CONCAT(e.`name`, ' - ', e.`nomor_polisi`) AS `kendaraan`,
		f.`no_ro` as `no_ro`,
		f.`id` as `id_ro`,
		g.`name` AS `doctor_name`,
		h.`name` AS `hospital_name`
		FROM `penarikan_barang` a
		LEFT JOIN `spb` b ON a.`id_spb` = b.`id`
		LEFT JOIN `user` c ON a.`courier` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `kendaraan` e ON a.`no_kendaraan` = e.`id`
		LEFT JOIN `rencana_operasi` f ON b.`id_ro` = f.`id`
		LEFT JOIN `doctor` g ON f.`id_doctor` = g.`id`
		LEFT JOIN `hospital` h ON f.`id_hospital` = h.`id`
		ORDER BY f.`no_ro` DESC
		");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($spb);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$rencana_operasi = R::getRow("SELECT a.*,
		b.`name` AS `doctor_name`,
		c.`name` AS `hospital_name`,
		d.`name` AS `spine_anatomi_name`,
		e.`name` AS `subcase_name`,
		f.`name` AS `case_name`,
		i.`name` AS `jenis_tagihan_name`,
		j.`name` AS `user_name`,
		k.`name` AS `box_status_name`,
		l.`name` AS `status_ro_name`,
		m.`name` AS `type_ro_name`,
		n.`name` AS `technical_support_name`,
		o.`id` AS `id_spb`
	FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
		LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
		LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
		LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
		LEFT JOIN `user` j ON a.`created_by` = j.`id`
		LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
		LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
		LEFT JOIN `user` n ON a.`technical_support` = n.`id`
		LEFT JOIN `spb` o ON o.`id_ro` = a.`id`
	WHERE a.`id` = $ro_id
		GROUP BY a.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$spb 		= R::getAll("SELECT a.`id` AS `value`, a.`no_spb` AS `label` FROM `spb` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		WHERE b.`stage` = 6
		AND a.`id` NOT IN (SELECT `id_spb` FROM `penarikan_barang`)");
		$rencana_operasi = R::getAll("SELECT `id` AS `value`, `no_ro` AS `label` FROM `rencana_operasi` WHERE `stage` = 6");
		$spb_edit	= R::getAll("SELECT `id` AS `value`, `no_spb` AS `label` FROM `spb` WHERE `id` IN (SELECT `id_spb` FROM `penarikan_barang`)");
		$kurir 		= R::getAll("SELECT `id` AS `value`, `name` AS `label` FROM `user` WHERE `division` IN('5','3')");
		$kendaraan 	= R::getAll("SELECT `id` AS `value`, CONCAT(`nomor_polisi`,' - ',`name`) AS `label` FROM `kendaraan`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'spb' => $spb, 
			'ro' => $rencana_operasi, 
			'spb_edit' => $spb_edit, 
			'kurir' => $kurir,
			'kendaraan' => $kendaraan
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/get_no_ro/{id}", function (Request $request, Response $response,$args){
	try {
		$id_spb 	= $args['id'];
		$spb 		= R::getRow("SELECT * FROM `spb` WHERE `id` = $id_spb");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($spb);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/info/{id}", function (Request $request, Response $response, $args){
	try {
		$spb_id = $args['id'];
		$rencana_operasi = R::getRow("SELECT a.*,
		b.`name` AS `doctor_name`,
		c.`name` AS `hospital_name`,
		d.`name` AS `spine_anatomi_name`,
		e.`name` AS `subcase_name`,
		f.`name` AS `case_name`,
		i.`name` AS `jenis_tagihan_name`,
		j.`name` AS `user_name`,
		k.`name` AS `box_status_name`,
		l.`name` AS `status_ro_name`,
		m.`name` AS `type_ro_name`,
		n.`name` AS `technical_support_name`
	FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
		LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
		LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
		LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
		LEFT JOIN `user` j ON a.`created_by` = j.`id`
		LEFT JOIN `spb` o ON a.`id` = o.`id_ro`
		LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
		LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
		LEFT JOIN `user` n ON a.`technical_support` = n.`id`
		WHERE o.`id` = $spb_id
				GROUP BY a.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();

		$data						= R::xdispense( 'penarikan_barang' );
		$data->id_spb				= $param['id_spb'];
		$data->courier				= $param['kurir'];
		$data->no_kendaraan			= $param['no_kendaraan'];
		$data->etd					= date('Y-m-d H:i:s', strtotime($param['etd']));
		$data->eta					= date('Y-m-d H:i:s', strtotime($param['eta']));
		$data->created_date			= date("Y-m-d H:i:s");
		$data->created_by			= $param['id_user'];
		$id = R::store( $data );

		if($id){
			// log activity
			$getdata_spb = R::getRow("SELECT a.*,b.`name` AS `courier_name`,
			CONCAT(c.`name`,' - ',c.`nomor_polisi`) AS `kendaraan`,
			DATE_FORMAT(a.`etd`,'%W %M %e %Y %h:%i:%s') AS `estimasi_keberangkatan`,
			DATE_FORMAT(a.`eta`,'%W %M %e %Y %h:%i:%s') AS `estimasi_tiba`
			FROM `penarikan_barang` a 
			LEFT JOIN `user` b ON a.`courier` = b.`id`
			LEFT JOIN `kendaraan` c ON a.`no_kendaraan` = c.`id`
			WHERE a.`id` = $id");


			$log_text = "";
			$log_text .= "Kurir : <strong>".$getdata_spb['courier_name']."</strong><br/>";
			$log_text .= "Kendaraan : <strong>".$getdata_spb['kendaraan']."</strong><br/>";
			$log_text .= "Estimasi Keberangkatan : <strong>".$getdata_spb['estimasi_keberangkatan']."</strong><br/>";
			$log_text .= "Estimasi Waktu Tiba : <strong>".$getdata_spb['estimasi_tiba']."</strong>";

			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id_ro'];
			$log->user					= $id_user;
			$log->title					= '[Add Penarikan]';
			$log->log					= "Start Penarikan barang";
			$log->log_comment			= $log_text;
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );

			// log activity
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id_ro'];
			$log->user					= 0;
			$log->title					= '[Set Stage]';
			$log->log					= "Rencana Operasi Progress 'Retracting'";
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );


			// data email
			$param =  R::getRow("SELECT
			a.*,
			b.`no_spb`,
			c.`name` AS `courier_name`,
			d.`name` AS `created_by_name`,
			CONCAT(e.`name`, ' - ', e.`nomor_polisi`) AS `kendaraan`,
			f.`no_ro` AS `no_ro`,
			f.`id` AS `id_ro`,
			g.`name` AS `doctor_name`,
			h.`name` AS `hospital_name`
			FROM `penarikan_barang` a
			LEFT JOIN `spb` b ON a.`id_spb` = b.`id`
			LEFT JOIN `user` c ON a.`courier` = c.`id`
			LEFT JOIN `user` d ON a.`created_by` = d.`id`
			LEFT JOIN `kendaraan` e ON a.`no_kendaraan` = e.`id`
			LEFT JOIN `rencana_operasi` f ON b.`id_ro` = f.`id`
			LEFT JOIN `doctor` g ON f.`id_doctor` = g.`id`
			LEFT JOIN `hospital` h ON f.`id_hospital` = h.`id`
			WHERE a.`id` = $id");
		
			// tambah receipent
			$receipnt =  R::getAll("SELECT * FROM `user` WHERE `division` IN ('3','5')");
			$addCC = array();
			$receipnt_email = array();
			foreach ($receipnt as $key => $value) {
				$addCC[$value['email']] = $value['name'];
				$receipnt_email[] = $value['email'];
			}
			$receipnt_log_text = implode(', ',$receipnt_email);

			sendmailCC('prpebri@gmail.com', $addCC , 'Pebri', '[Logistic System] Penarikan barang', template_email_spb('Pebri' ,'prpebri@gmail.com', $param));
		}

		

		$rencana_operasi 			= R::load( 'rencana_operasi', $param['id_ro'] );
		$rencana_operasi->stage		= 7;
		$id 						= R::store( $rencana_operasi );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'penarikan_barang', $param['id'] );
		$data->id_spb				= $param['id_spb'];
		$data->courier				= $param['kurir'];
		$data->no_kendaraan			= $param['no_kendaraan'];
		$data->etd					= date('Y-m-d H:i:s', strtotime($param['etd']));
		$data->eta					= date('Y-m-d H:i:s', strtotime($param['eta']));
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/done', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'penarikan_barang', $param['id'] );
		$data->atd 					= date("Y-m-d H:i:s");
		$data->done 				= 1;
		$id 						= R::store( $data );


		$id_spb = $data['id_spb'];
		$cekspb = R::getRow("SELECT `id_ro` FROM `spb` WHERE `id` = $id_spb");
		$id_ro = $cekspb['id_ro'];

		$rencana_operasi 			= R::load( 'rencana_operasi', $id_ro );
		if($rencana_operasi['integrated']==2){
			$rencana_operasi->stage		= 10;
			$rencana_operasi->progress	= 100;
		}else{
			$rencana_operasi->stage		= 8;
		}
		$id 						= R::store( $rencana_operasi );

		// log activity
		$id_user 					= $param['id_user'];
		$log						= R::xdispense( 'rencana_operasi_log' );
		$log->id_rencana_operasi	= $cekspb['id_ro'];
		$log->user					= $id_user;
		$log->title					= '[Update Penarikan]';
		$log->log					= "Update Box Returned to Office";
		$log->log_comment			= '';
		$log->date					= date("Y-m-d H:i:s");
		$id_log						= R::store( $log );

		// log activity
		$id_user 					= $param['id_user'];
		$log						= R::xdispense( 'rencana_operasi_log' );
		$log->id_rencana_operasi	= $cekspb['id_ro'];
		$log->user					= 0;
		$log->title					= '[Set Stage]';
		$log->log					= "Rencana Operasi Progress 'Returned' And 'Done'";
		$log->log_comment			= '';
		$log->date					= date("Y-m-d H:i:s");
		$id_log						= R::store( $log );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();