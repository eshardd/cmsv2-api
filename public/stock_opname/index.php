<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$warehouse_stock = R::getAll("SELECT a.*, b.`name` AS `warehouse_name`,
		c.`id_product` AS `id_product`,
		c.`name` AS `product_detail_name`,
		c.`code` AS `product_detail_code`,
        d.`name` AS `product_name`,
        e.`name` AS `principle_name`
		FROM `warehouse_stock` a 
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id` 
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
        LEFT JOIN `product` d ON d.`id` = c.`id_product`
        LEFT JOIN `principle` e ON d.`id_principle` = e.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($warehouse_stock);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/result/{id_so}", function (Request $request, Response $response, $args){
	$id_so = $args['id_so'];
	try {
		// $data = R::getAll("SELECT DISTINCT(a.`product_detail`),c.`name` AS `product_detail_name`,b.`id` AS `product_id`,b.`name` AS `product_name` FROM `warehouse_stock_opname_detail` a
		// LEFT JOIN `product` b ON a.`product` = b.`id`
		// LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id`
		// WHERE a.`id_so` = 1");

		$data = R::getAll("SELECT a.*,(a.`stock_data`+a.`stock_box`) AS `total_stock_data`,((a.`stock_data`+a.`stock_box`)- a.`stock_actual`) AS `selisih` ,
		b.`no_so` AS `no_so`,
		c.`name` AS `product_name`,
		d.`name` AS `product_detail_name`
		FROM `warehouse_stock_opname_detail` a
		LEFT JOIN `warehouse_stock_opname` b ON a.`id_so` = b.`id`
		LEFT JOIN `product` c ON a.`product` = c.`id`
		LEFT JOIN `product_detail` d ON a.`product_detail` = d.`id`
		WHERE ((a.`stock_data`+a.`stock_box`)- a.`stock_actual`) > 0
		AND a.`id_so` = $id_so");

		// $new_arr = array();

		// foreach ($data as $key => $value) {
		// 	$new_arr[$value['product_id']][] = $value;
		// }

		// print_r($new_arr);

		// die();
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		//throw $th;
	}
	
	// try {
	// 	$data = R::getAll("SELECT a.*,c.`name` as `product_name`,d.`name` as `product_detail_name`,
	// 	(a.`stock_data`+a.`stock_box`) AS `sumary`,
	// 	IF((a.`stock_data`+a.`stock_box`) > a.`stock_actual`,'Minus','Kaga') as `calc`,
	// 	((a.`stock_data`+a.`stock_box`) - a.`stock_actual`) as `selisih`
	// 	from  `warehouse_stock_opname_detail` a
	// 	left join `warehouse_stock_opname` b on a.`id_so` = b.`id`
	// 	left join `product` c on a.`product` = c.`id`
	// 	left join `product_detail` d on a.`product_detail` = d.`id`
	// 	where ((a.`stock_data`+a.`stock_box`) - a.`stock_actual`) > 0");
	// 	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	// } catch (Exception $e) {
	// 	return $response->withStatus(400)->write($e->getMessage());
	// }
});

$app->get("/list_so", function (Request $request, Response $response){
	try {
		$data = R::getAll("SELECT a.*, c.`name` AS `created_by_name` FROM `warehouse_stock_opname` a
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id`
		LEFT JOIN `user` c ON a.`created_by` = c.`id`
		WHERE a.`status` = 1");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/order/{id}", function (Request $request, Response $response, $args){
	try {
		$id_so = $args['id'];
		$data = R::getRow("SELECT a.*,b.`name` AS `warehouse_name` FROM `warehouse_stock_opname` a LEFT JOIN `warehouse` b on a.`id_warehouse` = b.`id` WHERE a.`id` = $id_so");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/wh/{warehouse}/{id_so}", function (Request $request, Response $response, $args){
	try {
		$id_warehouse = $args['warehouse'];
		$id_so = $args['id_so'];
		$product_detail = R::getAll("SELECT a.*, b.`name` AS `product_name`,c.`name` AS `principle_name`,
		(SELECT (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = a.`id` AND `id_warehouse` = $id_warehouse) AS `sisa`,
		(SELECT SUM(aa.`quantity`) FROM `box_detail` aa LEFT JOIN `box` bb ON aa.`id_box` = bb.`id` LEFT JOIN `warehouse` cc ON bb.`id_warehouse` = cc.`id` WHERE bb.`id_warehouse` = $id_warehouse AND aa.`id_product_detail` = a.`id`) AS `inbox`,
		(SELECT `stock_actual` FROM `warehouse_stock_opname_detail` WHERE `product_detail` = a.`id` AND `id_so` = $id_so) AS `qty`
		FROM `product_detail` a
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `principle` c ON b.`id_principle` = c.`id`");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param								= $request->getParsedBody();
		$id_so 								= $param['id_so'];
		$list_data 							= $param['data'];

		foreach ($list_data as $key => $value) {
			if(isset($value['qty']) && $value['qty'] > 0){

				$cekdata = R::findOne( 'warehouse_stock_opname_detail', ' product_detail = :id_product_detail AND id_so = :id_sot ', [ ':id_product_detail' => $value['id'] , ':id_sot' => $id_so  ] );

				if($cekdata != NULL){
					$cekdata->stock_data 	= $value['sisa'];
					$cekdata->stock_box 	= $value['inbox'];
					$cekdata->stock_actual 	= $value['qty'];
					$update 				= R::store( $cekdata );
				}else{
					$data					= R::xdispense( 'warehouse_stock_opname_detail' );
					$data->id_so 			= $id_so;
					$data->product		 	= $value['id_product'];
					$data->product_detail 	= $value['id'];
					$data->stock_data 		= $value['sisa'];
					$data->stock_box 		= $value['inbox'];
					$data->stock_actual 	= $value['qty'];
					$insert					= R::store( $data );
				}
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/integrate', function (Request $request, Response $response){
	try{

		// penyesuaian stok
		$param							= $request->getParsedBody();
		$id_so = $param['id'];
		$data = R::getAll("SELECT a.*, ((a.`stock_data` + a.`stock_box`) - a.`stock_actual`) AS `selisih`
		FROM `warehouse_stock_opname_detail` a
		WHERE ((a.`stock_data`+a.`stock_box`)- a.`stock_actual`) > 0 
		AND a.`id_so` = $id_so");
		
		$id_warehouse = $param['id_warehouse'];

		foreach ($data as $key => $value) {
			$data						= R::xdispense( 'warehouse_stock' );
			$data->id_warehouse			= $id_warehouse;
			$data->id_product_detail	= $value['product_detail'];
			$data->out					= $value['selisih'];
			$data->created_date			= date("Y-m-d H:i:s");
			$data->description			= "Penyesuaian SO : ".$param['no_so']; 
			$id							= R::store( $data );
		}

		// update replacement
		$data							= R::load( 'warehouse_stock_opname', $param['id'] );
		$data->integrated				= 1;
		$id								= R::store( $data );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();