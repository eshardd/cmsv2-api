<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$rencana_operasi = R::getAll("SELECT a.*, 
		b.`name` AS `doctor_name`, 
		c.`name` AS `hospital_name`, 
		d.`name` AS `status_ro_name`,
		e.`name` AS `created_by_name`,
		f.`name` AS `stage_name`,
		DATE_FORMAT(a.`date`,'%d-%m-%Y') AS `datenow`
		FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `rencana_operasi_status` d ON a.`status_ro` = d.`id`
		LEFT JOIN `user` e ON a.`created_by` = e.`id`
		LEFT JOIN `rencana_operasi_stage` f ON a.`stage` = f.`id`
		ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/filterdate', function (Request $request, Response $response){
	$post				= $request->getParsedBody();
	
	$sql_date = '';
	$sql_month = '';
	$sql_year = '';

	if (isset($post['date']) && $post['date'] != null) {
		$date = str_pad($post['date'], 2, 0, STR_PAD_LEFT);
		$sql_date = 'AND day(a.`date`) = '.$date;
	}
	
	if (isset($post['month']) && $post['month'] != null) {
		$month = str_pad($post['month'], 2, 0, STR_PAD_LEFT);
		$sql_month = 'AND month(a.`date`) = '.$month;
	}
	
	if (isset($post['year']) && $post['year'] != null) {
		$year = $post['year'];
		$sql_year = 'AND YEAR(a.`date`) = '.$year;
	}

	try {
		$rencana_operasi = R::getAll("SELECT a.*, 
		b.`name` AS `doctor_name`, 
		c.`name` AS `hospital_name`, 
		d.`name` AS `status_ro_name`,
		e.`name` AS `created_by_name`,
		f.`name` AS `stage_name`,
		DATE_FORMAT(a.`date`,'%d %b %Y') AS `datenow`
		FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `rencana_operasi_status` d ON a.`status_ro` = d.`id`
		LEFT JOIN `user` e ON a.`created_by` = e.`id`
		LEFT JOIN `rencana_operasi_stage` f ON a.`stage` = f.`id`
		WHERE 1=1 $sql_date $sql_month $sql_year
		ORDER BY a.`id` DESC");


		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$rencana_operasi = R::getRow("SELECT a.*,
				b.`name` AS `doctor_name`,
				c.`name` AS `hospital_name`,
				d.`name` AS `spine_anatomi_name`,
				e.`name` AS `subcase_name`,
				f.`name` AS `case_name`,
				i.`name` AS `jenis_tagihan_name`,
				j.`name` AS `user_name`,
	            k.`name` AS `box_status_name`,
	            l.`name` AS `status_ro_name`,
	            m.`name` AS `type_ro_name`,
	            n.`name` AS `technical_support_name`
			FROM `rencana_operasi` a
				LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
				LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
				LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
				LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
				LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
				LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
				LEFT JOIN `user` j ON a.`created_by` = j.`id`
	            LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
	            LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
	            LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
	            LEFT JOIN `user` n ON a.`technical_support` = n.`id`
			WHERE a.`id` = $ro_id
				GROUP BY a.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/comment/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$comment = R::getAll("SELECT a.*, b.`name`, b.`image`  FROM `rencana_operasi_comment` a LEFT JOIN user b on a.`id_user` = b.`id` WHERE a.`id_rencana_operasi` = $ro_id  ORDER BY a.`datetime` DESC");
		$log = R::getAll("SELECT a.*,IF(b.`name`!= '', b.`name`, '[SYSTEM]') AS `user_name` FROM `rencana_operasi_log` a
		LEFT JOIN `user` b ON a.`user` = b.`id`
		WHERE `id_rencana_operasi` = $ro_id ORDER BY `id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('comment'=>$comment,'log'=>$log));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `rencana_operasi`");
		if ($ai['id'] == null) {$new_ai = 1;}else{$new_ai = $ai['id']+1;}

		$post				= $request->getParsedBody();		
		$param 				= $post['ro'];
		$product 			= $post['product'];
		$alat_pendukung 	= $post['alat_pendukung'];
		
		// untuk log
		$id_user = $param['id_user'];
		$data_user = R::getRow("SELECT `name` FROM `user` WHERE `id` = $id_user");

		$rencana_operasi						= R::xdispense( 'rencana_operasi' );
		$rencana_operasi->no_ro					= 'PMN.RO/'.date('Ymd').'/'.$new_ai;
		$rencana_operasi->date					= date('Y-m-d H:i:s', strtotime($param['date']));
		$rencana_operasi->id_doctor				= $param['doctor'];
		$rencana_operasi->id_hospital			= $param['hospital'];
		$rencana_operasi->pasien				= $param['pasien'];
		$rencana_operasi->no_mr					= $param['no_mr'];
		$rencana_operasi->id_spine_anatomi		= $param['spine_anatomi'];
		$rencana_operasi->case					= $param['case'];
		$rencana_operasi->subcase				= $param['subcase'];
		$rencana_operasi->remarks				= $param['remarks'];
		$rencana_operasi->jenis_tagihan			= $param['jenis_tagihan'];
		$rencana_operasi->created_by			= $param['id_user'];
		$rencana_operasi->created_date			= date("Y-m-d H:i:s");
		$rencana_operasi->last_update			= date("Y-m-d H:i:s");
		$rencana_operasi->type_ro				= $param['type_ro'];
		$rencana_operasi->status_ro				= 1;
		$rencana_operasi->stage					= 1;
		$rencana_operasi->progress				= 10;
		$id										= R::store( $rencana_operasi );


		if ($id) {
			foreach ($product as $key => $value) {
				$rencana_operasi_prod						= R::xdispense( 'rencana_operasi_detail' );
				$rencana_operasi_prod->id_ro				= $id;

				// cek ada box stay nggak disana, kalo ada box stay. pake yang disana
				$id_hospital = $param['hospital'];
				$id_product_detail = $value['product_detail'];
				$list_box_stay = R::getRow("SELECT * FROM `box` WHERE `stay` = $id_hospital AND `product` = $id_product_detail");
				if($list_box_stay){
					$rencana_operasi_prod->id_box			= $list_box_stay['id'];
				}

				$rencana_operasi_prod->id_product			= $value['product_detail'];
				$prod										= R::store( $rencana_operasi_prod );
			}
			foreach ($alat_pendukung as $key => $value) {
				$rencana_operasi_alat_pendukung						= R::xdispense( 'rencana_operasi_alat_pendukung' );
				$rencana_operasi_alat_pendukung->id_ro				= $id;
				$rencana_operasi_alat_pendukung->id_product_detail	= $value['product_detail'];
				$rencana_operasi_alat_pendukung->quantity			= $value['quantity'];
				$prod												= R::store( $rencana_operasi_alat_pendukung );
			}
		}

		// log activity
		if($id){
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $id;
			$log->user					= $id_user;
			$log->title					= '[Created]';
			$log->log					= "Create Rencana Operasi No : ".$rencana_operasi->no_ro;
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		}

		// data email
		$param =  R::getRow("SELECT a.`id`, DATE_FORMAT(a.`date`, '%d %M %Y') AS `date`, d.`name` AS `hospital_name`, b.`name` AS `request_by`, a.`pasien`, e.`name` AS `anatomi`, (SELECT GROUP_CONCAT(bb.`name`) FROM `rencana_operasi_detail` aa LEFT JOIN `product` bb ON aa.`id_product` = bb.`id` WHERE aa.`id_ro` = $id) AS `request_instrument_name`, f.`name` AS `case`, g.`name` AS `subcase` FROM `rencana_operasi` a  LEFT JOIN `user` b ON a.`created_by` = b.`id` LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id` LEFT JOIN `hospital` d ON a.`id_hospital` = d.`id` LEFT JOIN `spine_anatomi` e ON a.`id_spine_anatomi` = e.`id` LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id` LEFT JOIN `diagnosa_subcase` g ON a.`subcase` = g.`id`  WHERE a.`id` = $id");
	
		// tambah receipent
		$receipnt =  R::getAll("SELECT * FROM `user` WHERE `division` IN ('3','5')");
		$addCC = array();
		$receipnt_email = array();
		foreach ($receipnt as $key => $value) {
			$addCC[$value['email']] = $value['name'];
			$receipnt_email[] = $value['email'];
		}
		$receipnt_log_text = implode(', ',$receipnt_email);


		//sendmailCC('eshardd@gmail.com', $addCC , 'Eshardd', '[Logistic System] Rencana Operasi', template_email_rencana_operasi('Eshardd' ,'Eshardd@gmail.com', $param));
		
		// log activity
		if($id){
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $id;
			$log->user					= 0;
			$log->title					= '[Send Email]';
			$log->log					= "Email Sent";
			$log->log_comment			= 'Email Notification Sent to : <br/>'.$receipnt_log_text;
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/comment', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$comment						= R::xdispense( 'rencana_operasi_comment' );
		$comment->id_rencana_operasi	= $param['id'];
		$comment->id_user				= $param['id_user'];
		$comment->message				= $param['message'];
		$comment->datetime				= date("Y-m-d H:i:s");
		$id								= R::store( $comment );

		// log activity
		if($id){
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id'];
			$log->user					= $id_user;
			$log->title					= '[Comment]';
			$log->log					= "Add new comment";
			$log->log_comment			= $comment->message;
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param									= $request->getParsedBody();
		$rencana_operasi 						= R::load( 'rencana_operasi', $param['id'] );
		$rencana_operasi->date					= date("Y-m-d H:i:s",strtotime($param['date']));
		$rencana_operasi->id_doctor				= $param['doctor'];
		$rencana_operasi->id_hospital			= $param['hospital'];
		$rencana_operasi->pasien				= $param['pasien'];
		$rencana_operasi->no_mr					= $param['no_mr'];
		$rencana_operasi->id_spine_anatomi		= $param['spine_anatomi'];
		$rencana_operasi->case					= $param['case'];
		$rencana_operasi->subcase				= $param['subcase'];
		$rencana_operasi->remarks				= $param['remarks'];
		$rencana_operasi->jenis_tagihan			= $param['jenis_tagihan'];
		if($param['status_box']){
			if ($rencana_operasi->status_box == null) {
				$rencana_operasi->stage				= 2;
				
				// log activity
				$id_user 					= $param['id_user'];
				$id_box_status 				= $param['status_box'];
				$data_status_box 			= R::getRow("SELECT `name` FROM `box_status` WHERE `id` = $id_box_status");
				$log						= R::xdispense( 'rencana_operasi_log' );
				$log->id_rencana_operasi	= $param['id'];
				$log->user					= $id_user;
				$log->title					= '[Set Status Box]';
				$log->log					= "Set Rencana Operasi Box into : ".$data_status_box['name'];
				$log->log_comment			= '';
				$log->date					= date("Y-m-d H:i:s");
				$id_log						= R::store( $log );

				// log activity
				$id_user 					= $param['id_user'];
				$log						= R::xdispense( 'rencana_operasi_log' );
				$log->id_rencana_operasi	= $param['id'];
				$log->user					= 0;
				$log->title					= '[Set Stage]';
				$log->log					= "Rencana Operasi 'Waiting for Checklist'";
				$log->log_comment			= '';
				$log->date					= date("Y-m-d H:i:s");
				$id_log						= R::store( $log );
			}else{
				if($rencana_operasi->status_box != $param['status_box']){

					// log activity
					$id_user 					= $param['id_user'];
					$id_box_status 				= $param['status_box'];
					$data_status_box 			= R::getRow("SELECT `name` FROM `box_status` WHERE `id` = $id_box_status");
					$log						= R::xdispense( 'rencana_operasi_log' );
					$log->id_rencana_operasi	= $param['id'];
					$log->user					= $id_user;
					$log->title					= '[Set Status Box]';
					$log->log					= "Update Rencana Operasi Box into : ".$data_status_box['name'];
					$log->log_comment			= '';
					$log->date					= date("Y-m-d H:i:s");
					$id_log						= R::store( $log );
				}
			}
			$rencana_operasi->status_box		= $param['status_box'];
		}
		if($rencana_operasi->status_ro != $param['status_ro']){
			
			// log activity
			$id_user 					= $param['id_user'];
			$id_status_ro				= $param['status_ro'];
			$data_ro_status 			= R::getRow("SELECT `name` FROM `rencana_operasi_status` WHERE `id` = $id_status_ro");
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id'];
			$log->user					= $id_user;
			$log->title					= '[Change Status Rencana Operasi]';
			$log->log					= "Change Rencana Operasi Status into : '".$data_ro_status['name']."'" ;
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
			
			$rencana_operasi->status_ro				= $param['status_ro'];
		}

		if($rencana_operasi->technical_support != $param['ts']){
			// log activity
			$id_user 					= $param['id_user'];
			$id_ts 						= $param['ts'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			
			if($param['ts'] == 0){
				$log->log					= "Update Rencana Operasi Into : 'Non Technical Support'" ;
			}else{
				$data_ts 					= R::getRow("SELECT `name` FROM `user` WHERE `id` = $id_ts");
				$log->log					= "Assign : '".$data_ts['name']."' As Technical Support" ;
			}
			$log->id_rencana_operasi	= $param['id'];
			$log->user					= $id_user;
			$log->title					= '[Assign Technical Support]';
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		}
		$rencana_operasi->technical_support		= $param['ts'];

		$rencana_operasi->last_update			= date("Y-m-d H:i:s");
		$id 									= R::store( $rencana_operasi );

		// data email
		//$param_email =  R::getRow("SELECT a.`id`, DATE_FORMAT(a.`date`, '%d %M %Y') AS `date`, d.`name` AS `hospital_name`, b.`name` AS `request_by`, a.`pasien`, e.`name` AS `anatomi`, (SELECT GROUP_CONCAT(bb.`name`) FROM `rencana_operasi_detail` aa LEFT JOIN `product` bb ON aa.`id_product` = bb.`id` WHERE aa.`id_ro` = $id) AS `request_instrument_name`, f.`name` AS `case`, g.`name` AS `subcase` FROM `rencana_operasi` a  LEFT JOIN `user` b ON a.`created_by` = b.`id` LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id` LEFT JOIN `hospital` d ON a.`id_hospital` = d.`id` LEFT JOIN `spine_anatomi` e ON a.`id_spine_anatomi` = e.`id` LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id` LEFT JOIN `diagnosa_subcase` g ON a.`subcase` = g.`id`  WHERE a.`id` = $id");

		if($param['status_box']){
			//sendmail('eshardd@gmail.com', 'Suherman', '[Logistic System] Rencana Operasi', template_email_rencana_operasi('Suherman' ,'eshardd@gmail.com', $param_email));
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$user 					= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user`");
		$hospital 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `hospital`");
		$doctor 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `doctor`");
		$spine_anatomi 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `spine_anatomi`");
		$case 					= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `diagnosa_case`");
		$subcase 				= R::getAll("SELECT `id` AS `value`, `name` as `label`,`id_case` FROM `diagnosa_subcase`");
		$jenis_tagihan 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `jenis_tagihan`");
		$product 				= R::getAll("SELECT a.`id` AS `value`, CONCAT(b.`name`,' - ', a.`name`) AS `label` FROM `product` a LEFT JOIN `principle` b ON a.`id_principle` = b.`id` WHERE a.`id` NOT IN ('5','6')");
		//$alat_pendukung 		= R::getALL("SELECT a.`id` AS `value`, a.`name` AS `label` FROM `product_detail` a WHERE a.`id_product` = 1");
		$alat_pendukung 		= R::getALL("SELECT a.`id` AS `value`, a.`name` AS `label` FROM `product_detail` a WHERE a.`id_product` in('32','5','6')");
		$ts_list				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user` where `division` = 6");
		$status_ro 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `rencana_operasi_status`");
		$rencana_operasi_type 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `rencana_operasi_type`");
		$rencana_operasi_stage 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `rencana_operasi_stage`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'user'=>$user,
			'hospital'=>$hospital,
			'doctor'=>$doctor,
			'spine_anatomi'=>$spine_anatomi,
			'case'=>$case,
			'subcase'=>$subcase,
			'jenis_tagihan'=>$jenis_tagihan,
			'product'=>$product,
			'ts_list'=>$ts_list,
			'status_ro'=>$status_ro,
			'ro_type'=>$rencana_operasi_type,
			'alat_pendukung'=>$alat_pendukung,
			'stage'=>$rencana_operasi_stage,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_product_list', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_product 				= $param['id_product'];
		$id_ro 						= $param['ro'];
		$cek_duplicate				= R::exec( 'SELECT * FROM `rencana_operasi_detail` WHERE `id_product` ='.$id_product.' AND `id_ro` = '.$id_ro );

		if ($cek_duplicate == 0) {
			$data						= R::xdispense( 'rencana_operasi_detail' );
			$data->id_ro				= $param['ro'];
			$data->id_product			= $param['id_product'];
			$id							= R::store( $data );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_alat_pendukung', function (Request $request, Response $response){
	try{

		$param						= $request->getParsedBody();
		$id_product_detail 			= $param['id_product_detail'];
		$quantity 					= $param['quantity'];
		$id_ro 						= $param['ro'];
		$cek_duplicate				= R::exec( 'SELECT * FROM `rencana_operasi_alat_pendukung` WHERE `id_product_detail` ='.$id_product_detail.' AND `id_ro` = '.$id_ro );

		if ($cek_duplicate == 0) {
			$data						= R::xdispense( 'rencana_operasi_alat_pendukung' );
			$data->id_ro				= $param['ro'];
			$data->id_product_detail	= $param['id_product_detail'];
			$data->quantity				= $param['quantity'];
			$id							= R::store( $data );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_list/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$data = R::getAll("SELECT a.*,b.`no_ro`,c.`name` AS `product_name`, d.`code_box`, e.`name` AS `principle_name` FROM `rencana_operasi_detail` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product` c ON a.`id_product` = c.`id`
		LEFT JOIN `box` d ON a.`id_box` = d.`id`
		LEFT JOIN `principle` e ON c.`id_principle` = e.`id`
		WHERE a.`id_ro` = $ro_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_detail_list/{id}", function (Request $request, Response $response,$args){
	try {
		$id_product_detail  = $args['id'];
		$product_detail 	= R::getRow("SELECT a.*,b.`name` AS `product_name`, c.`name` AS `type_name`,d.`name` AS `principle_name` FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id` 
		LEFT JOIN `product_type` c ON a.`type` = c.`id`
		LEFT JOIN `principle` d ON b.`id_principle` = d.`id` 
		WHERE a.`id` = $id_product_detail ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/alat_pendukung_list/{id}", function (Request $request, Response $response,$args){
	try {
		$id_ro  = $args['id'];
		$alat_pendukung 	= R::getAll("SELECT a.*,b.`no_ro`,c.`name` AS `product_detail_name` FROM `rencana_operasi_alat_pendukung` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		WHERE a.`id_ro` = $id_ro");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($alat_pendukung);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/detail/{id}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
		$box 	= R::load( 'rencana_operasi_detail', $id );
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->delete('/alat_pendukung/{id}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
		$box 	= R::load( 'rencana_operasi_alat_pendukung', $id );
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});
$app->run();