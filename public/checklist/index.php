<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response, $args){
	try {
		$box = R::getAll("SELECT a.*, b.`code_box`, c.`name` FROM `box_topup` a
		LEFT JOIN `box` b on a.`id_box` =  b.`id`
		LEFT JOIN `user` c on a.`created_by` = c.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($box);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$list = R::getAll("SELECT a.*,c.`name` as `product_detail_name`,c.`type`,c.`code` FROM `box_topup_detail` a
		LEFT JOIN `box_topup` b on a.`id_box_topup` = b.`id`
		LEFT JOIN `product_detail` c on a.`id_product_detail` = c.`id`
		where a.`id_box_topup` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($list);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/assign_box', function (Request $request, Response $response){
	try{
		$param					= $request->getParsedBody();
		$data 					= R::load( 'rencana_operasi_detail', $param['id'] );
		$data->id_box 			= $param['id_box'];
		$id 					= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();