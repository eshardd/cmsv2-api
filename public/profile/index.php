<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/user/{id}", function (Request $request, Response $response, $args){
	$user_id = $args['id'];
	try {
		$user 	= R::getRow("SELECT * FROM `user` where `id` = $user_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($user);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$profile 					= R::load( 'user', $param['id'] );
		$profile->name 				= $param['name'];
		$profile->username 			= $param['username'];
		$profile->email 			= $param['email'];
		$profile->aboutme 			= $param['aboutme'];
		$profile->address 			= $param['address'];
		if(!empty($param['image'])){
			$profile->image 			= $param['image'];
		}
		$id 						= R::store( $profile );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->put('/edit_password', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$user_id 					= $param['id'];

		$profile 					= R::load( 'user', $user_id );
		
		if($param['password'] == $param['confirmation']){
			$profile->password 			= password_hash($param['password'], PASSWORD_BCRYPT, array('cost' => 10));
			$id 						= R::store( $profile );
			$user 	= R::getRow("SELECT * FROM `user` where `id` = $user_id");
			sendmail($user['email'], $user['name'], '[Logistic System] Change Password', template_email_change_password($user['name'] ,$user['email'], $param['password']));
			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Please Check Your Email Address'));
		}else{
			$id = null;
			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Password Not Match'));
		}
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();