<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$diagnosa_case 	= R::getAll("SELECT a.*,
		c.`name` AS `potong_product_name`,
		CONCAT(c.`name`,' - ',b.`name`) AS `potong_product_id_name`,
		e.`name` AS `menjadi_product_name`,
		CONCAT(e.`name`,' - ',d.`name`) AS `menjadi_product_id_name` 
		FROM `log_potong_rod` a 
		LEFT JOIN `product_detail` b ON a.`potong_product_id` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `product_detail` d ON a.`menjadi_product_id` = d.`id`
		LEFT JOIN `product` e ON d.`id_product` = e.`id`
		ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($diagnosa_case);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$product 	= R::getAll("SELECT b.`id` AS `value`,CONCAT(c.`name` ,' - ', b.`name`) AS `label` FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `principle` c ON b.`id_principle` = c.`id`
		WHERE a.`name` LIKE '%Rod%' 
		GROUP BY a.`id_product`
		ORDER BY a.`id_product` ASC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_item/{id_product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$list = R::getAll("SELECT a.`id` as `value` ,a.`name` AS `label` FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		WHERE a.`name` LIKE '%Rod%' AND a.`id_product` = $id_product 
		ORDER BY a.`id_product` ASC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($list);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->post('/add', function (Request $request, Response $response){
	try{

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `log_potong_rod`");
		if ($ai['id'] == null) {$new_ai = 1;}else{$new_ai = $ai['id']+1;}

		$param								= $request->getParsedBody();
		$potong_rod							= R::xdispense( 'log_potong_rod' );
		$potong_rod->no_pr					= 'PMN.PR/'.date('Ymd').'/'.$new_ai;
		$potong_rod->potong_product_id 		= $param['potong_product_id'];
		$potong_rod->menjadi_product_id 	= $param['menjadi_product_id'];
		$potong_rod->potong_quantity 		= $param['quantity_potong'];
		$potong_rod->menjadi_quantity 		= $param['quantity_menjadi'];
		$potong_rod->created_by 			= $param['created_by'];
		$potong_rod->created_date 			= date("Y-m-d H:i:s");
		$id									= R::store( $potong_rod );


		// potong
		$wh_out							= R::xdispense( 'warehouse_stock' );
		$wh_out->id_warehouse 			= 1;
		$wh_out->id_product_detail 		= $param['potong_product_id'];
		$wh_out->in 					= 0;
		$wh_out->out 					= $param['quantity_potong'];
		$wh_out->description 			= "potong Rod";
		$wh_out->created_by 			= $param['created_by'];
		$wh_out->created_date 			= date("Y-m-d H:i:s");
		$post_wh = R::store( $wh_out );

		// menjadi
		$wh_in							= R::xdispense( 'warehouse_stock' );
		$wh_in->id_warehouse 			= 1;
		$wh_in->id_product_detail 		= $param['menjadi_product_id'];
		$wh_in->in 						= $param['quantity_menjadi'];
		$wh_in->out 					= 0;
		$wh_in->description 			= "potong Rod";
		$wh_in->created_by 				= $param['created_by'];
		$wh_in->created_date 			= date("Y-m-d H:i:s");
		$post_wh = R::store( $wh_in );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$diagnosa_case 				= R::load( 'diagnosa_case', $param['id'] );
		$diagnosa_case->name 		= $param['name'];
		$id 						= R::store( $diagnosa_case );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();