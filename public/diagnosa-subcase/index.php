<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$diagnosa_subcase 	= R::getAll("SELECT a.*, b.`name` AS `case_name` FROM `diagnosa_subcase` a LEFT JOIN `diagnosa_case` b ON a.`id_case` = b.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($diagnosa_subcase);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$diagnosa_subcase				= R::xdispense( 'diagnosa_subcase' );
		$diagnosa_subcase->id_case 		= $param['id_case'];
		$diagnosa_subcase->name 		= $param['name'];
		$id								= R::store( $diagnosa_subcase );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'subcase has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$diagnosa_subcase 				= R::load( 'diagnosa_subcase', $param['id'] );
		$diagnosa_subcase->id_case 		= $param['id_case'];
		$diagnosa_subcase->name 		= $param['name'];
		$id 							= R::store( $diagnosa_subcase );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'subase Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$diagnosa_subcase = R::getAll("SELECT `id` AS `value`, `name` AS `label` FROM `diagnosa_case`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($diagnosa_subcase);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();