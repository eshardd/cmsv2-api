<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$data 	= R::getAll("SELECT a.*,b.`name` AS `doctor_name`,c.`name` AS `hospital_name`,d.`name` AS `user_name` FROM `laporan_pemakaian` a 
		LEFT JOIN `doctor` b ON a.`id_dokter` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post("/filter", function (Request $request, Response $response){
	try {

		$post				= $request->getParsedBody();
		$sql_date = '';
		$sql_month = '';
		$sql_year = '';

		if (isset($post['date']) && $post['date'] != null) {
			$date = str_pad($post['date'], 2, 0, STR_PAD_LEFT);
			$sql_date = 'AND day(a.`created_date`) = '.$date;
		}
		
		if (isset($post['month']) && $post['month'] != null) {
			$month = str_pad($post['month'], 2, 0, STR_PAD_LEFT);
			$sql_month = 'AND month(a.`created_date`) = '.$month;
		}
		
		if (isset($post['year']) && $post['year'] != null) {
			$year = $post['year'];
			$sql_year = 'AND YEAR(a.`created_date`) = '.$year;
		}

		$data 	= R::getAll("SELECT a.*,b.`name` AS `doctor_name`,c.`name` AS `hospital_name`,d.`name` AS `user_name` FROM `laporan_pemakaian` a 
		LEFT JOIN `doctor` b ON a.`id_dokter` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id` WHERE 1=1 $sql_date $sql_month $sql_year ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `laporan_pemakaian`");
		if ($ai['id'] == null) {$new_ai = 1;}else{$new_ai = $ai['id']+1;}

		$post					= $request->getParsedBody();
		$param 					= $post['info'];
		$detail 				= $post['detail'];
		$data					= R::xdispense( 'laporan_pemakaian' );
	
		$data->tanggal			= date('Y-m-d H:i:s', strtotime($param['tanggal']));
		$data->no_lp			= 'PMN.LP/'.date('Ymd').'/'.$new_ai;
		$data->id_dokter 		= $param['dokter'];
		$data->id_hospital 		= $param['hospital'];
		$data->pasien 			= $param['pasien'];
		$data->no_mr 			= $param['no_mr'];
		$data->subdist 			= $param['subdist'];
		$data->created_by		= $param['id_user'];
		$data->created_date		= date("Y-m-d H:i:s");
		$id						= R::store( $data );

		if ($id) {
			foreach ($detail as $key => $value) {
				$data_detail = R::xdispense( 'laporan_pemakaian_detail' );
				$data_detail->id_laporan_penggunaan = $id;
				$data_detail->id_product_detail 	= $value['product_detail'];
				$data_detail->quantity 				= $value['quantity'];
				$data_detail->box 					= $value['box'];
				$id_detail							= R::store( $data_detail );
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'laporan_pemakaian', $param['id'] );
		$data->tanggal				= date('Y-m-d H:i:s', strtotime($param['tanggal']));
		$data->id_dokter 			= $param['dokter'];
		$data->id_hospital 			= $param['hospital'];
		$data->pasien 				= $param['pasien'];
		$data->no_mr 				= $param['no_mr'];
		$data->subdist 				= $param['subdist'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post("/product_detail", function (Request $request, Response $response){
	try {
		$post	= $request->getParsedBody();
		$id_box = $post['box'];
		$id_product_detail = $post['product_detail'];
		$product = R::getRow("SELECT a.*,b.`name`,b.`code`,c.`code_box` FROM `box_detail` a 
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id` 
		LEFT JOIN `box` c ON a.`id_box` = c.`id`
		WHERE b.`id` = $id_product_detail
		AND a.`id_box` = $id_box
		");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$dokter 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `doctor`");
		$hospital 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `hospital`");
		$product 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product`");
		$product_detail 	= R::getAll("SELECT `id` AS `value`, `name` as `label`,`id_product`,`code` FROM `product_detail` where `type` = 1");
		$subdist 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `subdist`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'dokter'=>$dokter,
			'hospital'=>$hospital,
			'product'=>$product,
			'product_detail'=>$product_detail,
			'subdist'=>$subdist,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown_box_list/{id}", function (Request $request, Response $response, $args){
	try {
		$id_rs = $args['id'];
		$data 			= R::getAll("SELECT a.`id` AS `value`,CONCAT(b.`name`,' - ',a.`code_box`) AS `label` FROM `box` a LEFT JOIN `product` b ON a.`product` = b.`id` WHERE a.`stay` = $id_rs");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'box'=>$data
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_detail_list/{id}", function (Request $request, Response $response, $args){
	try {
		$id_box 		= $args['id'];
		$data 			= R::getAll("SELECT a.`id_product_detail` AS `value` , b.`name` AS `label`  FROM `box_detail` a 
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id` WHERE b.`type` = 1
		AND a.`id_box` = $id_box");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'product_detail'=>$data
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_item_list/{id}", function (Request $request, Response $response, $args){
	try {
		$id_product 	= $args['id'];
		$data 			= R::getAll("SELECT a.`id` AS `value` , a.`name` AS `label`  FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id` WHERE b.`id` = $id_product");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'product_detail'=>$data
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post("/tambahan", function (Request $request, Response $response){
	try{

		$param					= $request->getParsedBody();
		$data_detail = R::xdispense( 'laporan_pemakaian_detail' );
		$data_detail->id_laporan_penggunaan = $param['id'];
		$data_detail->id_product_detail 	= $param['product_detail'];
		$data_detail->quantity 				= $param['quantity'];
		$id_detail							= R::store( $data_detail );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id_detail, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/detail/{id}", function (Request $request, Response $response,$args){
	try {
		$id = $args['id'];
		$data 	= R::getAll("SELECT a.*, c.`name` AS `product_detail_name`,c.`code` AS `product_detail_code` FROM `laporan_pemakaian_detail` a
		LEFT JOIN `laporan_pemakaian` b ON a.`id_laporan_penggunaan` = a.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id` where a.`id_laporan_penggunaan` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/detail/{id}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
		$box 	= R::load( 'laporan_pemakaian_detail', $id );
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});


$app->post('/add_detail', function (Request $request, Response $response){
	try{
		$post								= $request->getParsedBody();
		$data_detail 						= R::xdispense( 'laporan_pemakaian_detail' );
		$data_detail->id_laporan_penggunaan 	= $post['id_laporan_pemakaian'];
		$data_detail->id_product_detail 		= $post['detail']['product_detail'];
		$data_detail->quantity 				= $post['detail']['quantity'];
		$id_detail							= R::store( $data_detail );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/syncronize', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_laporan_pemakaian		= $param['id'];
		$data 						= R::getAll("SELECT * FROM `laporan_pemakaian_detail` WHERE `id_laporan_penggunaan` = $id_laporan_pemakaian");
		$data_laporan 				= R::getRow("SELECT * FROM `laporan_pemakaian` WHERE `id` = ".$id_laporan_pemakaian);

		if($data){
			foreach ($data as $key => $value) {
				$id_box = $value['box'];
				$id_product_detail = $value['id_product_detail'];
				$box = R::findOne( 'box', ' id = :id', [ ':id' => $id_box  ] );
				if(isset($id_box)  || $id_box != null){
					$warehouse = $box['id_warehouse'];
					$data_box = R::findOne( 'box_detail', ' id_box = :id_box AND id_product_detail = :id_product_detail ', [ ':id_product_detail' => $id_product_detail , ':id_box' => $id_box  ] );
					$new_qty = $data_box['quantity']- $value['quantity'];
					$data_box->quantity 				= $new_qty;
					R::store( $data_box );
				}else{
					$warehouse_stock						= R::xdispense( 'warehouse_stock' );
					$warehouse_stock->id_warehouse			= $warehouse;
					$warehouse_stock->id_product_detail		= $id_product_detail;
					$warehouse_stock->out					= $value['quantity'];
					$warehouse_stock->created_date			= date("Y-m-d H:i:s");
					$warehouse_stock->description			= "Laporan Pemakaian ".$data_laporan['no_lp'];
					$id										= R::store( $warehouse_stock );
				}
			}

			$data_laporan 				= R::load( 'laporan_pemakaian', $id_laporan_pemakaian );
			$data_laporan->integrated 	= 1;
			$id 						= R::store( $data_laporan );
		}

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();