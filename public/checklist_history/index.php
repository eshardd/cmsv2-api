<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response, $args){
	try {
		$box = R::getAll("SELECT a.*,
		b.`name` AS `doctor_name`,
		c.`name` AS `hospital_name`,
		d.`name` AS `spine_anatomi_name`,
		e.`name` AS `subcase_name`,
		f.`name` AS `case_name`,
		i.`name` AS `jenis_tagihan_name`,
		j.`name` AS `user_name`,
		k.`name` AS `box_status_name`,
		l.`name` AS `status_ro_name`,
		m.`name` AS `type_ro_name`,
		n.`name` AS `technical_support_name`
	FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
		LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
		LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
		LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
		LEFT JOIN `user` j ON a.`created_by` = j.`id`
		LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
		LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
		LEFT JOIN `user` n ON a.`technical_support` = n.`id` WHERE a.`status_ro` = 1 AND a.`stage` > 2 ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($box);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response, $args){
	try {
		$ro_list = R::getAll("SELECT `id` AS `value`, `no_ro` as `label` FROM `rencana_operasi` WHERE `status_ro` = 1");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($ro_list);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_list/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$item_list = R::getAll("SELECT a.*,b.`no_ro`, c.`name` AS `product_name`,
		d.`code_box`
		FROM `rencana_operasi_detail` a
				LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
				LEFT JOIN `product` c ON a.`id_product` = c.`id`
				LEFT JOIN `box` d ON a.`id_box` = d.`id` 
		WHERE a.`id_ro` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($item_list);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/assign_box', function (Request $request, Response $response){
	try{
		$param					= $request->getParsedBody();
		$data 					= R::load( 'rencana_operasi_detail', $param['id'] );
		$data->id_box 			= $param['id_box'];
		$id 					= R::store( $data );

		// update stage
		$param					= $request->getParsedBody();
		$rencana_operasi		= R::load('rencana_operasi', $param['id_ro']);
		$rencana_operasi->stage = 3;
		R::store( $rencana_operasi );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();