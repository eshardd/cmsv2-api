<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$product_detail 		= R::getAll("SELECT a.*,b.`name` AS `product_name`, c.`name` AS `type_name`,d.`name` AS `principle_name` FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id` 
		LEFT JOIN `product_type` c ON a.`type` = c.`id`
		LEFT JOIN `principle` d ON b.`id_principle` = d.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$product_detail					= R::xdispense( 'product_detail' );
		$product_detail->id_product 	= $param['id_product'];
		$product_detail->code			= $param['code'];
		$product_detail->name			= $param['name'];
		$product_detail->type			= $param['type'];
		$id								= R::store( $product_detail );

		// add ke list box biar kedepannya bisa di topup
		$boxlist = R::getAll("SELECT * FROM `box` WHERE `product` = ".$param['id_product']);
		foreach ($boxlist as $key => $value) {
			$data_box						= R::xdispense( 'box_detail' );
			$data_box->id_box 				= $value['id'];
			$data_box->id_product_detail	= $id;
			$data_box->quantity				= 0;
			$data_box->standard				= 0;
			$id_box							= R::store($data_box);
		}

		

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'product Has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$product_detail 				= R::load( 'product_detail', $param['id'] );
		$product_detail->id_product 	= $param['id_product'];
		$product_detail->code			= $param['code'];
		$product_detail->name			= $param['name'];
		$product_detail->type			= $param['type'];
		$id 							= R::store( $product_detail );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'product List Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$product 		= R::getAll("SELECT a.`id` AS `value`,CONCAT(b.`name`,' - ',a.`name`) AS `label` FROM `product` a
		LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$product_type 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product_type`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('product'=>$product,'product_type'=>$product_type));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/id/{id}", function (Request $request, Response $response,$args){
	try {
		$id_product_detail = $args['id'];
		$product_detail 		= R::getAll("SELECT a.*,b.`name` AS `product_name`, c.`name` AS `type_name`,d.`name` AS `principle_name` FROM `product_detail` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id` 
		LEFT JOIN `product_type` c ON a.`type` = c.`id`
		LEFT JOIN `principle` d ON b.`id_principle` = d.`id` 
		WHERE a.`id` = $id_product_detail ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->run();