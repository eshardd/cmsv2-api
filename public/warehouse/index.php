<?php
include '../configuration/index.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$warehouselist = R::getAll("SELECT a.*,b.`name` AS `subarea_name` FROM `warehouse` a
		LEFT JOIN `subarea` b ON a.`subarea` = b.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($warehouselist);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$warehouselist				= R::xdispense( 'warehouse' );
		$warehouselist->name 		= $param['name'];
		$warehouselist->subarea 	= $param['subarea'];
		$warehouselist->location	= $param['location'];
		$id							= R::store( $warehouselist );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Warehouse List has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$warehouselist 				= R::load( 'warehouse', $param['id'] );
		$warehouselist->name 		= $param['name'];
		$warehouselist->subarea 	= $param['subarea'];
		$warehouselist->location 	= $param['location'];
		$id 						= R::store( $warehouselist );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Warehouse List Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$subarea = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `subarea`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'subarea'=>$subarea,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();