<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/list", function (Request $request, Response $response){
	try {

		$total_principle 	= R::getRow("SELECT count(*) as `total` FROM `principle`");
		
		$total_doctor 		= R::getRow("SELECT count(*) as `total` FROM `doctor`");
		$total_rs_negri		= R::getRow("SELECT count(*) as `total` FROM `hospital` WHERE `type` = 'Negeri'");
		$total_rs_swasta	= R::getRow("SELECT count(*) as `total` FROM `hospital` WHERE `type` = 'Swasta'");
		$total_ro 			= R::getRow("SELECT count(*) as `total` FROM `rencana_operasi`");
		$penggunaan 		= R::getRow("SELECT sum(`quantity`) as `total` FROM `penggunaan_barang`");
		$operasi 			= R::getAll("SELECT DATE(a.`date`) AS `tanggal`, COUNT(a.`id`) AS `total` FROM `rencana_operasi` a GROUP BY DATE(a.`date`) ASC");
		$operasi_bulanan 	= R::getAll("SELECT MONTHNAME(a.`date`) AS `month`, COUNT(a.`id`) AS `total` FROM `rencana_operasi` a GROUP BY MONTH(a.`date`) ASC");
		$total_operasi 		= R::getRow("SELECT COUNT(*) as `total` FROM `rencana_operasi`");
		$pengiriman			= R::getAll("SELECT b.`name` AS `hospital_name`,c.`name` AS `doctor_name`,
		e.`name` AS `courier_name`,
		DATE_FORMAT(d.`eta`,'%e %M %Y %h:%i:%s') AS `estimasi_tiba`,
		a.* 
		FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `spb` d ON a.`id` = d.`id_ro`
		LEFT JOIN `user` e ON d.`courier` = e.`id`
		WHERE a.`stage` = 4");

		$penarikan			= R::getAll("SELECT b.`name` AS `hospital_name`,c.`name` AS `doctor_name`,
		e.`name` AS `courier_name`,
		DATE_FORMAT(d.`eta`,'%e %M %Y %h:%i:%s') AS `estimasi_tiba`,
		a.*,
		f.`name` AS `subarea_name`
		FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `spb` d ON a.`id` = d.`id_ro`
		LEFT JOIN `user` e ON d.`courier` = e.`id`
		LEFT JOIN `subarea` f ON b.`subarea` = f.`id`
		WHERE a.`stage` = 6");

		$penarikan_box = R::getAll('SELECT a.`id_ro`,b.`code_box`,c.`name` as `product_name` FROM `rencana_operasi_detail` a 
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product` c ON b.`product` = c.`id`');

		foreach ($penarikan_box as $key1 => $value1) {
			 foreach ($penarikan as $key2 => $value2) {
			 	if($value2['id'] == $value1['id_ro']){
			 		$penarikan[$key2]['item'][] = $value1;
			 	}
			 }
		}

		// 1
		$stage1 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 1 AND a.`status_ro` = 1
		");

		// 2
		$stage2 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 2 AND a.`status_ro` = 1
		");

		// 3
		$stage3 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 3 AND a.`status_ro` = 1
		");

		// 4
		$stage4 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 4 AND a.`status_ro` = 1
		");

		// 5
		$stage5 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 5 AND a.`status_ro` = 1
		");

		// 6
		$stage6 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 6 AND a.`status_ro` = 1
		");

		// 5
		$stage7 = R::getAll("SELECT a.`id`,a.`no_ro`,a.`created_date`, b.`name` AS `hospital_name`,c.`name` AS `doctor_name`, d.`name` AS `created_by_name`, e.`name` AS `subarea_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `subarea` e ON b.`subarea` = e.`id`
		WHERE a.`stage` = 7 AND a.`status_ro` = 1
		");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(
		array(
			'principle'=>$total_principle,
			'doctor'=>$total_doctor,
			'ro'=>$total_ro,
			'penggunaan'=>$penggunaan,
			'delivery'=>$pengiriman,
			'penarikan'=>$penarikan,
			'operasi'=>$operasi,
			'operasi_bulanan'=>$operasi_bulanan,
			'rs_negri'=>$total_rs_negri,
			'rs_swasta'=>$total_rs_swasta,
			'total_operasi'=>$total_operasi['total'],
			'stage1'=>$stage1,
			'stage2'=>$stage2,
			'stage3'=>$stage3,
			'stage4'=>$stage4,
			'stage5'=>$stage5,
			'stage6'=>$stage6,
			'stage7'=>$stage7,
			)
		);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->get("/master", function (Request $request, Response $response){
	try {

		$data 	= R::getAll("SELECT a.`id`,
		b.`no_ro`,
		n.`name` AS `created_by_name`,
		b.`date`,
		m.`name` AS `operasi_type`,
		f.`name` AS `hospital_name`,
		g.`name` AS `doctor_name`,
		b.`pasien`,
		b.`no_mr`,
		h.`name` AS `jenis_tagihan_name`,
		i.`name` AS `spine_anatomi`,
		j.`name` AS `case_name`,
		k.`name` AS `subcase_name`,
		l.`name` AS `ts_name`,
		d.`code_box`,
		e.`name` AS `product_name`,
		c.`name` AS `product_detail_name`,
		a.`quantity`,
		o.`no_spb`,
		p.`name` AS `courier_name`,
		o.`etd`,
		o.`eta`,
		q.`name` AS `kendaraan_name`,
		q.`nomor_polisi`,
		s.`name` AS `ditarik_oleh`,
		r.`etd` AS `ditarik_pada`,
		r.`eta` AS `tiba_pada`,
		b.`integrated`,
		t.`name` AS `stage_name`
		FROM `penggunaan_barang` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `box` d ON a.`id_box` = d.`id`
		LEFT JOIN `product` e ON d.`product` = e.`id`
		LEFT JOIN `hospital` f ON b.`id_hospital` = f.`id`
		LEFT JOIN `doctor` g ON b.`id_doctor` = g.`id`
		LEFT JOIN `jenis_tagihan` h ON b.`jenis_tagihan` = h.`id`
		LEFT JOIN `spine_anatomi` i ON b.`id_spine_anatomi` = i.`id`
		LEFT JOIN `diagnosa_case` j ON b.`case` = j.`id`
		LEFT JOIN `diagnosa_subcase` k  ON b.`subcase` = k.`id`
		LEFT JOIN `user` l ON b.`technical_support` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON b.`type_ro` = m.`id`
		LEFT JOIN `user` n ON b.`created_by` = n.`id`
		LEFT JOIN `spb` o ON b.`id` = o.`id_ro`
		LEFT JOIN `user` p ON o.`courier` = p.`id`
		LEFT JOIN `kendaraan` q ON o.`no_kendaraan` = p.`id`
		LEFT JOIN `penarikan_barang` r ON r.`id_spb` = o.`id`
		LEFT JOIN `user` s ON r.`courier` = s.`id`
		LEFT JOIN `rencana_operasi_stage` t ON b.`stage` = t.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});



$app->run();