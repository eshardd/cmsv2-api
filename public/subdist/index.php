<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$subdist 	= R::getAll("SELECT * FROM `subdist` ORDER BY `id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($subdist);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$subdist			= R::xdispense( 'subdist' );
		$subdist->name 		= $param['name'];
		$id					= R::store( $subdist );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$subdist 			= R::load( 'subdist', $param['id'] );
		$subdist->name 		= $param['name'];
		$id 				= R::store( $subdist );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();