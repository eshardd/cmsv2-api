<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/product_detail_name/{id}", function (Request $request, Response $response, $args){
	$id = $args['id'];
	try {
		$name = R::getRow("SELECT a.* FROM `product_detail` a WHERE a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($name);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->run();