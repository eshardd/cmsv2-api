<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$hospital = R::getAll("SELECT a.*,b.`name` AS `area_name`, c.`name` AS `subarea_name` FROM `hospital` a
		LEFT JOIN `area` b ON a.`area` = b.`id`
		LEFT JOIN `subarea` c ON a.`subarea` = c.`id`
		ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($hospital);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param					= $request->getParsedBody();
		$hospital				= R::xdispense( 'hospital' );
		$hospital->no 			= $param['no'];
		$hospital->name 		= $param['name'];
		$hospital->type			= $param['type'];
		$hospital->area			= $param['area'];
		$hospital->subarea		= $param['subarea'];
		$id						= R::store( $hospital );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Hospital has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param					= $request->getParsedBody();
		$hospital 				= R::load( 'hospital', $param['id'] );
		$hospital->no 			= $param['no'];
		$hospital->name 		= $param['name'];
		$hospital->type			= $param['type'];
		$hospital->area			= $param['area'];
		$hospital->subarea		= $param['subarea'];
		$id 					= R::store( $hospital );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Hospital Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$area 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `area`");
		$subarea = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `subarea`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'area'=>$area,
			'subarea'=>$subarea,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();