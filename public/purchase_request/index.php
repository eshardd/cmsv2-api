<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$jenis_tagihan 	= R::getAll("SELECT a.*,b.`name` AS `created_by_name` FROM `purchase_request` a LEFT JOIN `user` b ON a.`created_by` = b.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($jenis_tagihan);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `purchase_request`");

		if ($ai['id'] == null) {
			$new_ai = 1;
		}else{
			$new_ai = $ai['id']+1;
		}

		$data						= R::xdispense( 'purchase_request' );
		$data->date 				= $param['date'];
		$data->no_pr 				= 'PMN.PR/'.date('Ymd').'/'.$new_ai;
		$data->created_date			= date("Y-m-d H:i:s");
		$data->created_by 			= $param['id_user'];
		$data->page 				= $param['page'];
		$id							= R::store( $data );

		if($id){
			foreach ($param['data'] as $key => $value) {
				$data_detail						= R::xdispense( 'purchase_request_detail' );
				$data_detail->id_pr 				= $id;
				$data_detail->id_product_detail		= $value['product_detail'];
				$data_detail->quantity				= $value['quantity'];
				$id_detail							= R::store( $data_detail );
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_termin', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();

		foreach ($param['data'] as $key => $value) {
			$data						= R::xdispense( 'purchase_receive' );
			$data->id_purchase_request	= $param['id_pr'];
			$data->created_date			= date("Y-m-d H:i:s");
			$data->created_by 			= $param['id_user'];
			$data->id_product_detail 	= $value['product_detail'];
			$data->quantity 			= $value['quantity'];
			$id							= R::store( $data );
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_list', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'purchase_request_detail', $param['po_list_id'] );
		$data->id_product_detail	= $param['product_detail'];
		$data->quantity				= $param['quantity'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_data', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'purchase_request', $param['id'] );
		$data->status				= $param['status'];
		$data->processed_by			= $param['id_user'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown_product", function (Request $request, Response $response){
	try {
		$product 				= R::getAll("SELECT a.`id` AS `value`, CONCAT(b.`name`,' - ', a.`name`) AS `label` FROM `product` a LEFT JOIN `principle` b ON a.`id_principle` = b.`id` ORDER BY a.`id` DESC");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'product'=>$product,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown_termin/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$pr = R::getRow("SELECT * FROM `purchase_request` WHERE `id` = $id");
		$pr_detail = R::getAll("SELECT a.`id` AS `value`, b.`name` AS `label` FROM `purchase_request_detail` a LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id` WHERE a.`id_pr` = $id");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'purchase_request_info'=>$pr,
			'purchase_request_detail'=>$pr_detail,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown_product_detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id'];
		$product_detail	 		= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product_detail` where `id_product` = $id_product");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$data = R::getRow("SELECT a.*,b.`name` AS `product_name` FROM `product_detail` a LEFT JOIN `product` b ON a.`id_product` = b.`id` where a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list/{id_pr}", function (Request $request, Response $response, $args){
	try {
		$id_pr		= $args['id_pr'];
		$data 		= R::getAll("SELECT a.*,c.`code` AS `product_detail_code`,d.`id` AS `id_product`,d.`name` AS `product_name`, b.`no_pr` AS `purchase_request_no`, c.`name` AS `product_detail_name` FROM `purchase_request_detail` a 
		LEFT JOIN `purchase_request` b ON a.`id_pr` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id` WHERE a.`id_pr` = $id_pr");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/remove/{id}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
		$box 	= R::load( 'purchase_request_detail', $id );
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->get("/view_list/{id_pr}", function (Request $request, Response $response, $args){
	try {
		$id_pr		= $args['id_pr'];
		$po 		= R::getRow("SELECT a.*, b.`name` AS `user_name` FROM `purchase_request` a LEFT JOIN `user` b ON a.`created_by` = b.`id` WHERE a.`id` = $id_pr");
		$list 		= R::getAll("SELECT a.*,c.`code` AS `product_detail_code`,d.`id` as `id_product`,d.`name` AS `product_name`, b.`no_pr` AS `purchase_request_no`, c.`name` AS `product_detail_name` FROM `purchase_request_detail` a 
		LEFT JOIN `purchase_request` b ON a.`id_pr` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id` WHERE a.`id_pr` = $id_pr");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('po'=>$po,'list'=>$list));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();