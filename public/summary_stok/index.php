<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all/{id_wh}", function (Request $request, Response $response, $args){
	try {
		$id_wh = $args['id_wh'];

		$search = '';
		if($id_wh == 'all'){
			$wh_list = R::getAll("SELECT * FROM `warehouse`");
			$wh_last = R::getRow("SELECT * FROM `warehouse` ORDER BY `id` DESC LIMIT 0,1");
			
			$sqldata = '';
			foreach ($wh_list as $key => $value) {
				$id_wh = $value['id'];	
				$last = $wh_last['id'];
				
				if($value['id'] != $last){
					$union = 'UNION ';
				}else{				
					$union = '';
				}
				
				$sqldata .= 'SELECT 
				a.`id_warehouse`,
				b.`name` as `warehouse_name`,
				e.`name` AS `principle_name`,
				d.`name` AS `product_name`,
				c.`name` AS `product_detail_name`,
				c.`code`,
				c.`code_x`,
				SUM(a.`in`) AS `in`, 
				SUM(a.`out`) AS `out`, 
				SUM(a.`in`)-SUM(a.`out`) AS `summary_stock` 
				FROM `warehouse_stock` a 
				LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id`
				LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
				LEFT JOIN `product` d ON c.`id_product` = d.`id`
				LEFT JOIN `principle` e ON d.`id_principle` = e.`id`
				WHERE 1=1 AND a.`id_warehouse` = '.$id_wh.' GROUP BY a.`id_product_detail` '.$union;
			}
		}else{
			$search = "AND a.`id_warehouse`= $id_wh";
			$sqldata = "SELECT 
			a.`id_warehouse`,
			b.`name` as `warehouse_name`,
			e.`name` AS `principle_name`,
			d.`name` AS `product_name`,
			c.`name` AS `product_detail_name`,
			c.`code`,
			c.`code_x`,
			SUM(a.`in`)-SUM(a.`out`) AS `summary_stock` 
			FROM `warehouse_stock` a 
			LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id`
			LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
			LEFT JOIN `product` d ON c.`id_product` = d.`id`
			LEFT JOIN `principle` e ON d.`id_principle` = e.`id`
			WHERE 1=1 $search GROUP BY a.`id_product_detail`";
		}		

		

		

		$data = R::getAll($sqldata);
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response, $args){
	try {

		$data = R::getAll("SELECT `id` as `value`, `name` as `label` FROM `warehouse`");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();