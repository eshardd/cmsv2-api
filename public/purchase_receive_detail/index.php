<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/list/{id}", function (Request $request, Response $response , $args){
	try {
		$id = $args['id'];
		$purchase_receive 	= R::getRow("SELECT a.*,b.`name` AS `created_by_name`,DATE_FORMAT(a.`date`,'%d %M %Y') AS `date`, c.`no_pr`
		FROM `purchase_receive` a
		LEFT JOIN `user` b ON a.`created_by` = b.`id` 
		LEFT JOIN `purchase_request` c ON a.`id_purchase_request` = c.`id`
		WHERE a.`id` = $id");

		$purchase_receive_detail = R::getAll("SELECT a.*,
		d.`id` AS `id_product`,
		d.`id_principle`,
		c.`name` AS `product_detail_name`,
		d.`name` AS `product_name`, 
		c.`code` AS `code`,
		e.`no_pr`
		FROM `purchase_receive_detail` a
		LEFT JOIN `purchase_receive` b ON a.`id_purchase_receive` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id`
		LEFT JOIN `purchase_request` e ON b.`id_purchase_request` = e.`id`
		WHERE a.`id_purchase_receive` = $id");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(
			array('purchase_receive'=>$purchase_receive,'purchase_receive_detail'=>$purchase_receive_detail)
		);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'purchase_receive_detail', $param['id'] );
		$data->id_product_detail	= $param['id_product_detail'];
		$data->quantity				= $param['quantity'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/delete/{id}', function ($request, $response, $args) {
    try {
	    $id 	= $args['id'];
		$box 	= R::load( 'purchase_receive_detail', $id );
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->run();