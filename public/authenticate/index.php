<?php
include '../configuration/index.php';
require '../configuration/keys.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

$app->post("/login", function (Request $request, Response $response){
	try {
		$param			= $request->getParsedBody();
		$email 			= $param['email'];
		$password 		= $param['password'];
		$user 			= R::getRow("SELECT * FROM `user` where `email` = '$email'");

		if (!password_verify($param['password'], $user['password'])){
			// login gagal
			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('user'=>null,'message'=>'Login Failed'));
		}else{
			// login sukses
			$privilege_group_id = $user['privilege'];
			$user_privilege_access = R::getAll("SELECT `id_user_privilege_menu` FROM `user_privilege_access` WHERE `id_user_privilege_group` = $privilege_group_id");
			$modify_array = array();
			foreach ($user_privilege_access as $key => $value) {
				$modify_array[] = $value['id_user_privilege_menu']; 
			}

			$user = [
                'id'			=>$user['id'],
                'email'			=>$user['email'],
                'name'			=>$user['name'],
                'level'			=>$user['level'],
                'privilege'		=>$user['privilege'],
                'division'		=>$user['division'],
                'privilege_list'=>$modify_array,
            ];
            $key = keys();
            $payload = array(
                "iat" => time(),  // jam sekarang
                "exp" => time() + (60*60) // (menit * detik)   ini di set mau berapa menit
            );
            $jwt = JWT::encode($payload, keys());
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson(array('token'=>$jwt,'user'=>$user,'message'=>'Login Success'));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();