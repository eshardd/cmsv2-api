<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all_box", function (Request $request, Response $response, $args){
	try {
		$box = R::getAll("SELECT a.*, 
		b.`name` AS `product_name`, 
		d.`name` AS `created_by_name`, 
		e.`name` AS `status_name`, 
		f.`name` AS `principle_name`,
		c.`name` AS `warehouse_name`, 
		g.`name` AS `hospital_name`
		FROM `box` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `warehouse` c ON a.`id_warehouse` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `box_status` e ON a.`status` = e.`id`
		LEFT JOIN `principle` f ON b.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON a.`stay` = g.`id`
		LEFT JOIN `rencana_operasi_detail` h ON h.`id_box` = a.`id`
		GROUP BY a.`id`
		ORDER BY a.`code_box` ASC");

		$new_array = array();
		foreach ($box as $key => $value) {
			$id_si_box = $value['id'];
			$check = R::getRow("SELECT * FROM `box_detail` WHERE `id_box` = $id_si_box AND `quantity` != `standard`");

			if($check){
				$value['condition'] = 'Not Complete';
			}else{
				$value['condition'] = 'Complete';
			}

			$checktotal = R::getRow("SELECT SUM(`quantity`) AS `jumlah` FROM `box_detail` WHERE `id_box` = $id_si_box");

			if($checktotal['jumlah'] == 0){
				$value['condition'] = 'Empty';
			}
			
			$new_array[] = $value;
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($new_array);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/filter_box/{id_product}/{id_warehouse}/{status}", function (Request $request, Response $response, $args){
	try {
		$id_warehouse = $args['id_warehouse'];
		$id_product = $args['id_product'];
		$status = $args['status'];
		$filter_wh = '';
		$filter_prod = '';
		$filter_status = '';

		if($id_warehouse == 0){
			$filter_wh = '';
		}else{
			$filter_wh = 'AND c.`id` = '.$id_warehouse;
		}

		if($id_product == 0){
			$filter_prod = '';
		}else{
			$filter_prod = 'AND a.`product` = '.$id_product;
		}

		if($status == 0){
			$filter_status = '';
		}else{
			$filter_status = 'AND a.`status` = '.$status;
		}

		$box = R::getAll("SELECT a.*, 
		b.`name` AS `product_name`, 
		d.`name` AS `created_by_name`, 
		e.`name` AS `status_name`, 
		f.`name` AS `principle_name`,
		c.`name` AS `warehouse_name`, 
		g.`name` AS `hospital_name`,
		i.`id` AS `id_subarea`
		FROM `box` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `warehouse` c ON a.`id_warehouse` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `box_status` e ON a.`status` = e.`id`
		LEFT JOIN `principle` f ON b.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON a.`stay` = g.`id`
		LEFT JOIN `rencana_operasi_detail` h ON h.`id_box` = a.`id`
		LEFT JOIN `subarea` i ON c.`subarea` = i.`id`
		WHERE 1 = 1
		".$filter_wh." ".$filter_prod." ".$filter_status."
		GROUP BY a.`id`
		ORDER BY a.`code_box` ASC
		");

		$new_array = array();
		foreach ($box as $key => $value) {
			$id_si_box = $value['id'];
			$check = R::getRow("SELECT * FROM `box_detail` WHERE `id_box` = $id_si_box AND `quantity` != `standard`");

			if($check){
				$value['condition'] = 'Not Complete';
			}else{
				$value['condition'] = 'Complete';
			}

			$checktotal = R::getRow("SELECT SUM(`quantity`) AS `jumlah` FROM `box_detail` WHERE `id_box` = $id_si_box");

			if($checktotal['jumlah'] == 0){
				$value['condition'] = 'Empty';
			}
			


			$new_array[] = $value;
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($new_array);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/filter_box_assign/{id_product}/{id_subarea}/{status}", function (Request $request, Response $response, $args){
	try {
		$id_subarea = $args['id_subarea'];
		$id_product = $args['id_product'];
		$status = $args['status'];
		$filter_wh = '';
		$filter_prod = '';
		$filter_status = '';
		$id_subarea = 0;
		if($id_subarea == 0){
			$filter_wh = '';
		}else{
			$filter_wh = 'AND i.`id` = '.$id_subarea;
		}

		if($id_product == 0){
			$filter_prod = '';
		}else{
			$filter_prod = 'AND a.`product` = '.$id_product;
		}

		if($status == 0 || $status == 3){
			$filter_status = '';
		}else{
			$filter_status = 'AND a.`status` = '.$status;
		}

		$box = R::getAll("SELECT a.*, 
		b.`name` AS `product_name`, 
		d.`name` AS `created_by_name`, 
		e.`name` AS `status_name`, 
		f.`name` AS `principle_name`,
		c.`name` AS `warehouse_name`, 
		g.`name` AS `hospital_name`,
		i.`id` AS `id_subarea`
		FROM `box` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `warehouse` c ON a.`id_warehouse` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `box_status` e ON a.`status` = e.`id`
		LEFT JOIN `principle` f ON b.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON a.`stay` = g.`id`
		LEFT JOIN `rencana_operasi_detail` h ON h.`id_box` = a.`id`
		LEFT JOIN `subarea` i ON c.`subarea` = i.`id`
		WHERE 1 = 1 AND a.`status` != 3
		".$filter_wh." ".$filter_prod." ".$filter_status."
		GROUP BY a.`id`
		ORDER BY a.`code_box` ASC
		");

		$new_array = array();
		foreach ($box as $key => $value) {
			$id_si_box = $value['id'];
			$check = R::getRow("SELECT * FROM `box_detail` WHERE `id_box` = $id_si_box AND `quantity` != `standard`");

			if($check){
				$value['condition'] = 'Not Complete';
			}else{
				$value['condition'] = 'Complete';
			}

			$checktotal = R::getRow("SELECT SUM(`quantity`) AS `jumlah` FROM `box_detail` WHERE `id_box` = $id_si_box");

			if($checktotal['jumlah'] == 0){
				$value['condition'] = 'Empty';
			}
			


			$new_array[] = $value;
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($new_array);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/all_product", function (Request $request, Response $response, $args){
	try {
		$product = R::getAll("SELECT b.`id` as `principle_id`,a.`id` AS `value`,a.`name` AS `label` FROM `product` a
		LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$hospital = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `hospital`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('product'=>$product,'hospital'=>$hospital));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/standard/{id_product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$product = R::getAll("SELECT a.*, b.`name` AS `product_name`, c.`name` AS `product_detail_name` FROM `box_standard` a
				LEFT JOIN `product` b ON a.`product` = b.`id`
				LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id` WHERE a.`product` = $id_product");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/checklist/{id_product}/{id_warehouse}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$id_warehouse = $args['id_warehouse'];
		$product = R::getAll("SELECT a.*, b.`name` AS `product_name`, c.`code` , c.`name` AS `product_detail_name`,
		(SELECT (SUM(`in`)-SUM(`out`)) FROM `warehouse_stock` WHERE `id_product_detail` = a.`product_detail` AND `id_warehouse` = $id_warehouse) AS `stok` 
		FROM `box_standard` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id` WHERE a.`product` = $id_product AND (a.`quantity` != 0 || a.`quantity_skoliosis` != 0)");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product/{id_product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$arr_include = array('33','34','35');
		$where = (isset($args['id_product']) && $args['id_product'] != 33 && $args['id_product'] != 34 && $args['id_product'] != 35 ) ? '`id_product` = '.$args['id_product'] : '1=1';
		$product = R::getAll("SELECT `id` AS `value`, concat(`code`,' - ',`name`) as `label`,`type` FROM `product_detail` WHERE $where");
		
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/isibox/{id}", function (Request $request, Response $response, $args){
	try {
		$id_box = $args['id'];

		$box = R::getAll("
		SELECT a.*, b.`code_box`,c.`code` AS `code`,c.`name` AS `product_detail_name`, c.`type`
		FROM `box_detail` a 
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		WHERE a.`id_box` = $id_box AND a.`standard` != 0 
		");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($box);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_implant', function (Request $request, Response $response){
	try{
		$param								= $request->getParsedBody();
		$box_standard						= R::xdispense( 'box_standard' );
		$box_standard->product 				= $param['product'];
		$box_standard->product_detail		= $param['product_detail'];
		$box_standard->quantity				= $param['quantity'];
		$box_standard->quantity_skoliosis	= $param['quantity_skoliosis'];
		$box_standard->type					= 1;
		$id									= R::store( $box_standard );

		$product 			= $param['product'];
		$product_detail 	= $param['product_detail'];
		$quantity_standard 	= $param['quantity'];
		$quantity_skoliosis = $param['quantity_skoliosis'];

		$update_box_standard 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 1) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

		$update_box_skoliosis 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 2) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_instrument', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$box_standard					= R::xdispense( 'box_standard' );
		$box_standard->product 			= $param['product'];
		$box_standard->product_detail	= $param['product_detail'];
		$box_standard->quantity			= $param['quantity'];
		$box_standard->quantity_skoliosis	= $param['quantity_skoliosis'];
		$box_standard->type				= 2;
		$id								= R::store( $box_standard );

		$product 			= $param['product'];
		$product_detail 	= $param['product_detail'];
		$quantity_standard 	= $param['quantity'];
		$quantity_skoliosis = $param['quantity_skoliosis'];

		$update_box_standard 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 1) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

		$update_box_skoliosis 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 2) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_standard', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$box 						= R::load( 'box_standard', $param['id'] );
		$box->product_detail		= $param['product_detail'];
		$box->quantity				= $param['quantity'];
		$box->quantity_skoliosis	= $param['quantity_skoliosis'];
		$box->type					= $param['type'];
		$id 						= R::store( $box );

		$product 			= $param['product'];
		$product_detail 	= $param['product_detail'];
		$quantity_standard 	= $param['quantity'];
		$quantity_skoliosis = $param['quantity_skoliosis'];

		$update_box_standard 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 1) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

		$update_box_skoliosis 		= R::exec( "UPDATE `box_detail` SET `standard` = 
		(SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
		WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $product AND `type` = 2) 
		AND `id_product_detail` = `id_product_detail`
		AND (SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_box', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_product 				= $param['product'];
		$box						= R::xdispense( 'box' );
		$box->product 				= $id_product;
		$box->code_box				= $param['no_box'];
		$box->id_warehouse			= $param['warehouse'];
		$box->status				= $param['status'];
		if($param['status'] == 1){
			$box->stay					= $param['rs'];
		}else{			
			$box->stay					= NULL;
		}
		$box->created_date			= date("Y-m-d H:i:s");
		$box->created_by			= $param['user_id'];
		$id							= R::store( $box );

		if ($id) {

			$list_product_detail	= R::getAll("SELECT a.* FROM `product_detail` a WHERE `id_product` = $id_product");
			foreach ($list_product_detail as $key => $value) {
				$box_detail						= R::xdispense( 'box_detail' );
				$box_detail->id_box				= $id;
				$box_detail->id_product_detail	= $value['id'];
				$box_detail->quantity			= 0;
				$id_box_detail					= R::store( $box_detail );
			}
		}


		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/topup_box', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();

		// parameter box yang akan di isi
		$id_box 						= $param['id'];
		$user_id 						= $param['user_id'];
		$id_product 					= $param['product'];
		$id_warehouse 					= $param['id_warehouse'];
		$type 							= $param['type'];
		$notes 							= $param['notes'];

		// cek dulu ada stok apa kaga di wh tempat box itu berada

		// set tipe box
		$box 							= R::load( 'box', $id_box );
		$box->type 						= $type;
		$box							= R::store( $box );

		// add nomor checklist ke dalam list checklist
		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `box_topup`");
		if ($ai['id'] == null) {
			$new_ai = 1;
		}else{
			$new_ai = $ai['id']+1;
		}

		$box_topup						= R::xdispense( 'box_topup' );
		$box_topup->no_topup 			= 'PMN.TOP/'.date('Ymd').'/'.$new_ai;
		$box_topup->id_box				= $id_box;
		$box_topup->created_date		= date("Y-m-d H:i:s");
		$box_topup->created_by			= $user_id;
		$box_topup->notes				= $notes;
		$id_box_topup					= R::store( $box_topup );

		// standar pengisian box
		$joint_query = R::getAll(
			"SELECT a.*,
				b.`quantity` AS `normal_quantity`,
				b.`quantity_skoliosis` AS `skoliosis_quantity`,
				(SELECT (SUM(`in`)-SUM(`out`)) FROM `warehouse_stock` WHERE `id_product_detail` = a.`id_product_detail` AND `id_warehouse` = c.`id_warehouse`) AS `stok`, 
				c.`id_warehouse` as `warehouse_id`
			FROM `box_detail` a
				left join `box_standard` b on a.`id_product_detail` = b.`product_detail` 
				left join `box` c on a.`id_box` = c.`id`
			where a.`id_box` = $id_box
			AND ((a.`quantity` != a.`standard`) OR ((a.`quantity` = 0 AND a.`standard` = 0)))
			and c.`product` = $id_product
			and ((b.`quantity` is not NULL) OR (b.`quantity_skoliosis` is not null))
			GROUP BY a.`id`");

		// loop untuk update data barangnya 1 per 1
		foreach ($joint_query as $key => $value) {
			$kekurangan_stok				= 0;
			$pengurangan_qty_gudang			= 0;
			$penambahan_qty_gudang			= 0;
			$hutang_topup					= 0;
			$pengisian_lengkap				= 0;
			$qty_pengisian					= 0;
			$flag_update 					= true;				// flag yg menentukan apakah di update atau tidak
			$box_detail						= R::load( 'box_detail', $value['id'] );
			$qty_stok_gudang				= $value['stok'];  
			$qty_standard 					= $value['standard'];
			$stok_dalam_box 				= $value['quantity'];

			// ========
			if($stok_dalam_box > $qty_standard){
				$penambahan_qty_gudang = $stok_dalam_box-$qty_standard;
				$qty_pengisian = $stok_dalam_box - $penambahan_qty_gudang;
				
				$box_detail->quantity		= $qty_pengisian;
				$box_detail->standard		= $qty_standard;
				$id_box_detail				= R::store( $box_detail );

				$warehouse_stock						= R::xdispense( 'warehouse_stock' );
				$warehouse_stock->id_warehouse 			= $id_warehouse;
				$warehouse_stock->id_product_detail		= $box_detail['id_product_detail'];
				$warehouse_stock->in					= $penambahan_qty_gudang;
				$warehouse_stock->out					= 0;
				$warehouse_stock->description			= '[system] - Penambahan dari Box :'.$id_box;
				$warehouse_stock->created_date			= date("Y-m-d H:i:s");
				$warehouse_stock->created_by			= 0;
				$id_wh_stock							= R::store( $warehouse_stock );

			}elseif($stok_dalam_box < $qty_standard){
				$pengisian_lengkap = $qty_standard - $stok_dalam_box;

				if($qty_stok_gudang >= $pengisian_lengkap){
					$qty_pengisian 			= $pengisian_lengkap;
					$pengurangan_qty_gudang = $qty_pengisian;
				}elseif($qty_stok_gudang < $pengisian_lengkap){
					$qty_pengisian 			= $qty_stok_gudang;
					$pengurangan_qty_gudang = $qty_stok_gudang;
				}
				
				$box_detail->quantity		= $box_detail['quantity'] + $qty_pengisian;
				$box_detail->standard		= $qty_standard;
				$id_box_detail				= R::store( $box_detail );

				$warehouse_stock						= R::xdispense( 'warehouse_stock' );
				$warehouse_stock->id_warehouse 			= $id_warehouse;
				$warehouse_stock->id_product_detail		= $box_detail['id_product_detail'];
				$warehouse_stock->in					= 0;
				$warehouse_stock->out					= $pengurangan_qty_gudang;
				$warehouse_stock->description			= '[system] - Topup Box :'.$id_box;
				$warehouse_stock->created_date			= date("Y-m-d H:i:s");
				$warehouse_stock->created_by			= 0;
				$id_wh_stock							= R::store( $warehouse_stock );
			}elseif($stok_dalam_box == 0 && $qty_standard == 0){
				
				if($param['type'] == 1){
					$pengisian_lengkap				= $value['normal_quantity'];
				}elseif($param['type'] == 2){
					$pengisian_lengkap 				= $value['skoliosis_quantity'];
				}

				if($qty_stok_gudang >= $pengisian_lengkap){
					$qty_pengisian 			= $pengisian_lengkap;
					$pengurangan_qty_gudang = $qty_pengisian;
				}elseif($qty_stok_gudang < $pengisian_lengkap){
					$qty_pengisian 			= $qty_stok_gudang;
					$pengurangan_qty_gudang = $qty_stok_gudang;
				}
				
				$box_detail->quantity		= $box_detail['quantity'] + $qty_pengisian;
				$box_detail->standard		= $pengisian_lengkap;
				$id_box_detail				= R::store( $box_detail );

				$warehouse_stock						= R::xdispense( 'warehouse_stock' );
				$warehouse_stock->id_warehouse 			= $id_warehouse;
				$warehouse_stock->id_product_detail		= $box_detail['id_product_detail'];
				$warehouse_stock->in					= 0;
				$warehouse_stock->out					= $pengurangan_qty_gudang;
				$warehouse_stock->description			= '[system] - Topup Box :'.$id_box;
				$warehouse_stock->created_date			= date("Y-m-d H:i:s");
				$warehouse_stock->created_by			= 0;
				$id_wh_stock							= R::store( $warehouse_stock );
			}
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_box', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$box 						= R::load( 'box', $param['id'] );
		$box->product 				= $param['product'];
		$box->code_box				= $param['no_box'];
		$box->id_warehouse			= $param['warehouse'];
		$box->status				= $param['status'];
		$box->type					= $param['type'];

		$product = $param['product'];
		$id_box = $param['id'];
		
		if($param['type'] == 1){
			R::exec( "UPDATE `box_detail` SET `standard` = 
			(SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
			WHERE `id_box` = $id_box 
			AND `id_product_detail` = `id_product_detail`");

			$stok_lebih = R::getAll("SELECT * FROM `box_detail` WHERE `id_box` = $id_box AND `quantity` > `standard`");
			foreach ($stok_lebih as $key => $value) {
				$box_detail						= R::load( 'box_detail', $value['id'] );
				$pindah_ke_gudang 				= $box_detail->quantity;
				$box_detail->quantity 			= $box_detail->quantity - $pindah_ke_gudang;
				$id_box_detail					= R::store( $box_detail );
				
				$warehouse_stock						= R::xdispense( 'warehouse_stock' );
				$warehouse_stock->id_warehouse 			= $box->id_warehouse;
				$warehouse_stock->id_product_detail		= $value['id_product_detail'];
				$warehouse_stock->in					= $pindah_ke_gudang;
				$warehouse_stock->out					= 0;
				$warehouse_stock->description			= '[system] - Penambahan dari Box :'.$id_box;
				$warehouse_stock->created_date			= date("Y-m-d H:i:s");
				$warehouse_stock->created_by			= 0;
				$id_wh_stock							= R::store( $warehouse_stock );
			}

		}else{
			R::exec( "UPDATE `box_detail` SET `standard` = 
			(SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`)
			WHERE `id_box` = $id_box 
			AND `id_product_detail` = `id_product_detail`
			AND (SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $product AND a.`product_detail` = `id_product_detail`) != 0");
		}

		if($param['status'] == 1){
			$box->stay					= $param['rs'];
		}else{			
			$box->stay					= NULL;
		}
		$id 						= R::store( $box );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Doctor Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/standard/{id}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
	    $box 	= R::load( 'box_standard', $id );

		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->get("/dropdown_filter", function (Request $request, Response $response){
	try {
		$warehouse 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `warehouse`");
		$product 				= R::getAll("SELECT a.`id` AS `value`, CONCAT(b.`name`,' - ', a.`name`) AS `label` FROM `product` a
									LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$status 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `box_status`");
		$principle 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `principle`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'warehouse'=>$warehouse,
			'product'=>$product,
			'status'=>$status,
			'principle'=>$principle,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/buffer_item_list/{id_product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$data 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product_detail` where `id_product` = $id_product");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'product_detail'=>$data
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list_buffer/{id_box}", function (Request $request, Response $response, $args){
	try {
		$id_box		= $args['id_box'];
		$data 		= R::getAll("SELECT a.*, b.`code_box` AS `code_box` , c.`name` AS `product_detail_name` FROM `box_buffer` a
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id` WHERE a.`id_box` = $id_box");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_buffer', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_warehouse				= $param['id_warehouse'];
		$data						= R::xdispense( 'box_buffer' );
		$data->id_box 				= $param['id_box'];
		$data->id_product_detail	= $param['product_detail'];
		$data->quantity				= $param['quantity'];
		$id							= R::store( $data );

		if($id){
			// mengurangi stok wh tempat box tersebut berada
			$id_box = $param['id_box'];
			$data_box 							= R::getRow("SELECT * FROM `box` WHERE `id` = $id_box");
			$warehouse_stock					= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse 		= $id_warehouse;
			$warehouse_stock->id_product_detail	= $param['product_detail'];
			$warehouse_stock->in				= 0;
			$warehouse_stock->out				= $param['quantity'];
			$warehouse_stock->description		= '[Tambah Buffer Box] '.$data_box['code_box'];
			$warehouse_stock->created_date		= date("Y-m-d H:i:s");
			$id									= R::store( $warehouse_stock );
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_buffer', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'box_buffer', $param['buffer_list_id'] );
		$data->id_product_detail 	= $param['product_detail'];
		$data->quantity				= $param['quantity'];
		$id 						= R::store( $data );

		if($id){

			// $qty_lama = $data['quantity'];
			// $qty_baru = $data['quantity'];

			// mengurangi stok wh tempat box tersebut berada
			$id_box = $param['id_box'];
			$data_box 							= R::getRow("SELECT * FROM `box` WHERE `id` = $id_box");
			$warehouse_stock					= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse 		= $id_warehouse;
			$warehouse_stock->id_product_detail	= $param['product_detail'];
			$warehouse_stock->in				= 0;
			$warehouse_stock->out				= $param['quantity'];
			$warehouse_stock->description		= '[Tambah Buffer Box] '.$data_box['code_box'];
			$id									= R::store( $warehouse_stock );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/remove_buffer/{id}/{id_warehouse}', function ($request, $response, $args) {
    try {
	    $id = $args['id'];
		$box 	= R::load( 'box_buffer', $id );
		
		
		// mengurangi stok wh tempat box tersebut berada
		$id_box = $box['id_box'];
		$data_box 							= R::getRow("SELECT * FROM `box` WHERE `id` = $id_box");
		$warehouse_stock					= R::xdispense( 'warehouse_stock' );
		$warehouse_stock->id_warehouse 		= $args['id_warehouse'];
		$warehouse_stock->id_product_detail	= $box['id_product_detail'];
		$warehouse_stock->in				= $box['quantity'];
		$warehouse_stock->out				= 0;
		$warehouse_stock->description		= '[Pengurangan Buffer Box] '.$data_box['code_box'];
		$warehouse_stock->created_date		= date("Y-m-d H:i:s");
		$id									= R::store( $warehouse_stock );
		
		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->get("/update_isi_box", function (Request $request, Response $response, $args){
	try {
		$product = R::getAll("SELECT `id` FROM `product`");
		foreach ($product as $key => $value) {
			$id_product = $value['id'];
			R::exec("UPDATE `box_detail` SET `standard` = (SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $id_product AND a.`product_detail` = `id_product_detail`)
			WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $id_product AND `type` = 1) 
			AND `id_product_detail` = `id_product_detail`
			AND (SELECT a.`quantity` FROM `box_standard` a WHERE a.`product` = $id_product AND a.`product_detail` = `id_product_detail`) != 0");

			R::exec("UPDATE `box_detail` SET `standard` = 
			(SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $id_product AND a.`product_detail` = `id_product_detail`)
			WHERE `id_box` IN(SELECT `id` FROM `box` WHERE `product` = $id_product AND `type` = 2) 
			AND `id_product_detail` = `id_product_detail`
			AND (SELECT a.`quantity_skoliosis` FROM `box_standard` a WHERE a.`product` = $id_product AND a.`product_detail` = `id_product_detail`) != 0");
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();