<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$purchase_receive 	= R::getAll("SELECT a.*,c.`no_pr`,b.`name` AS `created_by_name`
		FROM `purchase_receive` a 
		LEFT JOIN `user` b ON a.`created_by` = b.`id`
		LEFT JOIN `purchase_request` c ON a.`id_purchase_request` = c.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($purchase_receive);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();


		$data_pr = $param['data'];
		$data_detail = $param['detail'];


		$data						= R::xdispense( 'purchase_receive' );
		$data->date 				= date("Y-m-d H:i:s",strtotime($data_pr['date']));
		$data->id_purchase_request	= $data_pr['purchase_request'];
		$data->name					= $data_pr['name'];
		$data->created_date			= date("Y-m-d H:i:s");
		$data->created_by 			= $data_pr['id_user'];
		$id							= R::store( $data );

		if($id){
			foreach ($data_detail as $key => $value) {
				$data_detail						= R::xdispense( 'purchase_receive_detail' );
				$data_detail->id_purchase_receive	= $id;
				$data_detail->id_product_detail		= $value['product_detail'];
				$data_detail->quantity				= $value['quantity'];
				$id_detail							= R::store( $data_detail );
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/request_list/{id_pr}", function (Request $request, Response $response, $args){
	try {
		$id_purchase_request = $args['id_pr'];
		$purchase_request_detail = R::getAll("SELECT a.*,d.`id_principle`,b.`no_pr`,c.`name` AS `product_detail_name`,d.`name` AS `product_name`, c.`code` AS `code` FROM `purchase_request_detail` a
		LEFT JOIN `purchase_request` b ON a.`id_pr` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id`
		WHERE a.`id_pr` = $id_purchase_request");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($purchase_request_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->get("/dropdown", function (Request $request, Response $response, $args){
	try {
		$nomor_purchase_request			= R::getAll("SELECT a.`id` AS `value`, a.`no_pr` AS `label` FROM `purchase_request` a");
		$product_detail					= R::getAll("SELECT a.`id_product_detail` AS `value`, b.`name` AS `label`,a.`id_pr`
		FROM `purchase_request_detail` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'purchase_request'=>$nomor_purchase_request,
			'product_detail'=>$product_detail
			)
		);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown_product_detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id'];
		$product_detail	 		= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product_detail` where `id_product` = $id_product");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();