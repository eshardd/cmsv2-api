<?php
include '../configuration/index.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$data = R::getAll("SELECT a.*,
		b.`name` AS `product_in_name`,
		c.`name` AS `product_out_name`,
		d.`name` AS `product_detail_in_name`,
		e.`name` AS `product_detail_out_name`
		FROM `warehouse_replacement` a
		LEFT JOIN `product` b ON a.`product_in` = b.`id`
		LEFT JOIN `product` c ON a.`product_out` = c.`id`
		LEFT JOIN `product_detail` d ON a.`product_detail_in` = d.`id`
		LEFT JOIN `product_detail` e ON a.`product_detail_out` = e.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/item_stock/{id_product_detail}", function (Request $request, Response $response, $args){
	try {
		$id_product_detail = $args['id_product_detail'];
		$data = R::getRow("SELECT a.*, b.`name` AS `product_name`,c.`name` AS `principle_name`,
		(SELECT (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = a.`id` AND `id_warehouse` = 1) AS `sisa`,
		(SELECT SUM(aa.`quantity`) FROM `box_detail` aa LEFT JOIN `box` bb ON aa.`id_box` = bb.`id` LEFT JOIN `warehouse` cc ON bb.`id_warehouse` = cc.`id` WHERE bb.`id_warehouse` = 1 AND aa.`id_product_detail` = a.`id`) AS `inbox`
		FROM `product_detail` a
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `principle` c ON b.`id_principle` = c.`id`
		WHERE a.`id` = $id_product_detail");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data						= R::xdispense( 'warehouse_replacement' );
		$data->product_in 			= $param['product_in'];
		$data->product_out			= $param['product_out'];
		$data->product_detail_in 	= $param['product_detail_in'];
		$data->product_detail_out	= $param['product_detail_out'];
		$data->quantity				= $param['quantity'];
		$data->created_by			= $param['user_id'];
		$data->created_date			= date("Y-m-d H:i:s");
		$id							= R::store( $data );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'warehouse_replacement', $param['id'] );
		$data->product_in 			= $param['product_in'];
		$data->product_out			= $param['product_out'];
		$data->product_detail_in 	= $param['product_detail_in'];
		$data->product_detail_out	= $param['product_detail_out'];
		$data->quantity				= $param['quantity'];
		$data->created_date			= date("Y-m-d H:i:s");
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/integrate', function (Request $request, Response $response){
	try{

		// penyesuaian stok
		$param							= $request->getParsedBody();
		
		$data_in						= R::xdispense( 'warehouse_stock' );
		$data_in->id_warehouse			= 1;
		$data_in->id_product_detail		= $param['product_detail_in'];
		$data_in->in					= $param['quantity'];
		$data_in->created_date			= date("Y-m-d H:i:s");
		$id_in							= R::store( $data_in );

		$data_out						= R::xdispense( 'warehouse_stock' );
		$data_out->id_warehouse			= 1;
		$data_out->id_product_detail	= $param['product_detail_out'];
		$data_out->out					= $param['quantity'];
		$data_out->created_date			= date("Y-m-d H:i:s");
		$data_out->description			= "Replacement";
		$id_in							= R::store( $data_out );

		// update replacement
		$data							= R::load( 'warehouse_replacement', $param['id'] );
		$data->integrated				= 1;
		$id								= R::store( $data );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();