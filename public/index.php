<?php
include 'configuration/index.php';
require 'configuration/keys.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
use Slim\Http\UploadedFile;

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->post('/encode_image', function (Request $request, Response $response){
	try{
		$uploadedFiles = $request->getUploadedFiles();
		$myImages = $uploadedFiles['demo'];
		$myFiles = $myImages->file;
		$myMimes = getMimes($myImages);
		$imageData = base64_encode(file_get_contents($myFiles));
		$src = 'data: '.$myMimes.';base64,'.$imageData;
		return $response->withStatus(200)->write($src);
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

function getMimes(UploadedFile $uploadedFile){
    return $uploadedFile->getClientMediaType();
}

$app->get('/verified', function (Request $request, Response $response) use($app) {
  try {
	  $jwt = $request->getHeaders();
	  $key = "pmnLogisticManagementSystem-v5.0.0.2";
	  if(isset($jwt['HTTP_AUTHORIZATION'])){
		  $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], keys(), array('HS256'));
		  if ($decoded) {
		  	return $response->withStatus(200);
		  }
	  }else{
	  	return $response->withStatus(302)->withHeader('Location', 'https://www.youtube.com/watch?v=TeritLmAvLQ');
	  }
  } catch (Exception $e) {
	  	return $response->withStatus(500);
  }
});

$app->run();