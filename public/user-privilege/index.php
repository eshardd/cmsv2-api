<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$user = R::getAll("SELECT * FROM `user_privilege_group`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($user);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/access/{id}", function (Request $request, Response $response, $args){
	$privilege_group_id = $args['id'];
	try {
		$user_privilege_access = R::getAll("SELECT `id_user_privilege_menu` FROM `user_privilege_access` WHERE `id_user_privilege_group` = $privilege_group_id");
		$modify_array = array();
		foreach ($user_privilege_access as $key => $value) {
			$modify_array[] = $value['id_user_privilege_menu']; 
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($modify_array);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list", function (Request $request, Response $response){
	try {
		$privilege_list = R::getAll("SELECT * FROM `user_privilege_menu`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($privilege_list);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param								= $request->getParsedBody();
		$user_privilege_group				= R::xdispense( 'user_privilege_group' );
		$user_privilege_group->name 		= $param['name'];
		$id									= R::store( $user_privilege_group );

		foreach ($param['privilege'] as $key => $value) {
			$user_privilege_access								= R::xdispense( 'user_privilege_access' );
			$user_privilege_access->id_user_privilege_group 	= $id;
			$user_privilege_access->id_user_privilege_menu 		= $value;
			$user_privilege_access->permission 					= "R,W";
			R::store( $user_privilege_access );
		}



		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$id_record 						= $param['id'];
		$user_privilege_group 			= R::load( 'user_privilege_group', $param['id'] );
		$user_privilege_group->name 	= $param['name'];
		$id 							= R::store( $user_privilege_group );


		// delete list access sebelumnya
		R::exec( "DELETE FROM`user_privilege_access` WHERE `id_user_privilege_group` = $id_record" );

		foreach ($param['privilege'] as $key => $value) {
			$user_privilege_access								= R::xdispense( 'user_privilege_access' );
			$user_privilege_access->id_user_privilege_group 	= $id;
			$user_privilege_access->id_user_privilege_menu 		= $value;
			$user_privilege_access->permission 					= "R,W";
			R::store( $user_privilege_access );
		}
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();