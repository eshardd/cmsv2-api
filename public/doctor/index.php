<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$doctor = R::getAll("SELECT * FROM `doctor` ORDER BY id DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($doctor);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$doctor				= R::xdispense( 'doctor' );
		$doctor->name 		= $param['name'];
		$doctor->type		= $param['type'];
		$id					= R::store( $doctor );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Doctor has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$doctor 			= R::load( 'doctor', $param['id'] );
		$doctor->name 		= $param['name'];
		$doctor->type 		= $param['type'];
		$id 				= R::store( $doctor );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Doctor Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();