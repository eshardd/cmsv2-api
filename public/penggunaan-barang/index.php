<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$doctor = R::getAll("SELECT a.*,b.`name` AS `hospital_name`, c.`name` AS `doctor_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		WHERE a.`stage` > 5 order by a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($doctor);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post("/filter", function (Request $request, Response $response){
	try {
		$post				= $request->getParsedBody();
		$sql_date = '';
		$sql_month = '';
		$sql_year = '';

		if (isset($post['date']) && $post['date'] != null) {
			$date = str_pad($post['date'], 2, 0, STR_PAD_LEFT);
			$sql_date = 'AND day(a.`date`) = '.$date;
		}
		
		if (isset($post['month']) && $post['month'] != null) {
			$month = str_pad($post['month'], 2, 0, STR_PAD_LEFT);
			$sql_month = 'AND month(a.`date`) = '.$month;
		}
		
		if (isset($post['year']) && $post['year'] != null) {
			$year = $post['year'];
			$sql_year = 'AND YEAR(a.`date`) = '.$year;
		}

		$doctor = R::getAll("SELECT a.*,b.`name` AS `hospital_name`, c.`name` AS `doctor_name` FROM `rencana_operasi` a 
		LEFT JOIN `hospital` b ON a.`id_hospital` = b.`id`
		LEFT JOIN `doctor` c ON a.`id_doctor` = c.`id`
		WHERE a.`stage` > 5 $sql_date $sql_month $sql_year order by a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($doctor);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/detail/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id_ro = $args['id_ro'];
		$data = R::getAll("SELECT e.`name` AS `product_name`, a.*,b.`no_ro` ,c.`code_box`,d.`name` AS `product_detail_name`,d.`code` FROM `penggunaan_barang` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `box` c ON a.`id_box` = c.`id` 
		LEFT JOIN `product_detail` d ON a.`id_product_detail` = d.`id`
		LEFT JOIN `product` e ON c.`product` = e.`id`
		WHERE a.`id_ro` = $id_ro");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list_box/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id_ro = $args['id_ro'];
		$data = R::getAll("SELECT a.`id_box` AS `value` ,CONCAT(c.`name`,' - ',b.`code_box`) AS `label` FROM `rencana_operasi_detail` a 
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product` c ON b.`product` = c.`id` 
		LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		WHERE a.`id_ro` = $id_ro");
		// $product_detail = R::getAll("SELECT a.`id_product_detail` AS `value`,b.`name` AS `label`, a.`id_box`
		// FROM `box_detail` a
		// LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		//  WHERE a.`quantity` != 0 AND a.`id_box` = 28");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list_item/{id_box}", function (Request $request, Response $response, $args){
	try {
		$id_box = $args['id_box'];
		$data = R::getAll("SELECT a.`id_product_detail` AS `value`,b.`name` AS `label`
		FROM `box_detail` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE a.`id_box` = $id_box AND a.`quantity` != 0 ORDER BY a.`id` DESC");
		// $product_detail = R::getAll("SELECT a.`id_product_detail` AS `value`,b.`name` AS `label`, a.`id_box`
		// FROM `box_detail` a
		// LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		//  WHERE a.`quantity` != 0 AND a.`id_box` = 28");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_detail_name/{id}", function (Request $request, Response $response, $args){
	$id = $args['id'];
	try {
		$name = R::getRow("SELECT a.* FROM `product_detail` a WHERE a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($name);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/box_name/{id}", function (Request $request, Response $response, $args){
	$id = $args['id'];
	try {
		$name = R::getRow("SELECT a.* FROM `box` a WHERE a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($name);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$id_ro 				= $param['id_ro'];
		$id_user 			= $param['id_user'];

	
		if($id_ro){
			if (empty($param['data'])) {
				//Tanpa penggunaan
				$ro 					= R::load( 'rencana_operasi', $param['id_ro'] );
				$ro->stage 				= 6;
				$ro->progress 			= 80;
				$ro->integrated 		= 2;
				$ro->last_update		= date("Y-m-d H:i:s");
				$id 					= R::store( $ro );

				// log activity
					$log						= R::xdispense( 'rencana_operasi_log' );
					$log->id_rencana_operasi	= $id_ro;
					$log->user					= 0;
					$log->title					= '[Tidak ada Penggunaan]';
					$log->log					= "Rencana Operasi Tidak ada Penggunaan";
					$log->log_comment			= '';
					$log->date					= date("Y-m-d H:i:s");
					$id_log						= R::store( $log );	

					$log						= R::xdispense( 'rencana_operasi_log' );
					$log->id_rencana_operasi	= $id_ro;
					$log->user					= 0;
					$log->title					= '[Set Stage]';
					$log->log					= "Rencana Operasi Progess 'Menunggu Penarikan'";
					$log->log_comment			= '';
					$log->date					= date("Y-m-d H:i:s");
					$id_log						= R::store( $log );
				return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
			}else{
				foreach ($param['data'] as $key => $value) {
					$data						= R::xdispense( 'penggunaan_barang' );
					$data->id_ro 				= $param['id_ro'];
					$data->id_box 				= $value['id_box'];
					$data->id_product_detail 	= $value['product_detail'];
					$data->quantity 			= $value['quantity'];
					$id							= R::store( $data );
				}
				$ro 					= R::load( 'rencana_operasi', $param['id_ro'] );
				$ro->stage 				= 6;
				$ro->progress 			= 80;
				$id 					= R::store( $ro );


				$data_penggunaan = R::getAll("SELECT a.*, b.`code_box`,c.`name` AS `product_name`, d.`name` AS `product_detail_name`
				FROM `penggunaan_barang` a
				LEFT JOIN `box` b ON a.`id_box` = b.`id`
				LEFT JOIN `product` c ON b.`product` = c.`id`
				LEFT JOIN `product_detail` d ON a.`id_product_detail` = d.`id`
				WHERE a.`id_ro` = $id_ro");

				$log_text = '';
				foreach ($data_penggunaan as $key => $value) {
					$log_text .= "<strong>".$value['product_name']." - ".$value['code_box']."</strong> |".$value['product_detail_name']." = ".$value['quantity']."<br/>";
				}

				// log activity
				if($id){
					$id_user 					= $param['id_user'];
					$log						= R::xdispense( 'rencana_operasi_log' );
					$log->id_rencana_operasi	= $id_ro;
					$log->user					= $id_user;
					$log->title					= '[Add Penggunaan Barang]';
					$log->log					= "Add Penggunaan Barang";
					$log->log_comment			= $log_text;
					$log->date					= date("Y-m-d H:i:s");
					$id_log						= R::store( $log );

					$log						= R::xdispense( 'rencana_operasi_log' );
					$log->id_rencana_operasi	= $id_ro;
					$log->user					= 0;
					$log->title					= '[Set Stage]';
					$log->log					= "Rencana Operasi Progess 'Menunggu Penarikan'";
					$log->log_comment			= '';
					$log->date					= date("Y-m-d H:i:s");
					$id_log						= R::store( $log );
				}
				return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
			}
		}
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/penambahan_barang_baru', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$id_ro 				= $param['id_ro'];
		$id_user 			= $param['id_user'];

		if($id_ro){
			$data						= R::xdispense( 'penggunaan_barang' );
			$data->id_ro 				= $param['id_ro'];
			$data->id_box 				= $param['id_box'];
			$data->id_product_detail 	= $param['id_product_detail'];
			$data->quantity 			= $param['quantity'];
			$id							= R::store( $data );

			$ro = R::getRow("SELECT * FROM `rencana_operasi` WHERE `id` = ".$param['id_ro']);
			if($ro['integrated'] == 1){
				// langsung kurangin dari box nya
				$id_box 			= $param['id_box'];
				$id_product_detail 	= $param['id_product_detail'];

				//data isi box
				$data_box	 		= R::findOne( 'box_detail', ' id_box = :id_box AND id_product_detail = :id_product_detail ', [ ':id_product_detail' => $id_product_detail , ':id_box' => $id_box  ] );
				$data_box 			= R::load( 'box_detail', $data_box['id'] );
				
				// data isi buffer
				$data_buffer	 		= R::findOne( 'box_buffer', ' id_box = :id_box AND id_product_detail = :id_product_detail ', [ ':id_product_detail' => $id_product_detail , ':id_box' => $id_box  ] );
				$new_qty 			= $data_box['quantity'] - $param['quantity'];

				// kalo udah minus
				if($new_qty < 0){
					// jika ada buffernya. kurangilah si buffer itu
					if(isset($data_buffer)){

						$minusbuffer = $data_buffer['quantity'] + $new_qty;
						
						//kalo buffernya bisa mencukupi
						if($minusbuffer > -1){
							$data_buffer->quantity = $minusbuffer;
							$new_qty = 0;
						}else{
							$new_qty = 0 + $minusbuffer;
							$minusbuffer = 0;
						}
					}
				}
				
				$data_box->quantity = $new_qty;
				
				// kalau ada pemakaian buffer
				if(isset($minusbuffer)){
					R::store( $data_buffer );	
				}
				R::store( $data_box );
			}

			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
			
		}
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/tambahan', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data						= R::xdispense( 'penggunaan_barang' );
		$data->id_ro 				= $param['id'];
		$data->id_product_detail 	= $param['product_detail'];
		$data->quantity 			= $param['quantity'];
		$id							= R::store( $data );

		// cek apa sudah di sync. bila sudah. langsung ngurangi stok aja
		$ro = R::getRow("SELECT * FROM `rencana_operasi` WHERE `id` = ".$param['id']);
		if($ro['integrated'] == 1){
			// jika ada barang yg dipake. bukan buffer dan bukan item box. maka stok wh berkurang
			$ro_detail 							= R::getRow("SELECT * FROM `rencana_operasi_detail` WHERE `id_ro` = ".$param['id']." GROUP BY `id_ro`");
			$box	 							= R::findOne( 'box', ' id = :id ', [ ':id' => $ro_detail['id_box']  ] );
			
			$wh = 1;
			if(isset($box['id_warehouse'])){
				$wh = $box['id_warehouse'];
			}
			
			$warehouse_stock					= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse		= $wh;
			$warehouse_stock->out 				= $param['quantity'];
			$warehouse_stock->id_product_detail = $param['product_detail'];
			$warehouse_stock->description 		= "Request alat dari RO nomor :".$ro['no_ro'];
			$warehouse_stock->created_date 		= date("Y-m-d H:i:s");
			$id					= R::store( $warehouse_stock );
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_ro						= $param['id_ro'];
		$data 						= R::load( 'penggunaan_barang', $param['id'] );
		
		if(isset($param['id_box'])){
			$data->id_box 				= $param['id_box'];
		}
		
		if(isset($param['id_product_detail'])){
			$data->id_product_detail 	= $param['id_product_detail'];
		}
		
		$data->quantity 			= $param['quantity'];
		$id 						= R::store( $data );

		$data_penggunaan = R::getAll("SELECT a.*, b.`code_box`,c.`name` AS `product_name`, d.`name` AS `product_detail_name`
		FROM `penggunaan_barang` a
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product` c ON b.`product` = c.`id`
		LEFT JOIN `product_detail` d ON a.`id_product_detail` = d.`id`
		WHERE a.`id_ro` = $id_ro");

		$log_text = '';
		foreach ($data_penggunaan as $key => $value) {
			$log_text .= "<strong>".$value['product_name']." - ".$value['code_box']."</strong> |".$value['product_detail_name']." = ".$value['quantity']."<br/>";
		}

		// log activity
		if($id){
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $id_ro;
			$log->user					= $id_user;
			$log->title					= '[Add Penarikan]';
			$log->log					= "Start Penarikan Barang";
			$log->log_comment			= $log_text;
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		}

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_alat_pendukung', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'penggunaan_barang', $param['id'] );
		$data->quantity 			= $param['quantity_pendukung'];
		$id 						= R::store( $data );
	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/revisi', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'rencana_operasi', $param['id_ro'] );
		$data->integrated 			= 0;
		$data->stage 				= 8;
		$id 						= R::store( $data );
	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_ro', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 						= R::load( 'rencana_operasi', $param['id_ro'] );
		$data->pasien 				= $param['name'];
		$data->no_mr 				= $param['no_mr'];
		$data->case 				= $param['case'];
		$data->subcase 				= $param['subcase'];
		$id 						= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/delete/{id}', function ($request, $response, $args) {
    try {
	    $id 	= $args['id'];
	    $box 	= R::load( 'penggunaan_barang', $id );

		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->put('/syncronize', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$id_ro 						= $param['id'];
		$data 						= R::getAll("SELECT * FROM `penggunaan_barang` WHERE `id_ro` = $id_ro");

		if($data){
			foreach ($data as $key => $value) {

				if(isset($value['id_box'])){
					// kalau penggunaan barang dalam box
					$id_box 			= $value['id_box'];
					$id_product_detail 	= $value['id_product_detail'];

					//data isi box
					$data_box	 		= R::findOne( 'box_detail', ' id_box = :id_box AND id_product_detail = :id_product_detail ', [ ':id_product_detail' => $id_product_detail , ':id_box' => $id_box  ] );
					$data_box 			= R::load( 'box_detail', $data_box['id'] );
					// data isi buffer
					$data_buffer	 		= R::findOne( 'box_buffer', ' id_box = :id_box AND id_product_detail = :id_product_detail ', [ ':id_product_detail' => $id_product_detail , ':id_box' => $id_box  ] );

					$new_qty 			= $data_box['quantity'] - $value['quantity'];

					// kalo udah minus
					if($new_qty < 0){
						// jika ada buffernya. kurangilah si buffer itu
						if(isset($data_buffer)){

							$minusbuffer = $data_buffer['quantity'] + $new_qty;
							
							//kalo buffernya bisa mencukupi
							if($minusbuffer > -1){
								$data_buffer->quantity = $minusbuffer;
								$new_qty = 0;
							}else{
								$new_qty = 0 + $minusbuffer;
								$minusbuffer = 0;
							}
						}
					}
					
					$data_box->quantity = $new_qty;
					
					// kalau ada pemakaian buffer
					if(isset($minusbuffer)){
						R::store( $data_buffer );	
					}
					R::store( $data_box );
					
				}else{
					//penggunaan alat pendukung
					$id_ro 	= $id_ro;

					// get nomor RO
					$ro = R::getRow("SELECT `no_ro` from `rencana_operasi` WHERE `id` = ".$id_ro);
					$no_ro = $ro['no_ro'];

					$pemakaian 	= $value['quantity'];
					$summary_pemakaian = 0;
					$id_product_detail 	= $value['id_product_detail'];
					$data_alat_pendukung	= R::getAll("SELECT * FROM `rencana_operasi_alat_pendukung` WHERE `id_ro` = $id_ro");

					// jika yg gaada box nya itu adalah alat pendukung
					if($data_alat_pendukung){
						foreach ($data_alat_pendukung as $key => $value_alat) {
							if($value_alat['id_product_detail'] == $id_product_detail){
								$summary_pemakaian = $value_alat['quantity'] - $value['quantity'];
								// kurangi wh langsung
								$warehouse_stock					= R::xdispense( 'warehouse_stock' );
								$warehouse_stock->id_warehouse		= 1;
								$warehouse_stock->in 				= $summary_pemakaian;
								$warehouse_stock->out 				= 0;
								$warehouse_stock->id_product_detail = $id_product_detail;
								$warehouse_stock->description 		= "Pengembalian dari RO nomor :".$no_ro;
								$warehouse_stock->created_date 		= date("Y-m-d H:i:s");
								$id					= R::store( $warehouse_stock );
							}else{
								// jika yang dibawa tidak dipake. masuk di proses ini
								$warehouse_stock					= R::xdispense( 'warehouse_stock' );
								$warehouse_stock->id_warehouse		= 1;
								$warehouse_stock->in 				= $value_alat['quantity'];
								$warehouse_stock->out 				= 0;
								$warehouse_stock->id_product_detail = $value_alat['id_product_detail'];
								$warehouse_stock->description 		= "Pengembalian dari RO nomor :".$no_ro;
								$warehouse_stock->created_date 		= date("Y-m-d H:i:s");
								$id					= R::store( $warehouse_stock );
							}
						}
					}


					// jika ada barang yg dipake. bukan buffer dan bukan item box. maka stok wh berkurang
					$box	 		= R::findOne( 'box', ' id = :id ', [ ':id' => $id_box  ] );
					$warehouse_stock					= R::xdispense( 'warehouse_stock' );
					$warehouse_stock->id_warehouse		= $box['id_warehouse'];
					$warehouse_stock->out 				= $pemakaian;
					$warehouse_stock->id_product_detail = $id_product_detail;
					$warehouse_stock->description 		= "Request alat dari RO nomor :".$no_ro;
					$warehouse_stock->created_date 		= date("Y-m-d H:i:s");
					$id					= R::store( $warehouse_stock );
				}
			}
			$data_ro 					= R::load( 'rencana_operasi', $id_ro );
			$data_ro->integrated 		= 1;
			$data_ro->stage 			= 9;
			$data_ro->progress 			= 100;
			$id 						= R::store( $data_ro );

			// log activity
			if($id){

				$data_penggunaan = R::getAll("SELECT a.*, b.`code_box`,c.`name` AS `product_name`, d.`name` AS `product_detail_name`
				FROM `penggunaan_barang` a
				LEFT JOIN `box` b ON a.`id_box` = b.`id`
				LEFT JOIN `product` c ON b.`product` = c.`id`
				LEFT JOIN `product_detail` d ON a.`id_product_detail` = d.`id`
				WHERE a.`id_ro` = $id_ro");

				$log_text = '';
				foreach ($data_penggunaan as $key => $value) {
					$log_text .= "<strong>".$value['product_name']." - ".$value['code_box']."</strong> |".$value['product_detail_name']." = ".$value['quantity']."<br/>";
				}

				$id_user 					= $param['id_user'];
				$log						= R::xdispense( 'rencana_operasi_log' );
				$log->id_rencana_operasi	= $id_ro;
				$log->user					= $id_user;
				$log->title					= '[synchronize penggunaan]';
				$log->log					= "synchronize Penggunaan barang";
				$log->log_comment			= $log_text;
				$log->date					= date("Y-m-d H:i:s");
				$id_log						= R::store( $log );

				$log						= R::xdispense( 'rencana_operasi_log' );
				$log->id_rencana_operasi	= $id_ro;
				$log->user					= 0;
				$log->title					= '[Set Stage]';
				$log->log					= "Rencana Operasi DONE..!!!";
				$log->log_comment			= '';
				$log->date					= date("Y-m-d H:i:s");
				$id_log						= R::store( $log );
			}
			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data Update success'));
		}else{
			$data_ro 					= R::load( 'rencana_operasi', $id_ro );
			$data_ro->integrated 		= 2;
			$data_ro->stage 			= 9;
			$data_ro->progress 			= 100;
			$id 						= R::store( $data_ro );

			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $id_ro;
			$log->user					= 0;
			$log->title					= '[Set Stage]';
			$log->log					= "Rencana Operasi DONE..!!!";
			$log->log_comment			= 'Tanpa Penggunaan Barang';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
			return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data Update success'));
		}
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();