<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all_product", function (Request $request, Response $response, $args){
	try {
		$product = R::getAll("SELECT a.`id` AS `value`, CONCAT(b.`name`,' - ', a.`name`) AS `label` FROM `product` a
		LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$hospital = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `hospital`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('product'=>$product,'hospital'=>$hospital));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/standard/{id_product}/{id_warehouse}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$id_warehouse = $args['id_warehouse'];
		$product = R::getAll("SELECT a.*, b.`name` AS `product_name`, c.`name` AS `product_detail_name`, d.`name` AS `warehouse_name` FROM `economic_order` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id`
		LEFT JOIN `warehouse` d ON a.`warehouse` = d.`id` WHERE a.`product` = $id_product AND a.`warehouse` = $id_warehouse");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_implant', function (Request $request, Response $response){
	try{
		$param							= $request->getParsedBody();
		$box_standard					= R::xdispense( 'economic_order' );
		$box_standard->product 			= $param['product'];
		$box_standard->warehouse 		= $param['warehouse'];
		$box_standard->product_detail	= $param['product_detail'];
		$box_standard->quantity			= $param['quantity'];
		$box_standard->type				= 1;
		$id								= R::store( $box_standard );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add_instrument', function (Request $request, Response $response){
	try{
		$param					= $request->getParsedBody();
		$data					= R::xdispense( 'economic_order' );
		$data->product 			= $param['product'];
		$data->warehouse 		= $param['warehouse'];
		$data->product_detail	= $param['product_detail'];
		$data->quantity			= $param['quantity'];
		$data->type				= 2;
		$id						= R::store( $data );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit_standard', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$box 						= R::load( 'economic_order', $param['id'] );
		$box->product_detail		= $param['product_detail'];
		$box->quantity				= $param['quantity'];
		$box->type					= $param['type'];
		$id 						= R::store( $box );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->delete('/standard/{id}', function ($request, $response, $args) {
    try {
	    $id 	= $args['id'];
	    $box 	= R::load( 'economic_order', $id );

		R::trash( $box );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id,'message'=>'Data Delete success'));
    } catch (Exception $e) {
    	
    }
});

$app->get("/dropdown_filter", function (Request $request, Response $response){
	try {
		$warehouse 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `warehouse`");
		$product 				= R::getAll("SELECT a.`id` AS `value`, CONCAT(b.`name`,' - ', a.`name`) AS `label` FROM `product` a
									LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$status 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `box_status`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'warehouse'=>$warehouse,
			'product'=>$product,
			'status'=>$status,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->get("/alert_all", function (Request $request, Response $response, $args){
	try {
		$data = R::getAll("SELECT a.*,b.`name` AS `product_name`,c.`name` AS `product_detail_name`,
		(
			(
				SELECT (SUM(`in`)-SUM(`out`)) 
				FROM `warehouse_stock` 
				WHERE `id_product_detail` = a.`product_detail`
				AND `id_warehouse` = 1
			)
			+
			(
				SELECT SUM(aa.`quantity`) 
				FROM `box_detail` aa 
				LEFT JOIN `box` bb ON aa.`id_box` = bb.`id` 
				LEFT JOIN `warehouse` cc ON bb.`id_warehouse` = cc.`id` 
				WHERE bb.`id_warehouse` = 1 
				AND aa.`id_product_detail` = a.`product_detail`
			)
		) AS `sisa_wh_and_box` 
		FROM `economic_order` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();