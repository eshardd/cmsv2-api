<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response, $args){
	try {
		$spb = R::getAll("SELECT g.`no_ro`,h.`name` AS `doctor_name`,i.`name` AS `hospital_name`,a.*,d.`name` AS `courier_name`,d.`name` AS `created_by_name`,CONCAT(f.`nomor_polisi`,' - ',f.`name`) AS `kendaraan` FROM `spb` a
		LEFT JOIN `user` d ON a.`courier` = d.`id`
		LEFT JOIN `user` e ON a.`created_by` = e.`id`
		LEFT JOIN `kendaraan` f ON a.`no_kendaraan` = f.`id` 
		LEFT JOIN `rencana_operasi` g ON a.`id_ro` = g.`id`
		LEFT JOIN `doctor` h ON g.`id_doctor` = h.`id`
		LEFT JOIN `hospital` i on g.`id_hospital` = i.`id`
		ORDER BY a.`id` DESC");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($spb);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list_box/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$data = R::getAll("SELECT a.*,b.`no_ro`,c.`name` AS `product_name`,d.`code_box` FROM `rencana_operasi_detail` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product` c ON a.`id_product` = c.`id`
		LEFT JOIN `box` d ON a.`id_box` = d.`id`
		WHERE a.`id_ro` = $ro_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list_alat_pendukung/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$data = R::getAll("SELECT a.*,b.`name` AS `product_detail_name` FROM `rencana_operasi_alat_pendukung` a 
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE a.`id_ro` = $ro_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/printview/{id}", function (Request $request, Response $response, $args){
	try {
		$spb_id = $args['id'];
		$data = R::getAll("SELECT c.`no_spb`,d.`no_ro`,c.`etd`,c.`eta`,e.`name` AS `created_by_name`,a.*, b.`code_box`,f.`name` AS `product_name` FROM `rencana_operasi_detail` a 
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `spb` c ON a.`id_ro` = c.`id_ro`
		LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		LEFT JOIN `user` e ON c.`created_by` = e.`id`
		LEFT JOIN `product` f ON a.`id_product` = f.`id`
		WHERE c.`id` = $spb_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/list/{id}", function (Request $request, Response $response, $args){
	try {
		$ro_id = $args['id'];
		$rencana_operasi = R::getRow("SELECT a.*,
				b.`name` AS `doctor_name`,
				c.`name` AS `hospital_name`,
				d.`name` AS `spine_anatomi_name`,
				e.`name` AS `subcase_name`,
				f.`name` AS `case_name`,
				i.`name` AS `jenis_tagihan_name`,
				j.`name` AS `user_name`,
	            k.`name` AS `box_status_name`,
	            l.`name` AS `status_ro_name`,
	            m.`name` AS `type_ro_name`,
	            n.`name` AS `technical_support_name`
			FROM `rencana_operasi` a
				LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
				LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
				LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
				LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
				LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
				LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
				LEFT JOIN `user` j ON a.`created_by` = j.`id`
	            LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
	            LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
	            LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
	            LEFT JOIN `user` n ON a.`technical_support` = n.`id`
			WHERE a.`id` = $ro_id
				GROUP BY a.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($rencana_operasi);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$ro 		= R::getAll("SELECT `id` AS `value`, `no_ro` AS `label` FROM `rencana_operasi` WHERE `status_ro` = 1 AND `stage` = 3 ORDER BY `no_ro` DESC");
		$kurir 		= R::getAll("SELECT `id` AS `value`, `name` AS `label` FROM `user` WHERE `division` IN('5','3')");
		$kendaraan 	= R::getAll("SELECT `id` AS `value`, CONCAT(`nomor_polisi`,' - ',`name`) AS `label` FROM `kendaraan`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'rencana_operasi' => $ro, 
			'kurir' => $kurir,
			'kendaraan' => $kendaraan
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `spb`");

		if ($ai['id'] == null) {
			$new_ai = 1;
		}else{
			$new_ai = $ai['id']+1;
		}

		$param						= $request->getParsedBody();
		$spb						= R::xdispense( 'spb' );
		$spb->no_spb				= 'PMN.SPB/'.date('Ymd').'/'.$new_ai;
		$spb->id_ro					= $param['id_ro'];
		$spb->courier				= $param['kurir'];
		$spb->no_kendaraan			= $param['no_kendaraan'];
		$spb->etd					= date('Y-m-d H:i:s', strtotime($param['start']));
		$spb->eta					= date('Y-m-d H:i:s', strtotime($param['estimasi']));
		$spb->created_date			= date("Y-m-d H:i:s");
		$spb->created_by			= $param['id_user'];
		$id 						= R::store( $spb );

		//kurangi stok alat pendukung
		$alat_pendukung 			= R::getAll("SELECT * FROM `rencana_operasi_alat_pendukung` where `id_ro` = ".$param['id_ro']);

		foreach ($alat_pendukung as $key_pendukung => $value_pendukung) {
			$quantity_alat_pendukung 				= $value_pendukung['quantity'];
			$warehouse_stock						= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse			= 1;
			$warehouse_stock->id_product_detail		= $value_pendukung['id_product_detail'];
			$warehouse_stock->in					= 0;
			$warehouse_stock->out					= $value_pendukung['quantity'];
			$warehouse_stock->description			= "pengiriman SPB nomor : ".$spb->no_spb;
			$warehouse_stock->created_date			= date("Y-m-d H:i:s");
			$warehouse_stock->created_by			= $param['id_user'];
			$id_wh_stok 							= R::store( $warehouse_stock );
		}

		if($id){
			// log activity
			$getdata_spb = R::getRow("SELECT a.*,b.`name` AS `courier_name`,
			CONCAT(c.`name`,' - ',c.`nomor_polisi`) AS `kendaraan`,
			DATE_FORMAT(a.`etd`,'%W %M %e %Y %h:%i:%s') AS `estimasi_keberangkatan`,
			DATE_FORMAT(a.`eta`,'%W %M %e %Y %h:%i:%s') AS `estimasi_tiba`
			FROM `spb` a 
			LEFT JOIN `user` b ON a.`courier` = b.`id`
			LEFT JOIN `kendaraan` c ON a.`no_kendaraan` = c.`id`
			WHERE a.`id` = $id");


			$log_text = "";
			$log_text .= "Kurir : <strong>".$getdata_spb['courier_name']."</strong><br/>";
			$log_text .= "Kendaraan : <strong>".$getdata_spb['kendaraan']."</strong><br/>";
			$log_text .= "Estimasi Keberangkatan : <strong>".$getdata_spb['estimasi_keberangkatan']."</strong><br/>";
			$log_text .= "Estimasi Waktu Tiba : <strong>".$getdata_spb['estimasi_tiba']."</strong>";

			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id_ro'];
			$log->user					= $id_user;
			$log->title					= '[Add SPB]';
			$log->log					= "Create 'Surat Pengiriman Barang' with no : '".$spb->no_spb."'";
			$log->log_comment			= $log_text;
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );

			// log activity
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $param['id_ro'];
			$log->user					= 0;
			$log->title					= '[Set Stage]';
			$log->log					= "Rencana Operasi Progress 'Delivery time'";
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );



			// data email
			$param =  R::getRow("SELECT g.`no_ro`,h.`name` AS `doctor_name`,i.`name` AS `hospital_name`,a.*,d.`name` AS `courier_name`,d.`name` AS `created_by_name`,CONCAT(f.`nomor_polisi`,' - ',f.`name`) AS `kendaraan` FROM `spb` a
			LEFT JOIN `user` d ON a.`courier` = d.`id`
			LEFT JOIN `user` e ON a.`created_by` = e.`id`
			LEFT JOIN `kendaraan` f ON a.`no_kendaraan` = f.`id` 
			LEFT JOIN `rencana_operasi` g ON a.`id_ro` = g.`id`
			LEFT JOIN `doctor` h ON g.`id_doctor` = h.`id`
			LEFT JOIN `hospital` i ON g.`id_hospital` = i.`id`
			WHERE a.`id` = $id");
		
			// tambah receipent
			$receipnt =  R::getAll("SELECT * FROM `user` WHERE `division` IN ('3','5')");
			$addCC = array();
			$receipnt_email = array();
			foreach ($receipnt as $key => $value) {
				$addCC[$value['email']] = $value['name'];
				$receipnt_email[] = $value['email'];
			}
			$receipnt_log_text = implode(', ',$receipnt_email);


			sendmailCC('prpebri@gmail.com', $addCC , 'Pebri', '[Logistic System] Pengiriman Barang', template_email_spb('Pebri' ,'prpebri@gmail.com', $param));
		}

		$rencana_operasi 			= R::load( 'rencana_operasi', $param['id_ro'] );
		$rencana_operasi->progress	= 40;
		$rencana_operasi->stage		= 4;
		$id 						= R::store( $rencana_operasi );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$spb 						= R::load( 'spb', $param['id'] );
		$spb->no_kendaraan 			= $param['no_kendaraan'];
		$spb->courier				= $param['kurir'];
		$spb->etd					= date('Y-m-d H:i:s', strtotime($param['etd']));
		$spb->eta					= date('Y-m-d H:i:s', strtotime($param['eta']));
		if($param['receiver'] != ''){
			$spb->receiver			= $param['receiver'];
			$spb->atd 				= date("Y-m-d H:i:s");

			$rencana_operasi 			= R::load( 'rencana_operasi', $spb['id_ro'] );
			$rencana_operasi->progress	= 60;
			$rencana_operasi->stage		= 5;
			$id 						= R::store( $rencana_operasi );
		
		
			// log activity
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $spb['id_ro'];
			$log->user					= $id_user;
			$log->title					= '[Update SPB]';
			$log->log					= "Set Box Arrived. Receiver : '".$spb->receiver."'";
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );

			// log activity
			$id_user 					= $param['id_user'];
			$log						= R::xdispense( 'rencana_operasi_log' );
			$log->id_rencana_operasi	= $spb['id_ro'];
			$log->user					= 0;
			$log->title					= '[Set Stage]';
			$log->log					= "Rencana Operasi Progress 'Arrived & Waiting to use'";
			$log->log_comment			= '';
			$log->date					= date("Y-m-d H:i:s");
			$id_log						= R::store( $log );
		
		}
		$id							= R::store( $spb );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Doctor Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();