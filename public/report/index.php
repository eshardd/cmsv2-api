<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/top_principle/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(e.`date`) = ".$args['filter'];
		}

		$principle_list = R::getAll("SELECT * FROM `principle`");
		$data 	= R::getAll("SELECT MONTH(e.`date`) as `bulan`,d.`id` AS `id_principle`,b.`name` AS `product_detail_name`,d.`name` AS `principle_name`,c.`name` AS `product_name`,a.`id_ro`,SUM(a.`quantity`) AS `total_penggunaan` FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `principle` d ON c.`id_principle` = d.`id`
		LEFT JOIN `rencana_operasi` e ON a.`id_ro` = e.`id`
		where 1=1 $where
		GROUP BY a.`id_product_detail`
		ORDER BY SUM(a.`quantity`) DESC");

		$raw = R::getAll("SELECT MONTH(e.`date`) AS `bulan`,
		e.`no_ro` AS `no_ro`,
		e.`date` AS ro_date,
		f.`code_box`,
		h.`name` AS `hospital_name`,
		i.`name` AS `doctor_name`,
		e.`pasien`,
		e.`no_mr`,
		g.`name` AS `ts_name`,
		d.`id` AS `id_principle`,
		a.`id_product_detail`,
		b.`name` AS `product_detail_name`,
		d.`name` AS `principle_name`,
		c.`name` AS `product_name`,
		a.`id_ro`,
		a.`quantity`
		FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `principle` d ON c.`id_principle` = d.`id`
		LEFT JOIN `rencana_operasi` e ON a.`id_ro` = e.`id`
		LEFT JOIN `box` f ON a.`id_box` = f.`id`
		LEFT JOIN `user` g ON e.`technical_support` = g.`id`
		LEFT JOIN `hospital` h ON e.`id_hospital` = h.`id`
		LEFT JOIN `doctor` i ON e.`id_doctor` = i.`id`
		WHERE a.`id_product_detail` IS NOT NULL");


		$new_arr = array();
		foreach ($principle_list as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data as $key_d => $value_d) {
				if($value_d['id_principle'] == $value_p['id'] && $counter < 5){
					$new_arr[$key_p]['data'][] = $value_d;
					$counter++;
				}	
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$raw,'data'=>$new_arr));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/area_sales/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(b.`date`) = ".$args['filter'];
		}

		$subarea = R::getAll("SELECT * FROM `subarea`");
		$data 	= R::getAll("SELECT 
		MONTH(b.`date`) AS `bulan`,
		f.`id` AS `subarea_id`,
		f.`name` AS `subarea_name`,
		c.`name` AS `hospital_name`,
		COUNT(f.`id`) AS `total_operasi`,
		SUM(a.`quantity`) AS `total_penggunaan` 
		FROM `penggunaan_barang` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
		LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
		LEFT JOIN `area` e ON c.`area` = e.`id`
		LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
		WHERE f.`id` IS NOT NULL $where 
		GROUP BY f.`id`,c.`id`");

		$raw = R::getAll("SELECT 
		f.`id` AS `subarea_id`,
		f.`name` AS `subarea_name`,
		c.`name` AS `hospital_name`,
		b.`no_ro`,
		b.`date`,
		d.`name` AS `doctor_name`,
		b.`pasien`,
		h.`name` AS `product_name`,
		i.`code_box`,
		g.`name` AS `product_detail_name`,
		a.`quantity`
		FROM `penggunaan_barang` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
		LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
		LEFT JOIN `area` e ON c.`area` = e.`id`
		LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
		LEFT JOIN `product_detail` g ON a.`id_product_detail` = g.`id`
		LEFT JOIN `product` h ON g.`id_product` = h.`id`
		LEFT JOIN `box` i ON a.`id_box` = i.`id`
		WHERE f.`id` IS NOT NULL");

		$new_arr = array();
		foreach ($subarea as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data as $key => $value) {
				if ($value_p['id'] == $value['subarea_id'] && $counter < 5) {
					$new_arr[$key_p]['data'][] = $value;
					$counter++;
				}
			}
		}
		//$new_arr = $data;
		

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$raw,'data'=>$new_arr));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_quantity/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(b.`date`) = ".$args['filter'];
		}

		$type = R::getAll("SELECT `type` FROM `doctor` GROUP BY `type`");
		$data 	= R::getAll("SELECT 
		d.`id`,
		d.`name` AS `name`,
		d.`type` AS `doctor_type`,
		(SELECT COUNT(aa.`id`) FROM `rencana_operasi` aa WHERE `id_doctor` = d.`id`) AS `total_operasi`,
		SUM(a.`quantity`) AS `total_penggunaan`
		FROM `penggunaan_barang` a 
				LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
				LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
				LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
				LEFT JOIN `area` e ON c.`area` = e.`id`
				LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
				WHERE f.`id` IS NOT NULL $where 
				GROUP BY d.`id`
		order by SUM(a.`quantity`) DESC");


		$raw_doctor = R::getAll("SELECT
		d.`id`,
		d.`name` AS `name`,
		d.`type` AS `doctor_type`,
		b.`no_ro`,
		b.`date`,
		i.`name` AS `hospital_name`,
		b.`pasien`,
		b.`no_mr`,
		h.`name` AS `product_name`,
		g.`code` AS `code1`,
		g.`code_x` AS `code2`,
		j.`code_box`,
		g.`name` AS `product_detail_name`,
		a.`quantity`
		FROM `penggunaan_barang` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
		LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
		LEFT JOIN `area` e ON c.`area` = e.`id`
		LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
		LEFT JOIN `product_detail` g ON a.`id_product_detail` = g.`id`
		LEFT JOIN `product` h ON g.`id_product` = h.`id`
		LEFT JOIN `hospital` i ON b.`id_hospital` = i.`id`
		LEFT JOIN `box` j ON a.`id_box` = j.`id`
		WHERE f.`id` IS NOT NULL");

		$new_arr = array();
		foreach ($type as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data as $key => $value) {
				if ($value_p['type'] == $value['doctor_type'] && $counter < 5) {
					$new_arr[$key_p]['data'][] = $value;
					$counter++;
				}
			}
		}

		$type_rs = R::getAll("SELECT `type` FROM `hospital` GROUP BY `type`");
		$data_rs = R::getAll("SELECT
		c.`name` AS `name`,
		c.`type` AS `hospital_type`,
		COUNT(a.`id_ro`) AS `total_operasi`,
		SUM(a.`quantity`) AS `total_penggunaan`
		FROM `penggunaan_barang` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
		LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
		LEFT JOIN `area` e ON c.`area` = e.`id`
		LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
		WHERE f.`id` IS NOT NULL 
		GROUP BY c.`id`
		ORDER BY SUM(a.`quantity`) DESC");

		$raw_rs = R::getAll("SELECT
		c.`name` AS `name`,
		c.`type` AS `hospital_type`,
		e.`name` AS `area_name`,
		f.`name` AS `subarea_name`,
		g.`name` AS `doctor_name`,
		b.`no_ro`,
		b.`date`,
		b.`pasien`,
		b.`no_mr`,
		h.`code` AS `code1`,
		h.`code_x` AS `code2`,
		i.`code_box`,
		h.`name` AS `product_detail_name`,
		a.`quantity`
		FROM `penggunaan_barang` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `hospital` c ON b.`id_hospital` = c.`id`
		LEFT JOIN `doctor` d ON b.`id_doctor` = d.`id`
		LEFT JOIN `area` e ON c.`area` = e.`id`
		LEFT JOIN `subarea` f ON c.`subarea` = f.`id`
		LEFT JOIN `doctor` g ON b.`id_doctor` = g.`id`
		LEFT JOIN `product_detail` h ON a.`id_product_detail` = h.`id`
		LEFT JOIN `box` i on a.`id_box` = i.`id`
		WHERE f.`id` IS NOT NULL");

		$new_arr_rs = array();
		foreach ($type_rs as $key_p => $value_p) {
			$new_arr_rs[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data_rs as $key => $value) {
				if ($value_p['type'] == $value['hospital_type'] && $counter < 5) {
					$new_arr_rs[$key_p]['data'][] = $value;
					$counter++;
				}
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'doctor'=>$new_arr,
			'hospital'=>$new_arr_rs,
			'raw'=>$raw_doctor,
			'raw_hospital'=>$raw_rs,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/delivery_performance/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(b.`date`) = ".$args['filter'];
		}

		$kurir = R::getAll("SELECT b.`id`,b.`name` FROM spb a
		LEFT JOIN `user` b ON a.`courier` = b.`id`
		GROUP BY b.`id`");

		$data 	= R::getAll("SELECT 
		b.`no_ro`,
		a.`courier`,
		c.`name` AS `courier_name`,
		b.`date` AS `operation_date`,
		a.`etd`,
		a.`eta`,
		a.`atd`,
		TIMESTAMPDIFF(HOUR, a.`eta`, a.`atd`)AS `delivery_time`,
		TIMESTAMPDIFF(HOUR, b.`date`, a.`atd`) AS `durasi`
		FROM `spb` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `user` c ON a.`courier` = c.`id`
		WHERE b.`stage` = 9 $where
		AND b.`status_ro` = 1");

		$new_arr = array();
		foreach ($kurir as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter_telat = 0;
			$counter_ontime = 0;
			foreach ($data as $key => $value) {
				if ($value_p['id'] == $value['courier']) {
					$new_arr[$key_p]['data'][] = $value;
					
					if($value['durasi'] < 0){
						$counter_ontime++;
					}elseif ($value['durasi'] > 0) {
						$counter_telat++;
					}
				}
			}
			$new_arr[$key_p]['info']['ontime'] = $counter_ontime;
			$new_arr[$key_p]['info']['telat'] = $counter_telat;
		}


		$data_raw = $data 	= R::getAll("SELECT 
		a.*,
		b.`no_ro`,
		a.`courier`,
		c.`name` AS `courier_name`,
		b.`date` AS `operation_date`,
		TIMESTAMPDIFF(HOUR, a.`eta`, a.`atd`)AS `delivery_time`,
		TIMESTAMPDIFF(HOUR, b.`date`, a.`atd`) AS `durasi`
		FROM `spb` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `user` c ON a.`courier` = c.`id`
		WHERE b.`stage` = 9
		AND b.`status_ro` = 1");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$data_raw,'data'=>$new_arr));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/instrument_rusak/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(a.`created_date`) = ".$args['filter'];
		}

		$principle_list = R::getAll("SELECT * FROM `principle`");
		$data 	= R::getAll("SELECT 
		MONTH(a.`created_date`) AS `bulan`,
		d.`id` AS `id_principle`,
		b.`name` AS `product_detail_name`,
		d.`name` AS `principle_name`,
		c.`name` AS `product_name`,
		SUM(a.`quantity`) AS `total_penggunaan` 
		FROM `instrument_rusak` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `principle` d ON c.`id_principle` = d.`id`
		where 1=1 $where
		GROUP BY a.`id_product_detail`
		ORDER BY SUM(a.`quantity`) DESC");

		$new_arr = array();
		foreach ($principle_list as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data as $key_d => $value_d) {
				if($value_d['id_principle'] == $value_p['id']){
					$new_arr[$key_p]['data'][] = $value_d;
					$counter++;
				}	
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$data,'data'=>$new_arr));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/top_instrument/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
			$where2 = '';
		}else{			
			$where = "AND MONTH(e.`date`) = ".$args['filter'];
			$where2 = "AND MONTH(ddd.`date`) = ".$args['filter'];
		}

		$raw = R::getAll("SELECT 
		a.*,
		f.`name` AS `principle_name`,
		d.`no_ro`,
		d.`date`,
		g.`name` AS `hospital_name`,
		h.`name` AS `doctor_name`,
		d.`pasien`,
		d.`no_mr`,
		e.`code_box`,
		c.`name` AS `product_name`,
		b.`code` AS `code1`,
		b.`code_x` AS `code2`,
		b.`name` AS `product_detail_name` 
		FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		LEFT JOIN `box` e ON a.`id_box` = e.`id`
		LEFT JOIN `principle` f ON c.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON d.`id_hospital` = g.`id`
		LEFT JOIN `doctor` h ON d.`id_hospital` = h.`id`
		WHERE a.`id_product_detail` IS NOT NULL
		");

		// $raw = R::getAll("SELECT 
		// a.*,
		// f.`name` AS `principle_name`,
		// d.`no_ro`,
		// d.`date`,
		// g.`name` AS `hospital_name`,
		// h.`name` AS `doctor_name`,
		// d.`pasien`,
		// d.`no_mr`
		// FROM `penggunaan_barang` a
		// LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		// LEFT JOIN `product` c ON b.`id_product` = c.`id`
		// LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		// LEFT JOIN `box` e ON a.`id_box` = e.`id`
		// LEFT JOIN `principle` f ON c.`id_principle` = f.`id`
		// LEFT JOIN `hospital` g ON d.`id_hospital` = g.`id`
		// LEFT JOIN `doctor` h ON d.`id_hospital` = h.`id`
		// WHERE a.`id_product_detail` IS NOT NULL AND a.`id_ro` IS NOT NULL
		// GROUP BY a.`id_ro`");

		$data 	= R::getAll("SELECT 
		MONTH(e.`date`) AS `bulan`,
		d.`name` AS `principle_name`,
		c.`name` AS `product_name`,
		(SELECT COUNT(aa.`id`) AS `penggunaan` FROM (
			SELECT MONTH(ddd.`date`) AS `bulan`,aaa.`id_ro`,ccc.`id`
			FROM `penggunaan_barang` aaa
			LEFT JOIN `product_detail` bbb ON aaa.`id_product_detail` = bbb.`id`
			LEFT JOIN `product` ccc ON bbb.`id_product` = ccc.`id`
			LEFT JOIN `rencana_operasi` ddd ON aaa.`id_ro` = ddd.`id` 
			WHERE aaa.`id_ro` IS NOT NULL $where2
			GROUP BY aaa.`id_ro`) aa 
			WHERE aa.`id` = c.`id`) AS `operasi`
		FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `principle` d ON c.`id_principle` = d.`id`
		LEFT JOIN `rencana_operasi` e ON a.`id_ro` = e.`id`
		WHERE 1=1 AND d.`id` IS NOT NULL
		GROUP BY c.`id`
		ORDER BY (SELECT COUNT(aa.`id`) AS `penggunaan` FROM (
			SELECT aaa.`id_ro`,ccc.`id`
			FROM `penggunaan_barang` aaa
			LEFT JOIN `product_detail` bbb ON aaa.`id_product_detail` = bbb.`id`
			LEFT JOIN `product` ccc ON bbb.`id_product` = ccc.`id`
			WHERE aaa.`id_ro` IS NOT NULL
			GROUP BY aaa.`id_ro`) aa 
			WHERE aa.`id` = c.`id`) DESC limit 0,5
		");


		// $new_arr = array();
		// foreach ($principle_list as $key_p => $value_p) {
		// 	$new_arr[$key_p]['info'] = $value_p;
		// 	$counter = 0;
		// 	foreach ($data as $key_d => $value_d) {
		// 		if($value_d['id_principle'] == $value_p['id'] && $counter < 5){
		// 			$new_arr[$key_p]['data'][] = $value_d;
		// 			$counter++;
		// 		}	
		// 	}
		// }

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$raw,'data'=>$data));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/top_implant/{filter}", function (Request $request, Response $response, $args){
	try {

		if($args['filter'] == 'all'){
			$where = '';
		}else{			
			$where = "AND MONTH(d.`date`) = ".$args['filter'];
		}

		$product_list = R::getAll("SELECT * FROM `product` WHERE `type` IS NOT NULL AND `name` != 'UNKNOWN'");

		$data 	= R::getAll("SELECT d.`date`,
		c.`id` AS `id_product`,
		a.*,
		f.`name` AS `principle_name`,
		d.`no_ro`,
		e.`code_box`,
		c.`name` AS `product_name`,
		b.`name` AS `product_detail_name`,
		SUM(a.`quantity`) AS `total_penggunaan`
		FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		LEFT JOIN `box` e ON a.`id_box` = e.`id`
		LEFT JOIN `principle` f ON c.`id_principle` = f.`id`
		WHERE a.`id_product_detail` IS NOT NULL $where
		GROUP BY a.`id_product_detail`
		ORDER BY SUM(a.`quantity`) DESC ");

		$raw = R::getAll("SELECT 
		c.`id` AS `id_product`,
		a.*,
		f.`name` AS `principle_name`,
		d.`no_ro`,
		d.`date`,
		g.`name` AS `hospital_name`,
		h.`name` AS `doctor_name`,
		d.`pasien`,
		d.`no_mr`,
		e.`code_box`,
		c.`name` AS `product_name`,
		b.`code` AS `code1`,
		b.`code_x` AS `code2`,
		b.`name` AS `product_detail_name`
		FROM `penggunaan_barang` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		LEFT JOIN `product` c ON b.`id_product` = c.`id`
		LEFT JOIN `rencana_operasi` d ON a.`id_ro` = d.`id`
		LEFT JOIN `box` e ON a.`id_box` = e.`id`
		LEFT JOIN `principle` f ON c.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON d.`id_hospital` = g.`id`
		LEFT JOIN `doctor` h ON d.`id_doctor` = h.`id`
		WHERE a.`id_product_detail` IS NOT NULL");


		$new_arr = array();
		foreach ($product_list as $key_p => $value_p) {
			$new_arr[$key_p]['info'] = $value_p;
			$counter = 0;
			foreach ($data as $key_d => $value_d) {
				if($value_d['id_product'] == $value_p['id'] && $counter < 5){
					$new_arr[$key_p]['data'][] = $value_d;
					$counter++;
				}	
			}
		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('raw'=>$raw,'data'=>$new_arr));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();