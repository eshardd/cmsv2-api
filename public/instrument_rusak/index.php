<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$area 	= R::getAll("SELECT a.*,b.`name` AS `product_name`, c.`name` AS `product_detail_name`, d.`name` AS `created_by_name` FROM `instrument_rusaK` a 
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		ORDER BY `id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($area);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$area						= R::xdispense( 'instrument_rusak' );
		$area->id_product 			= $param['id_product'];
		$area->id_product_detail 	= $param['id_product_detail'];
		$area->quantity 			= $param['quantity'];
		$area->description 			= $param['description'];
		$area->created_date 		= date("Y-m-d H:i:s");
		$area->created_by 			= $param['created_by'];
		$id					= R::store( $area );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$area 				= R::load( 'instrument_rusak', $param['id'] );
		$area->name 		= $param['name'];
		$id 						= R::store( $area );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$product 		= R::getAll("SELECT a.`id` AS `value`,CONCAT(b.`name`,' - ',a.`name`) AS `label` FROM `product` a
		LEFT JOIN `principle` b ON a.`id_principle` = b.`id`");
		$product_detail = R::getAll("SELECT a.`id` AS `value`, a.`name` AS `label`, a.`id_product` FROM `product_detail` a where a.`type` = 2");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array("product"=>$product,"product_detail"=>$product_detail));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();