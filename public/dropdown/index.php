<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/wh", function (Request $request, Response $response){
	try {
		$warehouse 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `warehouse`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'warehouse'=>$warehouse
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/replacement", function (Request $request, Response $response){
	try {
		$product 				= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product`");
		$product_detail			= R::getAll("SELECT `id` AS `value`, `name` as `label`, `id_product` FROM `product_detail`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'product'=>$product,
			'product_detail'=>$product_detail
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/box/{product}", function (Request $request, Response $response, $args){
	try {
		$id_product 	= $args['product'];
		$box = R::getAll("SELECT a.*,a.`id` AS `value`, CONCAT(a.`code_box` ,' - ', b.`name`) AS `label`, b.`name` AS `product_name`, d.`name` AS `created_by_name`, e.`name` AS `status_name`, f.`name` AS `principle_name`,c.`name` AS `warehouse_name`, g.`name` AS `hospital_name`
		FROM `box` a
		LEFT JOIN `product` b ON a.`product` = b.`id`
		LEFT JOIN `warehouse` c ON a.`id_warehouse` = c.`id`
		LEFT JOIN `user` d ON a.`created_by` = d.`id`
		LEFT JOIN `box_status` e ON a.`status` = e.`id`
		LEFT JOIN `principle` f ON b.`id_principle` = f.`id`
		LEFT JOIN `hospital` g ON a.`stay` = g.`id`
		WHERE a.`product` = $id_product AND a.`status` = 2
		ORDER BY a.`code_box` ASC");

		$new_array = array();
		foreach ($box as $key => $value) {
			$id_si_box = $value['id'];
			$check = R::getRow("SELECT * FROM `box_detail` WHERE `id_box` = $id_si_box AND `quantity` != `standard`");

			if($check){
				$value['condition'] = 'Not Complete';
			}else{
				$value['condition'] = 'Complete';
			}

			$checktotal = R::getRow("SELECT SUM(`quantity`) AS `jumlah` FROM `box_detail` WHERE `id_box` = $id_si_box");

			if($checktotal['jumlah'] == 0){
				$value['condition'] = 'Empty';
			}
			


			$new_array[] = $value;
		}
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'box'=>$new_array,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/penggunaan-barang", function (Request $request, Response $response){
	try {
		$rencana_operasi		= R::getAll("SELECT `id` AS `value`, `no_ro` AS `label` FROM `rencana_operasi` WHERE `status_ro` = 1 AND `stage` = 5 order by `id` DESC");
		$product				= R::getAll("SELECT a.`id_ro`,a.`id_box` AS `value`,CONCAT(b.`name`,' - ', c.`code_box`) AS `label` FROM `rencana_operasi_detail` a
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `box` c ON a.`id_box` = c.`id` ");
		$alat_pendukung			= R::getAll("SELECT a.`id` AS `value`,b.`name` AS `label`
		FROM `box_detail` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE a.`id_box` = 28 AND a.`quantity` != 0");

		$product_all 			= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `product`");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'rencana_operasi'=>$rencana_operasi,
			'product'=>$product,
			'product_all'=>$product_all
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/penggunaan-barang/{id_box}", function (Request $request, Response $response, $args){
	try {
		$id_box = $args['id_box'];
		$product_detail			= R::getAll("SELECT a.`id_product_detail` AS `value`,b.`name` AS `label`,b.`code`
		FROM `box_detail` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE a.`id_box` = $id_box AND a.`quantity` != 0 AND b.`type` = 1
		UNION
		SELECT a.`id_product_detail` AS `value` , b.`name` AS `label` , b.`code`
		FROM `box_buffer` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE a.`id_box` = $id_box AND a.`quantity` != 0");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'list'=>$product_detail
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/alat-pendukung/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id_ro = $args['id_ro'];
		$alat_pendukung			= R::getAll("SELECT a.`id_product_detail` AS `value`, b.`name` AS `label`,b.`code`
		FROM `rencana_operasi_alat_pendukung` a
		LEFT JOIN `product_detail` b ON a.`id_product_detail` = b.`id`
		WHERE `id_ro` = $id_ro");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($alat_pendukung);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_principle/{id_product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['id_product'];
		$product_detail	= R::getAll("SELECT a.`id` AS `value`, a.`name` AS `label`,a.`code`
		FROM `product_detail` a
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		WHERE a.`id_product` = $id_product");
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('product_detail'=>$product_detail));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();