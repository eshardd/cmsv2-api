<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$data = R::getAll("SELECT a.*,b.`name` AS `warehouse_name` FROM `warehouse_stock_opname` a
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post("/filterdate", function (Request $request, Response $response){
	try {
		$param						= $request->getParsedBody();
		
		$wh = $param['warehouse'];
		$year = $param['year'];
		$month = $param['month'];

		$sql_month = "AND b.`id` = $wh";
		$sql_year = "AND YEAR(a.`date`) = $year";
		$sql_wh = "AND MONTH(a.`date`) = $month";

		$data = R::getAll("SELECT a.*,b.`name` AS `warehouse_name` FROM `warehouse_stock_opname` a
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id`
		WHERE 1=1 $sql_month $sql_year $sql_wh ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($data);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `warehouse_stock_opname`");

		if ($ai['id'] == null) {
			$new_ai = 1;
		}else{
			$new_ai = $ai['id']+1;
		}

		$param						= $request->getParsedBody();
		$data						= R::xdispense( 'warehouse_stock_opname' );
		$data->id_warehouse 		= $param['warehouse'];
		$data->date					= $param['date'];
		$data->no_so				= 'PMN.SO/'.date('Ymd').'/'.$new_ai;
		$data->description			= $param['description'];
		$data->created_date			= date("Y-m-d H:i:s");
		$data->created_by			= $param['user_id'];
		$id							= R::store( $data );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$data 				= R::load( 'warehouse_stock_opname', $param['id'] );
		$data->id_warehouse	= $param['warehouse'];
		$data->no_so		= $param['no_so'];
		$data->date			= $param['date'];
		$data->description	= $param['description'];
		$data->status		= $param['status'];
		if($param['status'] == 1 && $data['finish_date'] == null){			
			$data->finish_date	= date("Y-m-d H:i:s");
		}
		$id 				= R::store( $data );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Data Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();