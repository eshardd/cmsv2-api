<?php
include '../configuration/index.php';
//include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

$app->get("/rencana_operasi/{id}", function (Request $request, Response $response, $args){
	try {
		$id_ro = $args['id'];

		$rencana_operasi = R::getRow("SELECT a.*,
				b.`name` AS `doctor_name`,
				c.`name` AS `hospital_name`,
				d.`name` AS `spine_anatomi_name`,
				e.`name` AS `subcase_name`,
				f.`name` AS `case_name`,
				i.`name` AS `jenis_tagihan_name`,
				j.`name` AS `user_name`,
	            k.`name` AS `box_status_name`,
	            l.`name` AS `status_ro_name`,
	            m.`name` AS `type_ro_name`,
	            n.`name` AS `technical_support_name`
			FROM `rencana_operasi` a
				LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
				LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
				LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
				LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
				LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
				LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
				LEFT JOIN `user` j ON a.`created_by` = j.`id`
	            LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
	            LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
	            LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
	            LEFT JOIN `user` n ON a.`technical_support` = n.`id`
			WHERE a.`id` = $id_ro
				GROUP BY a.`id`");

		$product_list = R::getAll("SELECT c.`name` AS `product_name` FROM `rencana_operasi_detail` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product` c ON a.`id_product` = c.`id`
		LEFT JOIN `box` d ON a.`id_box` = d.`id`
		LEFT JOIN `principle` e ON c.`id_principle` = e.`id`
		WHERE a.`id_ro` = $id_ro");

		$arr = array();
		foreach ($product_list as $key => $value) {
			$arr[$key] = $value['product_name'];
		}

		$request_instrument_name = implode(',',$arr);
	

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		$styleThinBlackBorderOutline = [
			'borders' => [
				'outline' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => 'FF000000'],
				],
			],
		];

		// ======================================== Kop Surat ===================================================
		$spreadsheet->setActiveSheetIndex(0);

		// logo PMN
		$drawing = new Drawing();
		$drawing->setPath(__DIR__ . '/img/logopmn.png');
		$drawing->setHeight(70);
		$drawing->setCoordinates('A1');
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// Title
		$spreadsheet->getActiveSheet()->setCellValue('D1', 'PT Patriot Medika Nusantara');
			// style D1
			$spreadsheet->getActiveSheet()->getStyle('D1')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('D1')->getFont()->setSize(16);
			$spreadsheet->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('D1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);

		$spreadsheet->getActiveSheet()->setCellValue('D2', 'Jl. Kelapa dua, Tangerang - Banten 15810');
		$spreadsheet->getActiveSheet()->setCellValue('D3', 'Fax : (021) - 7257608');
		$spreadsheet->getActiveSheet()->setCellValue('D4', 'Telp : (021) - 29015270');
			// style D2 - D4
			$spreadsheet->getActiveSheet()->getStyle('D2')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('D2')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('D2')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('D3')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('D3')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('D3')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('D4')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('D4')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('D4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		
		$spreadsheet->getActiveSheet()->setCellValue('J2', 'No Form: PMN. LOG - FR.001');
		
		// ==================================== END KOP SURAT =======================================================


		// ======================================= BODY 1 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A6', 'FORM REQUEST OPERASI');
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getFont()->setSize(16);
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
		
		$spreadsheet->getActiveSheet()->getStyle('a6:L7')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('a6:L7')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('a6:L7')->getFill()->getStartColor()->setARGB('ffd896');
		
		// merge
		$spreadsheet->getActiveSheet()->mergeCells('A6:L7');
		
		// isi
		$spreadsheet->getActiveSheet()->mergeCells('C9:D9');
		$spreadsheet->getActiveSheet()->setCellValue('C9', 'Diajukan Oleh');
		$spreadsheet->getActiveSheet()->setCellValue('E9', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F9:L9');
		$spreadsheet->getActiveSheet()->setCellValue('F9', $rencana_operasi['user_name']);
		
		$spreadsheet->getActiveSheet()->mergeCells('C10:D10');
		$spreadsheet->getActiveSheet()->setCellValue('C10', 'Tanggal Pengajuan');
		$spreadsheet->getActiveSheet()->setCellValue('E10', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F10:L10');
		$spreadsheet->getActiveSheet()->setCellValue('F10', $rencana_operasi['created_date']);

		$spreadsheet->getActiveSheet()->mergeCells('C11:D11');
		$spreadsheet->getActiveSheet()->setCellValue('C11', 'Tanggal Operasi');
		$spreadsheet->getActiveSheet()->setCellValue('E11', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F11:L11');
		$spreadsheet->getActiveSheet()->setCellValue('F11', $rencana_operasi['date']);

		$spreadsheet->getActiveSheet()->mergeCells('C12:D12');
		$spreadsheet->getActiveSheet()->setCellValue('C12', 'Tipe');
		$spreadsheet->getActiveSheet()->setCellValue('E12', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F12:L12');
		$spreadsheet->getActiveSheet()->setCellValue('F12', $rencana_operasi['type_ro_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C13:D13');
		$spreadsheet->getActiveSheet()->setCellValue('C13', 'Status Box');
		$spreadsheet->getActiveSheet()->setCellValue('E13', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F13:L13');
		$spreadsheet->getActiveSheet()->setCellValue('F13', $rencana_operasi['box_status_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C14:D14');
		$spreadsheet->getActiveSheet()->setCellValue('C14', 'Status Rencana Operasi');
		$spreadsheet->getActiveSheet()->setCellValue('E14', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F14:L14');
		$spreadsheet->getActiveSheet()->setCellValue('F14', $rencana_operasi['status_ro_name']);

			// style D1
			$spreadsheet->getActiveSheet()->getStyle('C9:E14')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('C9:E14')->getFont()->setSize(11);
			$spreadsheet->getActiveSheet()->getStyle('C9:E14')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('C9:E14')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('C9:C14')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
			$spreadsheet->getActiveSheet()->getStyle('C9:C14')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
			$spreadsheet->getActiveSheet()->getStyle('D9:D14')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
			$spreadsheet->getActiveSheet()->getStyle('D9:D14')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

			$spreadsheet->getActiveSheet()->getStyle('F9:F14')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('F9:F14')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F9:F14')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
			$spreadsheet->getActiveSheet()->getStyle('F9:F14')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
		// ======================================= END BODY 1 ==================================================

		// ======================================= BODY 2 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A16', 'INFORMATION DETAIL REQUEST');
		$spreadsheet->getActiveSheet()->mergeCells('A16:L16');
		$spreadsheet->getActiveSheet()->mergeCells('A17:L17');
		$spreadsheet->getActiveSheet()->getStyle('A16')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A16')->getFont()->setSize(16);
		$spreadsheet->getActiveSheet()->getStyle('A16')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A16')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A16')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A16')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A16:L16')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A16:L16')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A16:L16')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A17:L17')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A17:L17')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A17:L17')->getFill()->getStartColor()->setARGB('000000');
		
		// isi
		$spreadsheet->getActiveSheet()->mergeCells('C18:D18');
		$spreadsheet->getActiveSheet()->setCellValue('C18', 'Rumah Sakit');
		$spreadsheet->getActiveSheet()->setCellValue('E18', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F18:L18');
		$spreadsheet->getActiveSheet()->setCellValue('F18', $rencana_operasi['hospital_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C19:D19');
		$spreadsheet->getActiveSheet()->setCellValue('C19', 'Dokter');
		$spreadsheet->getActiveSheet()->setCellValue('E19', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F19:L19');
		$spreadsheet->getActiveSheet()->setCellValue('F19', $rencana_operasi['doctor_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C20:D20');
		$spreadsheet->getActiveSheet()->setCellValue('C20', 'Pasien');
		$spreadsheet->getActiveSheet()->setCellValue('E20', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F20:L20');
		$spreadsheet->getActiveSheet()->setCellValue('F20', $rencana_operasi['pasien']);

		$spreadsheet->getActiveSheet()->mergeCells('C21:D21');
		$spreadsheet->getActiveSheet()->setCellValue('C21', 'No Mr');
		$spreadsheet->getActiveSheet()->setCellValue('E21', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F21:L21');
		$spreadsheet->getActiveSheet()->setCellValue('F21', $rencana_operasi['no_mr']);

		$spreadsheet->getActiveSheet()->mergeCells('C22:D22');
		$spreadsheet->getActiveSheet()->setCellValue('C22', 'Spine Anatomi');
		$spreadsheet->getActiveSheet()->setCellValue('E22', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F22:L22');
		$spreadsheet->getActiveSheet()->setCellValue('F22', $rencana_operasi['spine_anatomi_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C23:D23');
		$spreadsheet->getActiveSheet()->setCellValue('C23', 'Case');
		$spreadsheet->getActiveSheet()->setCellValue('E23', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F23:L23');
		$spreadsheet->getActiveSheet()->setCellValue('F23', $rencana_operasi['case_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C24:D24');
		$spreadsheet->getActiveSheet()->setCellValue('C24', 'Subcase');
		$spreadsheet->getActiveSheet()->setCellValue('E24', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F24:L24');
		$spreadsheet->getActiveSheet()->setCellValue('F24', $rencana_operasi['subcase_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C25:D25');
		$spreadsheet->getActiveSheet()->setCellValue('C25', 'Request Instrument');
		$spreadsheet->getActiveSheet()->setCellValue('E25', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F25:L25');
		$spreadsheet->getActiveSheet()->setCellValue('F25', $request_instrument_name);

		$spreadsheet->getActiveSheet()->mergeCells('C26:D26');
		$spreadsheet->getActiveSheet()->setCellValue('C26', 'Remarks');
		$spreadsheet->getActiveSheet()->setCellValue('E26', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F26:L26');
		$spreadsheet->getActiveSheet()->setCellValue('F26', $rencana_operasi['remarks']);

		$spreadsheet->getActiveSheet()->mergeCells('C27:D27');
		$spreadsheet->getActiveSheet()->setCellValue('C27', 'Jenis Tagihan');
		$spreadsheet->getActiveSheet()->setCellValue('E27', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F27:L27');
		$spreadsheet->getActiveSheet()->setCellValue('F27', $rencana_operasi['jenis_tagihan_name']);

		$spreadsheet->getActiveSheet()->mergeCells('C28:D28');
		$spreadsheet->getActiveSheet()->setCellValue('C28', 'Technical Support');
		$spreadsheet->getActiveSheet()->setCellValue('E28', ':');
		$spreadsheet->getActiveSheet()->mergeCells('F28:L28');
		$tsname = '';
		if($rencana_operasi['technical_support_name']){
			$tsname = $rencana_operasi['technical_support_name'];
		}else{			
			$tsname = 'Non TS';
		}
		$spreadsheet->getActiveSheet()->setCellValue('F28', $tsname);

		// 	// style D1

			$spreadsheet->getActiveSheet()->getStyle('C18:F28')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('C18:F28')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('C18:F28')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
			$spreadsheet->getActiveSheet()->getStyle('C18:F28')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

			$spreadsheet->getActiveSheet()->getStyle('D18:D28')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
			$spreadsheet->getActiveSheet()->getStyle('D18:D28')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
		// ======================================= END BODY 2 ==================================================

		// ======================================= FOOTER ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('B38', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('B38:D41');
		$spreadsheet->getActiveSheet()->setCellValue('F38', 'Warehouse');
		$spreadsheet->getActiveSheet()->mergeCells('F38:H41');
		$spreadsheet->getActiveSheet()->setCellValue('J38', 'Quality Control');
		$spreadsheet->getActiveSheet()->mergeCells('J38:L41');

		$spreadsheet->getActiveSheet()->getStyle('B38:L41')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('B38:L41')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('B38:L41')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('B38:L41')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

		$spreadsheet->getActiveSheet()->getStyle('B38:D41')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('B38:D41')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('F38:H41')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('F38:H41')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('J38:L41')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('J38:L41')->getFill()->setFillType(Fill::FILL_SOLID);
		// ======================================= END FOOTER ==================================================

		// Set column widths
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(1);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(8);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(2);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(8);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(8);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(2);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(8);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(8);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(5);
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('7')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(5);
		$spreadsheet->getActiveSheet()->getRowDimension('9')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('12')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('13')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(5);
		$spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('17')->setRowHeight(5);
		$spreadsheet->getActiveSheet()->getRowDimension('18')->setRowHeight(15);
		//=============================================================================================



		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/checklist_box/{id}/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id_box = $args['id'];
		$id_ro = $args['id_ro'];

		$box = R::getAll("SELECT a.*, b.`code_box`,c.`code` AS `code`,c.`name` AS `product_detail_name`, c.`type`
		FROM `box_detail` a 
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		WHERE a.`id_box` = $id_box AND a.`standard` != 0 
		");

		$rencana_operasi = R::getRow("SELECT a.*,
		b.`name` AS `doctor_name`,
		c.`name` AS `hospital_name`,
		d.`name` AS `spine_anatomi_name`,
		e.`name` AS `subcase_name`,
		f.`name` AS `case_name`,
		i.`name` AS `jenis_tagihan_name`,
		j.`name` AS `user_name`,
		k.`name` AS `box_status_name`,
		l.`name` AS `status_ro_name`,
		m.`name` AS `type_ro_name`,
		n.`name` AS `technical_support_name`
		FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `spine_anatomi` d ON a.`id_spine_anatomi` = d.`id`
		LEFT JOIN `diagnosa_subcase` e ON a.`subcase` = e.`id`
		LEFT JOIN `diagnosa_case` f ON a.`case` = f.`id`
		LEFT JOIN `jenis_tagihan` i ON a.`jenis_tagihan` = i.`id`
		LEFT JOIN `user` j ON a.`created_by` = j.`id`
		LEFT JOIN `box_status` k ON a.`status_box` = k.`id`
		LEFT JOIN `rencana_operasi_status` l ON a.`status_ro` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON a.`type_ro` = m.`id`
		LEFT JOIN `user` n ON a.`technical_support` = n.`id`
		WHERE a.`id` = $id_ro
		GROUP BY a.`id`");

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		$styleThinBlackBorderOutline = [
			'borders' => [
				'outline' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => 'FF000000'],
				],
			],
		];

		// ======================================== Kop Surat ===================================================
		$spreadsheet->setActiveSheetIndex(0);

		// logo PMN
		$drawing = new Drawing();
		$drawing->setPath(__DIR__ . '/img/logopmn.png');
		$drawing->setHeight(70);
		$drawing->setCoordinates('A1');
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// Title
		$spreadsheet->getActiveSheet()->setCellValue('F1', 'PT Patriot Medika Nusantara');
		// style F1
			$spreadsheet->getActiveSheet()->mergeCells('F1:R1');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			
		$spreadsheet->getActiveSheet()->mergeCells('F2:R2');
		$spreadsheet->getActiveSheet()->mergeCells('F3:R3');
		$spreadsheet->getActiveSheet()->mergeCells('F4:R4');
		$spreadsheet->getActiveSheet()->setCellValue('F2', 'Jl. Kelapa dua, Tangerang - Banten 15810');
		$spreadsheet->getActiveSheet()->setCellValue('F3', 'Fax : (021) - 7257608');
		$spreadsheet->getActiveSheet()->setCellValue('F4', 'Telp : (021) - 29015270');
			// style D2 - D4
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		
		$spreadsheet->getActiveSheet()->setCellValue('S2', 'No : '.$rencana_operasi['no_ro']);


		$spreadsheet->getActiveSheet()->mergeCells('B6:F6'); $spreadsheet->getActiveSheet()->setCellValue('G6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B6', 'Nama Dokter');
		$spreadsheet->getActiveSheet()->mergeCells('H6:N6');
		$spreadsheet->getActiveSheet()->setCellValue('H6', $rencana_operasi['doctor_name']);
		
		$spreadsheet->getActiveSheet()->mergeCells('B7:F7'); $spreadsheet->getActiveSheet()->setCellValue('G7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B7', 'Rumah sakit');
		$spreadsheet->getActiveSheet()->mergeCells('H7:N7');
		$spreadsheet->getActiveSheet()->setCellValue('H7', $rencana_operasi['hospital_name']);

		$spreadsheet->getActiveSheet()->mergeCells('B8:F8'); $spreadsheet->getActiveSheet()->setCellValue('G8', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B8', 'Tanggal');
		$spreadsheet->getActiveSheet()->mergeCells('H8:N8');
		$spreadsheet->getActiveSheet()->setCellValue('H8', $rencana_operasi['date']);


		$spreadsheet->getActiveSheet()->mergeCells('P6:T6'); $spreadsheet->getActiveSheet()->setCellValue('U6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P6', 'Nama Pasien');
		$spreadsheet->getActiveSheet()->mergeCells('V6:Z6');
		$spreadsheet->getActiveSheet()->setCellValue('v6', $rencana_operasi['pasien']);

		$spreadsheet->getActiveSheet()->mergeCells('P7:T7'); $spreadsheet->getActiveSheet()->setCellValue('U7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P7', 'No RO');
		$spreadsheet->getActiveSheet()->mergeCells('V7:Z7');
		$spreadsheet->getActiveSheet()->setCellValue('v7', $rencana_operasi['no_mr']);
		
		// ==================================== END KOP SURAT =======================================================

		// ======================================= BODY 1 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A10', 'INSTRUMENT');
		$spreadsheet->getActiveSheet()->mergeCells('A10:AC10');
		$spreadsheet->getActiveSheet()->mergeCells('A11:AC11');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->getStartColor()->setARGB('000000');
	

		$spreadsheet->getActiveSheet()->mergeCells('A13:H13'); $spreadsheet->getActiveSheet()->setCellValue('A13', 'CODE');
		$spreadsheet->getActiveSheet()->mergeCells('I13:Y13'); $spreadsheet->getActiveSheet()->setCellValue('I13', 'ITEM NAME');
		$spreadsheet->getActiveSheet()->mergeCells('Z13:AA13'); $spreadsheet->getActiveSheet()->setCellValue('Z13', 'Qty');
		$spreadsheet->getActiveSheet()->mergeCells('AB13:AC13'); $spreadsheet->getActiveSheet()->setCellValue('AB13', 'Std');
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$startrow = 14;
		foreach ($box as $key => $value) {

			if($value['type'] == 2){

				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow.':H'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow.'', $value['code']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow.':Y'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow.'', $value['product_detail_name']);
				$spreadsheet->getActiveSheet()->mergeCells('Z'.$startrow.':AA'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('Z'.$startrow.'', $value['standard']);
				$spreadsheet->getActiveSheet()->mergeCells('AB'.$startrow.':AC'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('AB'.$startrow.'', $value['quantity']);
				$startrow++;
			}
		}

		$merge2 = $startrow+1;
		$merge2_black = $merge2+1; $spreadsheet->getActiveSheet()->getRowDimension($merge2_black)->setRowHeight(2);

		$spreadsheet->getActiveSheet()->setCellValue('A'.$merge2.'', 'IMPLANT');
		$spreadsheet->getActiveSheet()->mergeCells('A'.$merge2.':AC'.$merge2.'');
		$spreadsheet->getActiveSheet()->mergeCells('A'.$merge2_black.':AC'.$merge2_black.'');
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->getFill()->getStartColor()->setARGB('000000');

		$startrow2 = $merge2_black+2;

		foreach ($box as $key => $value) {
			if($value['type'] == 1){

				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow2.':H'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow2.'', $value['code']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow2.':Y'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow2.'', $value['product_detail_name']);
				$spreadsheet->getActiveSheet()->mergeCells('Z'.$startrow2.':AA'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('Z'.$startrow2.'', $value['standard']);
				$spreadsheet->getActiveSheet()->mergeCells('AB'.$startrow2.':AC'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('AB'.$startrow2.'', $value['quantity']);
				$startrow2++;
			}
		}

		// ======================================= FOOTER ==================================================
		//header
		
		$startbox = $startrow2+2;
		$endbox = $startbox+4;
		$spreadsheet->getActiveSheet()->setCellValue('B'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('B'.$startbox.':H'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('K'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('K'.$startbox.':Q'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('T'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('T'.$startbox.':Z'.$endbox.'');

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);
		// ======================================= END FOOTER ==================================================

		// Set column widths
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(3);
		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('7')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('9')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(2);
		$spreadsheet->getActiveSheet()->getRowDimension('12')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('13')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('17')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('18')->setRowHeight(15);
		//=============================================================================================



		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/spb/{id}/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$id_ro = $args['id_ro'];

		$data_pengiriman = R::getRow("SELECT a.`no_spb`,b.`no_ro`,c.`name`,CONCAT(d.`name`,' - ',d.`nomor_polisi`) AS `kendaraan`,
		DATE_FORMAT(a.`etd`,'%d %M %Y %h:%i%s') AS etd
		FROM `spb` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `user` c ON a.`courier` = c.`id`
		LEFT JOIN `kendaraan` d ON a.`no_kendaraan` = d.`id`
		WHERE a.`id` = $id");

		// $item_pengiriman = R::getAll("SELECT a.*,
		// b.`no_ro`,
		// c.`name` AS `product_name`,
		// d.`code_box`,
		// e.`name` AS `dibuat_oleh`
		// FROM `rencana_operasi_detail` a
		// LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		// LEFT JOIN `product` c ON a.`id_product` = c.`id`
		// LEFT JOIN `box` d ON a.`id_box` = d.`id`
		// LEFT JOIN `user` e ON b.`created_by` = e.`id`
		// WHERE a.`id_ro` = $id_ro");

		$item_pengiriman = R::getAll("SELECT a.`id`, b.`no_ro`, c.`name` AS `product_name`, d.`code_box`, e.`name` AS `dibuat_oleh` 
		FROM `rencana_operasi_detail` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id` 
		LEFT JOIN `product` c ON a.`id_product` = c.`id` 
		LEFT JOIN `box` d ON a.`id_box` = d.`id` 
		LEFT JOIN `user` e ON b.`created_by` = e.`id` 
		WHERE a.`id_ro` = $id_ro
		UNION
		SELECT a.`id`, b.`no_ro`, c.`name` AS `product_name`, a.`quantity` AS `code_box`, e.`name` AS `dibuat_oleh`
		FROM `rencana_operasi_alat_pendukung` a 
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id` 
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id` 
		LEFT JOIN `user` e ON b.`created_by` = e.`id` 
		WHERE a.`id_ro` = $id_ro");

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		$styleThinBlackBorderOutline = [
			'borders' => [
				'outline' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => 'FF000000'],
				],
			],
		];

		// ======================================== Kop Surat ===================================================
		$spreadsheet->setActiveSheetIndex(0);

		// logo PMN
		$drawing = new Drawing();
		$drawing->setPath(__DIR__ . '/img/logopmn.png');
		$drawing->setHeight(70);
		$drawing->setCoordinates('A1');
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// Title
		$spreadsheet->getActiveSheet()->setCellValue('F1', 'PT Patriot Medika Nusantara');
		// style F1
			$spreadsheet->getActiveSheet()->mergeCells('F1:R1');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			
		$spreadsheet->getActiveSheet()->mergeCells('F2:R2');
		$spreadsheet->getActiveSheet()->mergeCells('F3:R3');
		$spreadsheet->getActiveSheet()->mergeCells('F4:R4');
		$spreadsheet->getActiveSheet()->setCellValue('F2', 'Jl. Kelapa dua, Tangerang - Banten 15810');
		$spreadsheet->getActiveSheet()->setCellValue('F3', 'Fax : (021) - 7257608');
		$spreadsheet->getActiveSheet()->setCellValue('F4', 'Telp : (021) - 29015270');
			// style D2 - D4
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		
		$spreadsheet->getActiveSheet()->setCellValue('S2', 'No : '.$data_pengiriman['no_ro']);


		$spreadsheet->getActiveSheet()->mergeCells('B6:F6'); $spreadsheet->getActiveSheet()->setCellValue('G6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B6', 'Nama Driver');
		$spreadsheet->getActiveSheet()->mergeCells('H6:N6');
		$spreadsheet->getActiveSheet()->setCellValue('H6', $data_pengiriman['name']);
		
		$spreadsheet->getActiveSheet()->mergeCells('B7:F7'); $spreadsheet->getActiveSheet()->setCellValue('G7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B7', 'No Kendaraan');
		$spreadsheet->getActiveSheet()->mergeCells('H7:N7');
		$spreadsheet->getActiveSheet()->setCellValue('H7', $data_pengiriman['kendaraan']);

		$spreadsheet->getActiveSheet()->mergeCells('P6:T6'); $spreadsheet->getActiveSheet()->setCellValue('U6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P6', 'Tanggal Delivery');
		$spreadsheet->getActiveSheet()->mergeCells('V6:Z6');
		$spreadsheet->getActiveSheet()->setCellValue('v6', $data_pengiriman['etd']);

		$spreadsheet->getActiveSheet()->mergeCells('P7:T7'); $spreadsheet->getActiveSheet()->setCellValue('U7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P7', 'Nomor');
		$spreadsheet->getActiveSheet()->mergeCells('V7:Z7');
		$spreadsheet->getActiveSheet()->setCellValue('v7', $data_pengiriman['no_spb']);
		
		// ==================================== END KOP SURAT =======================================================

		// ======================================= BODY 1 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A10', 'Item List');
		$spreadsheet->getActiveSheet()->mergeCells('A10:AC10');
		$spreadsheet->getActiveSheet()->mergeCells('A11:AC11');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->getStartColor()->setARGB('000000');
	

		$spreadsheet->getActiveSheet()->mergeCells('A13:H13'); $spreadsheet->getActiveSheet()->setCellValue('A13', 'No SPB');
		$spreadsheet->getActiveSheet()->mergeCells('I13:N13'); $spreadsheet->getActiveSheet()->setCellValue('I13', 'Nama Product');
		$spreadsheet->getActiveSheet()->mergeCells('O13:U13'); $spreadsheet->getActiveSheet()->setCellValue('O13', 'Code Box / Qty');
		$spreadsheet->getActiveSheet()->mergeCells('V13:AC13'); $spreadsheet->getActiveSheet()->setCellValue('V13', 'Dibuat Oleh');
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$startrow = 14;
		foreach ($item_pengiriman as $key => $value) {
				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow.':H'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow.'', $data_pengiriman['no_spb']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow.':N'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow.'', $value['product_name']);
				$spreadsheet->getActiveSheet()->mergeCells('O'.$startrow.':U'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('O'.$startrow.'', $value['code_box']);
				$spreadsheet->getActiveSheet()->mergeCells('V'.$startrow.':AC'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('V'.$startrow.'', $value['dibuat_oleh']);
				$startrow++;
		}

		// // ======================================= FOOTER ==================================================
		// //header
		
		$startbox = $startrow+2;
		$endbox = $startbox+4;
		$spreadsheet->getActiveSheet()->setCellValue('B'.$startbox.'', 'Quality');
		$spreadsheet->getActiveSheet()->mergeCells('B'.$startbox.':H'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('K'.$startbox.'', 'Driver');
		$spreadsheet->getActiveSheet()->mergeCells('K'.$startbox.':Q'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('T'.$startbox.'', 'Penerima');
		$spreadsheet->getActiveSheet()->mergeCells('T'.$startbox.':Z'.$endbox.'');

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);
		// ======================================= END FOOTER ==================================================

		// Set column widths
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(3);
		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('7')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(1);
		$spreadsheet->getActiveSheet()->getRowDimension('9')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(2);
		$spreadsheet->getActiveSheet()->getRowDimension('12')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('13')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('17')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('18')->setRowHeight(15);
		//=============================================================================================



		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/box_topup/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];

		$data_topup = R::getRow("SELECT a.*,
		b.`code_box`,
		c.`name`
		FROM `box_topup` a
		LEFT JOIN `box` b ON a.`id_box` = b.`id`
		LEFT JOIN `user` c ON a.`created_by` = c.`id`
		WHERE a.`id` = $id");

		$item_topup = R::getAll("SELECT a.*,c.`name` AS `product_detail_name`,c.`type`,c.`code` FROM `box_topup_detail` a
		LEFT JOIN `box_topup` b ON a.`id_box_topup` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		WHERE a.`id_box_topup` = $id");

		// print_r($data_topup);
		// print_r($item_topup);
		// die();
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		$styleThinBlackBorderOutline = [
			'borders' => [
				'outline' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => 'FF000000'],
				],
			],
		];

		// ======================================== Kop Surat ===================================================
		$spreadsheet->setActiveSheetIndex(0);

		// logo PMN
		$drawing = new Drawing();
		$drawing->setPath(__DIR__ . '/img/logopmn.png');
		$drawing->setHeight(70);
		$drawing->setCoordinates('A1');
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// Title
		$spreadsheet->getActiveSheet()->setCellValue('F1', 'PT Patriot Medika Nusantara');
		// style F1
			$spreadsheet->getActiveSheet()->mergeCells('F1:R1');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			
		$spreadsheet->getActiveSheet()->mergeCells('F2:R2');
		$spreadsheet->getActiveSheet()->mergeCells('F3:R3');
		$spreadsheet->getActiveSheet()->mergeCells('F4:R4');
		$spreadsheet->getActiveSheet()->setCellValue('F2', 'Jl. Kelapa dua, Tangerang - Banten 15810');
		$spreadsheet->getActiveSheet()->setCellValue('F3', 'Fax : (021) - 7257608');
		$spreadsheet->getActiveSheet()->setCellValue('F4', 'Telp : (021) - 29015270');
			// style D2 - D4
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		
		$spreadsheet->getActiveSheet()->setCellValue('S2', 'No : '.$data_topup['no_topup']);


		$spreadsheet->getActiveSheet()->mergeCells('B6:F6'); $spreadsheet->getActiveSheet()->setCellValue('G6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B6', 'DiTopup Oleh');
		$spreadsheet->getActiveSheet()->mergeCells('H6:N6');
		$spreadsheet->getActiveSheet()->setCellValue('H6', $data_topup['name']);
		
		$spreadsheet->getActiveSheet()->mergeCells('B7:F7'); $spreadsheet->getActiveSheet()->setCellValue('G7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B7', 'Code Box');
		$spreadsheet->getActiveSheet()->mergeCells('H7:N7');
		$spreadsheet->getActiveSheet()->setCellValue('H7', $data_topup['code_box']);

		// $spreadsheet->getActiveSheet()->mergeCells('B8:F8'); $spreadsheet->getActiveSheet()->setCellValue('G8', ':');
		// $spreadsheet->getActiveSheet()->setCellValue('B8', 'Tanggal');
		// $spreadsheet->getActiveSheet()->mergeCells('H8:N8');
		// $spreadsheet->getActiveSheet()->setCellValue('H8', $data_topup['created_date']);


		$spreadsheet->getActiveSheet()->mergeCells('P6:T6'); $spreadsheet->getActiveSheet()->setCellValue('U6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P6', 'Tanggal');
		$spreadsheet->getActiveSheet()->mergeCells('V6:Z6');
		$spreadsheet->getActiveSheet()->setCellValue('v6', $data_topup['created_date']);

		// $spreadsheet->getActiveSheet()->mergeCells('P7:T7'); $spreadsheet->getActiveSheet()->setCellValue('U7', ':');
		// $spreadsheet->getActiveSheet()->setCellValue('P7', 'No RO');
		// $spreadsheet->getActiveSheet()->mergeCells('V7:Z7');
		// $spreadsheet->getActiveSheet()->setCellValue('v7', $data_topup['no_mr']);
		
		// ==================================== END KOP SURAT =======================================================

		// ======================================= BODY 1 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A10', 'INSTRUMENT');
		$spreadsheet->getActiveSheet()->mergeCells('A10:AC10');
		$spreadsheet->getActiveSheet()->mergeCells('A11:AC11');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->getStartColor()->setARGB('000000');
	

		$spreadsheet->getActiveSheet()->mergeCells('A13:H13'); $spreadsheet->getActiveSheet()->setCellValue('A13', 'CODE');
		$spreadsheet->getActiveSheet()->mergeCells('I13:Y13'); $spreadsheet->getActiveSheet()->setCellValue('I13', 'ITEM NAME');
		$spreadsheet->getActiveSheet()->mergeCells('Z13:AC13'); $spreadsheet->getActiveSheet()->setCellValue('Z13', 'Qty');
		//$spreadsheet->getActiveSheet()->mergeCells('AB13:AC13'); $spreadsheet->getActiveSheet()->setCellValue('AB13', 'Std');
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$startrow = 14;
		foreach ($item_topup as $key => $value) {

			//print_r($value);
			if($value['type'] == 2){

				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow.':H'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow.'', $value['code']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow.':Y'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow.'', $value['product_detail_name']);
				$spreadsheet->getActiveSheet()->mergeCells('Z'.$startrow.':AA'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('Z'.$startrow.'', $value['quantity']);
				//$spreadsheet->getActiveSheet()->mergeCells('AB'.$startrow.':AC'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('AB'.$startrow.'', $value['quantity']);
				$startrow++;
			}
		}
		$merge2 = $startrow+1;
		$merge2_black = $merge2+1; $spreadsheet->getActiveSheet()->getRowDimension($merge2_black)->setRowHeight(2);

		$spreadsheet->getActiveSheet()->setCellValue('A'.$merge2.'', 'IMPLANT');
		$spreadsheet->getActiveSheet()->mergeCells('A'.$merge2.':AC'.$merge2.'');
		$spreadsheet->getActiveSheet()->mergeCells('A'.$merge2_black.':AC'.$merge2_black.'');
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.'')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2.':AC'.$merge2.'')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A'.$merge2_black.':AC'.$merge2_black.'')->getFill()->getStartColor()->setARGB('000000');

		$startrow2 = $merge2_black+2;

		foreach ($item_topup as $key => $value) {
			if($value['type'] == 1){

				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow2.':H'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow2.'', $value['code']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow2.':Y'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow2.'', $value['product_detail_name']);
				$spreadsheet->getActiveSheet()->mergeCells('Z'.$startrow2.':AA'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('Z'.$startrow2.'', $value['quantity']);
				//$spreadsheet->getActiveSheet()->mergeCells('AB'.$startrow2.':AC'.$startrow2.''); $spreadsheet->getActiveSheet()->setCellValue('AB'.$startrow2.'', $value['quantity']);
				$startrow2++;
			}
		}

		// // ======================================= FOOTER ==================================================
		//header
		
		$startbox = $startrow2+2;
		$endbox = $startbox+4;
		$spreadsheet->getActiveSheet()->setCellValue('B'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('B'.$startbox.':H'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('K'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('K'.$startbox.':Q'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('T'.$startbox.'', 'Requester');
		$spreadsheet->getActiveSheet()->mergeCells('T'.$startbox.':Z'.$endbox.'');

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':H'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('T'.$startbox.':Z'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);
		// ======================================= END FOOTER ==================================================

		// Set column widths
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(3);
		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('7')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('9')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(2);
		$spreadsheet->getActiveSheet()->getRowDimension('12')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('13')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('17')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('18')->setRowHeight(15);
		//=============================================================================================



		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/penggunaan/{id_ro}", function (Request $request, Response $response, $args){
	try {
		$id_ro = $args['id_ro'];

		$data_ro = R::getRow("SELECT a.*, 
		b.`name` AS `doctor_name`, 
		c.`name` AS `hospital_name`, 
		d.`name` AS `status_ro_name`,
		e.`name` AS `created_by_name`,
		f.`name` AS `stage_name`
		FROM `rencana_operasi` a
		LEFT JOIN `doctor` b ON a.`id_doctor` = b.`id`
		LEFT JOIN `hospital` c ON a.`id_hospital` = c.`id`
		LEFT JOIN `rencana_operasi_status` d ON a.`status_ro` = d.`id`
		LEFT JOIN `user` e ON a.`created_by` = e.`id`
		LEFT JOIN `rencana_operasi_stage` f ON a.`stage` = f.`id`
		WHERE a.`id` = $id_ro");

		$data_penggunaan = R::getAll("SELECT e.`name` AS `product_name`, a.*,b.`no_ro` ,c.`code_box`,d.`name` AS `product_detail_name`,d.`code` FROM `penggunaan_barang` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `box` c ON a.`id_box` = c.`id` 
		LEFT JOIN `product_detail` d ON a.`id_product_detail` = d.`id`
		LEFT JOIN `product` e ON c.`product` = e.`id`
		WHERE a.`id_ro` = $id_ro");

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		$styleThinBlackBorderOutline = [
			'borders' => [
				'outline' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => 'FF000000'],
				],
			],
		];

		// ======================================== Kop Surat ===================================================
		$spreadsheet->setActiveSheetIndex(0);

		// logo PMN
		$drawing = new Drawing();
		$drawing->setPath(__DIR__ . '/img/logopmn.png');
		$drawing->setHeight(70);
		$drawing->setCoordinates('A1');
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// Title
		$spreadsheet->getActiveSheet()->setCellValue('F1', 'PT Patriot Medika Nusantara');
		// style F1
			$spreadsheet->getActiveSheet()->mergeCells('F1:R1');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$spreadsheet->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			
		$spreadsheet->getActiveSheet()->mergeCells('F2:R2');
		$spreadsheet->getActiveSheet()->mergeCells('F3:R3');
		$spreadsheet->getActiveSheet()->mergeCells('F4:R4');
		$spreadsheet->getActiveSheet()->setCellValue('F2', 'Jl. Kelapa dua, Tangerang - Banten 15810');
		$spreadsheet->getActiveSheet()->setCellValue('F3', 'Fax : (021) - 7257608');
		$spreadsheet->getActiveSheet()->setCellValue('F4', 'Telp : (021) - 29015270');
			// style D2 - D4
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F2')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F3')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setName('Arial Narrow');
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->setSize(12);
			$spreadsheet->getActiveSheet()->getStyle('F4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		
		$spreadsheet->getActiveSheet()->setCellValue('S2', 'No : '.$data_ro['no_ro']);


		$spreadsheet->getActiveSheet()->mergeCells('B6:F6'); $spreadsheet->getActiveSheet()->setCellValue('G6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B6', 'Hospital');
		$spreadsheet->getActiveSheet()->mergeCells('H6:N6');
		$spreadsheet->getActiveSheet()->setCellValue('H6', $data_ro['hospital_name']);
		
		$spreadsheet->getActiveSheet()->mergeCells('B7:F7'); $spreadsheet->getActiveSheet()->setCellValue('G7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('B7', 'Dokter');
		$spreadsheet->getActiveSheet()->mergeCells('H7:N7');
		$spreadsheet->getActiveSheet()->setCellValue('H7', $data_ro['doctor_name']);

		$spreadsheet->getActiveSheet()->mergeCells('P6:T6'); $spreadsheet->getActiveSheet()->setCellValue('U6', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P6', 'Tanggal');
		$spreadsheet->getActiveSheet()->mergeCells('V6:Z6');
		$spreadsheet->getActiveSheet()->setCellValue('v6', $data_ro['date']);

		$spreadsheet->getActiveSheet()->mergeCells('P7:T7'); $spreadsheet->getActiveSheet()->setCellValue('U7', ':');
		$spreadsheet->getActiveSheet()->setCellValue('P7', 'pasien');
		$spreadsheet->getActiveSheet()->mergeCells('V7:Z7');
		$spreadsheet->getActiveSheet()->setCellValue('v7', $data_ro['pasien']);
		
		// ==================================== END KOP SURAT =======================================================

		// ======================================= BODY 1 ==================================================
		// header
		$spreadsheet->getActiveSheet()->setCellValue('A10', 'List Penggunaan Barang');
		$spreadsheet->getActiveSheet()->mergeCells('A10:AC10');
		$spreadsheet->getActiveSheet()->mergeCells('A11:AC11');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setName('Arial Narrow');
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setSize(13);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A10:AC10')->getFill()->getStartColor()->setARGB('afcebd');

		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->setFillType(Fill::FILL_SOLID);
		$spreadsheet->getActiveSheet()->getStyle('A11:AC11')->getFill()->getStartColor()->setARGB('000000');
	

		$spreadsheet->getActiveSheet()->mergeCells('A13:D13'); $spreadsheet->getActiveSheet()->setCellValue('A13', 'Product');
		$spreadsheet->getActiveSheet()->mergeCells('E13:H13'); $spreadsheet->getActiveSheet()->setCellValue('E13', 'Box');
		$spreadsheet->getActiveSheet()->mergeCells('I13:N13'); $spreadsheet->getActiveSheet()->setCellValue('I13', 'Kode');
		$spreadsheet->getActiveSheet()->mergeCells('O13:Y13'); $spreadsheet->getActiveSheet()->setCellValue('O13', 'Item');
		$spreadsheet->getActiveSheet()->mergeCells('Z13:AC13'); $spreadsheet->getActiveSheet()->setCellValue('Z13', 'Jumlah');
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('A13:AC13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

		$startrow = 14;
		foreach ($data_penggunaan as $key => $value) {
				$spreadsheet->getActiveSheet()->mergeCells('A'.$startrow.':D'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('A'.$startrow.'', $value['product_name']);
				$spreadsheet->getActiveSheet()->mergeCells('E'.$startrow.':H'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('E'.$startrow.'', $value['code_box']);
				$spreadsheet->getActiveSheet()->mergeCells('I'.$startrow.':N'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('I'.$startrow.'', $value['code']);
				$spreadsheet->getActiveSheet()->mergeCells('O'.$startrow.':Y'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('O'.$startrow.'', $value['product_detail_name']);
				$spreadsheet->getActiveSheet()->mergeCells('Z'.$startrow.':AC'.$startrow.''); $spreadsheet->getActiveSheet()->setCellValue('Z'.$startrow.'', $value['quantity']);
				$startrow++;
		}

		// // ======================================= FOOTER ==================================================
		// //header
		
		$startbox = $startrow+2;
		$endbox = $startbox+4;
		$spreadsheet->getActiveSheet()->setCellValue('B'.$startbox.'', 'Technical Support');
		$spreadsheet->getActiveSheet()->mergeCells('B'.$startbox.':K'.$endbox.'');

		// $spreadsheet->getActiveSheet()->setCellValue('K'.$startbox.'', 'Quality Control');
		// $spreadsheet->getActiveSheet()->mergeCells('K'.$startbox.':Q'.$endbox.'');

		$spreadsheet->getActiveSheet()->setCellValue('S'.$startbox.'', 'Quality Control');
		$spreadsheet->getActiveSheet()->mergeCells('S'.$startbox.':AB'.$endbox.'');

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':AC'.$endbox.'')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);

		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':K'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('B'.$startbox.':K'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		// $spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		// $spreadsheet->getActiveSheet()->getStyle('K'.$startbox.':Q'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);

		$spreadsheet->getActiveSheet()->getStyle('S'.$startbox.':AB'.$endbox.'')->applyFromArray($styleThinBlackBorderOutline);
		$spreadsheet->getActiveSheet()->getStyle('S'.$startbox.':AB'.$endbox.'')->getFill()->setFillType(Fill::FILL_SOLID);
		// ======================================= END FOOTER ==================================================

		// Set column widths
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(3);
		$spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(3);
		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('7')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(1);
		$spreadsheet->getActiveSheet()->getRowDimension('9')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(2);
		$spreadsheet->getActiveSheet()->getRowDimension('12')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('13')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('14')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('17')->setRowHeight(15);
		$spreadsheet->getActiveSheet()->getRowDimension('18')->setRowHeight(15);
		//=============================================================================================



		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/master/", function (Request $request, Response $response, $args){
	try {
		
		$data 	= R::getAll("SELECT 
		a.`id`,
		b.`no_ro`,
		n.`name` AS `created_by_name`,
		b.`date`,
		m.`name` AS `operasi_type`,
		f.`name` AS `hospital_name`,
		g.`name` AS `doctor_name`,
		b.`pasien`,
		b.`no_mr`,
		h.`name` AS `jenis_tagihan_name`,
		i.`name` AS `spine_anatomi`,
		j.`name` AS `case_name`,
		k.`name` AS `subcase_name`,
		l.`name` AS `ts_name`,
		d.`code_box`,
		e.`name` AS `product_name`,
		c.`name` AS `product_detail_name`,
		a.`quantity`,
		o.`no_spb`,
		p.`name` AS `courier_name`,
		o.`etd`,
		o.`eta`,
		q.`name` AS `kendaraan_name`,
		q.`nomor_polisi`,
		s.`name` AS `ditarik_oleh`,
		r.`etd` AS `ditarik_pada`,
		r.`eta` AS `tiba_pada`,
		b.`integrated`,
		t.`name` AS `stage_name`
		FROM `penggunaan_barang` a
		LEFT JOIN `rencana_operasi` b ON a.`id_ro` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `box` d ON a.`id_box` = d.`id`
		LEFT JOIN `product` e ON d.`product` = e.`id`
		LEFT JOIN `hospital` f ON b.`id_hospital` = f.`id`
		LEFT JOIN `doctor` g ON b.`id_doctor` = g.`id`
		LEFT JOIN `jenis_tagihan` h ON b.`jenis_tagihan` = h.`id`
		LEFT JOIN `spine_anatomi` i ON b.`id_spine_anatomi` = i.`id`
		LEFT JOIN `diagnosa_case` j ON b.`case` = j.`id`
		LEFT JOIN `diagnosa_subcase` k  ON b.`subcase` = k.`id`
		LEFT JOIN `user` l ON b.`technical_support` = l.`id`
		LEFT JOIN `rencana_operasi_type` m ON b.`type_ro` = m.`id`
		LEFT JOIN `user` n ON b.`created_by` = n.`id`
		LEFT JOIN `spb` o ON b.`id` = o.`id_ro`
		LEFT JOIN `user` p ON o.`courier` = p.`id`
		LEFT JOIN `kendaraan` q ON o.`no_kendaraan` = q.`id`
		LEFT JOIN `penarikan_barang` r ON r.`id_spb` = o.`id`
		LEFT JOIN `user` s ON r.`courier` = s.`id`
		LEFT JOIN `rencana_operasi_stage` t ON b.`stage` = t.`id`
		where b.`no_ro` is not null and b.`status_ro` = 1");

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();
		$spreadsheet->setActiveSheetIndex(0);

		foreach(range('B','AC') as $columnID) {
			$spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		$styleArray = [
			'borders' => [
				'inside' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];
		

		$spreadsheet->getActiveSheet()->setCellValue('A1', 'ID');
		$spreadsheet->getActiveSheet()->setCellValue('B1', 'No Ro');
		$spreadsheet->getActiveSheet()->setCellValue('C1', 'Dibuat Oleh');
		$spreadsheet->getActiveSheet()->setCellValue('D1', 'Tanggal');
		$spreadsheet->getActiveSheet()->setCellValue('E1', 'Type');
		$spreadsheet->getActiveSheet()->setCellValue('F1', 'RS');
		$spreadsheet->getActiveSheet()->setCellValue('G1', 'Dokter');
		$spreadsheet->getActiveSheet()->setCellValue('H1', 'Pasien');
		$spreadsheet->getActiveSheet()->setCellValue('I1', 'No MR');
		$spreadsheet->getActiveSheet()->setCellValue('J1', 'Jenis Tagihan');
		$spreadsheet->getActiveSheet()->setCellValue('K1', 'Spine Anatomi');
		$spreadsheet->getActiveSheet()->setCellValue('L1', 'Case');
		$spreadsheet->getActiveSheet()->setCellValue('M1', 'Subcase');
		$spreadsheet->getActiveSheet()->setCellValue('N1', 'TS');
		$spreadsheet->getActiveSheet()->setCellValue('O1', 'Code Box');
		$spreadsheet->getActiveSheet()->setCellValue('P1', 'Product');
		$spreadsheet->getActiveSheet()->setCellValue('Q1', 'Item');
		$spreadsheet->getActiveSheet()->setCellValue('R1', 'Quantity');
		$spreadsheet->getActiveSheet()->setCellValue('S1', 'No SPB');
		$spreadsheet->getActiveSheet()->setCellValue('T1', 'Kurir');
		$spreadsheet->getActiveSheet()->setCellValue('U1', 'ETD');
		$spreadsheet->getActiveSheet()->setCellValue('V1', 'ETA');
		$spreadsheet->getActiveSheet()->setCellValue('W1', 'kendaraan');
		$spreadsheet->getActiveSheet()->setCellValue('X1', 'No Polisi');
		$spreadsheet->getActiveSheet()->setCellValue('Y1', 'Ditarik Oleh');
		$spreadsheet->getActiveSheet()->setCellValue('Z1', 'Ditarik pada');
		$spreadsheet->getActiveSheet()->setCellValue('AA1', 'Tiba pada');
		$spreadsheet->getActiveSheet()->setCellValue('AB1', 'Integrated');
		$spreadsheet->getActiveSheet()->setCellValue('AC1', 'Progress');

		$spreadsheet->getActiveSheet()->getStyle('A1:AC1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('c1ff9e');

		$row = 2;
		foreach ($data as $key => $value) {
			$spreadsheet->getActiveSheet()->setCellValue('A'.$row, $value['id']);
			$spreadsheet->getActiveSheet()->setCellValue('B'.$row, $value['no_ro']);
			$spreadsheet->getActiveSheet()->setCellValue('C'.$row, $value['created_by_name']);
			$spreadsheet->getActiveSheet()->setCellValue('D'.$row, $value['date']);
			$spreadsheet->getActiveSheet()->setCellValue('E'.$row, $value['operasi_type']);
			$spreadsheet->getActiveSheet()->setCellValue('F'.$row, $value['hospital_name']);
			$spreadsheet->getActiveSheet()->setCellValue('G'.$row, $value['doctor_name']);
			$spreadsheet->getActiveSheet()->setCellValue('H'.$row, $value['pasien']);
			$spreadsheet->getActiveSheet()->setCellValue('I'.$row, $value['no_mr']);
			$spreadsheet->getActiveSheet()->setCellValue('J'.$row, $value['jenis_tagihan_name']);
			$spreadsheet->getActiveSheet()->setCellValue('K'.$row, $value['spine_anatomi']);
			$spreadsheet->getActiveSheet()->setCellValue('L'.$row, $value['case_name']);
			$spreadsheet->getActiveSheet()->setCellValue('M'.$row, $value['subcase_name']);
			$spreadsheet->getActiveSheet()->setCellValue('N'.$row, $value['ts_name']);
			$spreadsheet->getActiveSheet()->setCellValue('O'.$row, $value['code_box']);
			$spreadsheet->getActiveSheet()->setCellValue('P'.$row, $value['product_name']);
			$spreadsheet->getActiveSheet()->setCellValue('Q'.$row, $value['product_detail_name']);
			$spreadsheet->getActiveSheet()->setCellValue('R'.$row, $value['quantity']);
			$spreadsheet->getActiveSheet()->setCellValue('S'.$row, $value['no_spb']);
			$spreadsheet->getActiveSheet()->setCellValue('T'.$row, $value['courier_name']);
			$spreadsheet->getActiveSheet()->setCellValue('U'.$row, $value['etd']);
			$spreadsheet->getActiveSheet()->setCellValue('V'.$row, $value['eta']);
			$spreadsheet->getActiveSheet()->setCellValue('W'.$row, $value['kendaraan_name']);
			$spreadsheet->getActiveSheet()->setCellValue('X'.$row, $value['nomor_polisi']);
			$spreadsheet->getActiveSheet()->setCellValue('Y'.$row, $value['ditarik_oleh']);
			$spreadsheet->getActiveSheet()->setCellValue('Z'.$row, $value['ditarik_pada']);
			$spreadsheet->getActiveSheet()->setCellValue('AA'.$row, $value['tiba_pada']);
			$spreadsheet->getActiveSheet()->setCellValue('AB'.$row, $value['integrated']);
			$spreadsheet->getActiveSheet()->setCellValue('AC'.$row, $value['stage_name']);
			$row++;
		}

		// laporan penggunaan
		$lp = R::getAll("SELECT 
		b.`no_lp`,
		h.`name` AS `created_by_name`,
		b.`created_date`,
		g.`name` AS `hospital_name`,
		h.`name` AS `doctor_name`,
		b.`pasien`,
		b.`no_mr`,
		e.`code_box` AS `code_box`,
		d.`name` AS `product_name`,
		c.`name` AS `product_detail_name`,
		a.`quantity`,
		b.`integrated`
		FROM `laporan_pemakaian_detail` a
		LEFT JOIN `laporan_pemakaian` b ON a.`id_laporan_penggunaan` = b.`id`
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
		LEFT JOIN `product` d ON c.`id_product` = d.`id`
		LEFT JOIN `box` e ON a.`box` = e.`id`
		LEFT JOIN `doctor` f ON b.`id_dokter` = f.`id`
		LEFT JOIN `hospital` g ON b.`id_hospital` = g.`id`
		LEFT JOIN `user` h ON b.`created_by` = h.`id`");

		foreach ($lp as $key_lp => $value_lp) {
			$spreadsheet->getActiveSheet()->setCellValue('A'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('B'.$row, $value_lp['no_lp']);
			$spreadsheet->getActiveSheet()->setCellValue('C'.$row, $value_lp['created_by_name']);
			$spreadsheet->getActiveSheet()->setCellValue('D'.$row, $value_lp['created_date']);
			$spreadsheet->getActiveSheet()->setCellValue('E'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('F'.$row, $value_lp['hospital_name']);
			$spreadsheet->getActiveSheet()->setCellValue('G'.$row, $value_lp['doctor_name']);
			$spreadsheet->getActiveSheet()->setCellValue('H'.$row, $value_lp['pasien']);
			$spreadsheet->getActiveSheet()->setCellValue('I'.$row, $value_lp['no_mr']);
			$spreadsheet->getActiveSheet()->setCellValue('J'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('K'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('L'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('M'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('N'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('O'.$row, $value_lp['code_box']);
			$spreadsheet->getActiveSheet()->setCellValue('P'.$row, $value_lp['product_name']);
			$spreadsheet->getActiveSheet()->setCellValue('Q'.$row, $value_lp['product_detail_name']);
			$spreadsheet->getActiveSheet()->setCellValue('R'.$row, $value_lp['quantity']);
			$spreadsheet->getActiveSheet()->setCellValue('S'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('T'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('U'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('V'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('W'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('X'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('Y'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('Z'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('AA'.$row, '-');
			$spreadsheet->getActiveSheet()->setCellValue('AB'.$row, $value_lp['integrated']);

			$spreadsheet->getActiveSheet()->getStyle('B'.$row.':AC'.$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f4a742');
			$row++;
		}

		$spreadsheet->getActiveSheet()->getStyle('A1:AC'.$row)->applyFromArray($styleArray);

		// ===================================== CONFIG ===============================================
		// Set page orientation and size
		$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Create a new worksheet, after the default sheet
		$spreadsheet->createSheet();
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});
$app->run();