<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$user = R::getAll("SELECT a.*, b.`name` AS `level_name`, c.`name` AS `privilege_name` FROM `user` a 
		LEFT JOIN `user_level` b ON a.`level` = b.`id`
		LEFT JOIN `user_privilege_group` c ON a.`privilege` = c.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($user);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/privilege_list/{privilege_id}", function (Request $request, Response $response, $args){
	$privilege_id = $args['privilege_id'];
	try {
		$privilege = R::getAll("SELECT a.* FROM `user_privilege_access` a WHERE a.`id_user_privilege_group` = $privilege_id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($privilege);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$new_password 		= substr(uniqid(),6 ,5);
		$param				= $request->getParsedBody();
		$user				= R::xdispense( 'user' );
		$user->username 	= $param['username'];
		$user->password 	= password_hash($new_password, PASSWORD_BCRYPT, array('cost' => 10));
		$user->email		= $param['email'];
		$user->name			= $param['name'];
		$user->level		= $param['level'];
		$user->privilege	= $param['privilege'];
		$user->division		= $param['division'	];
		$user->parent		= $param['parent'];
		$id					= R::store( $user );
		
		sendmail($param['email'], $param['name'], '[Logistic System] New Account', template_email($param['name'] ,$param['email'], $new_password));
		
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'User has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param				= $request->getParsedBody();
		$user 				= R::load( 'user', $param['id'] );
		$user->name 		= $param['name'];
		$user->username 	= $param['username'];
		$user->email 		= $param['email'];
		$user->level		= $param['level'];
		$user->privilege	= $param['privilege'];
		$user->division 	= $param['division'];
		$user->parent 		= $param['parent'];
		$id 				= R::store( $user );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'User Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/forgot_password', function (Request $request, Response $response){
	try{
		$new_password 	= substr(uniqid(),6 ,5);
		$param			= $request->getParsedBody();
		$user 			= R::load( 'user', $param['id'] );
		$user->password = password_hash($new_password, PASSWORD_BCRYPT, array('cost' => 10));
		$id 			= R::store( $user );

		sendmail($param['email'], $param['name'], '[Logistic System] Forgot Password', template_email_forgot_password($param['name'] ,$param['email'], $new_password));
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'User Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$user_level 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user_level`");
		$privilege_list = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user_privilege_group`");
		$division_list 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user_division`");
		$parent_list 	= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `user`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'level'=>$user_level,
			'privilege'=>$privilege_list,
			'division'=>$division_list,
			'parent'=>$parent_list
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();