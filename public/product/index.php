<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$product = R::getAll("SELECT a.*,b.`name` AS `principle_name` FROM `product` a LEFT JOIN `principle` b ON a.`id_principle` = b.`id` ORDER BY a.`id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id = $args['id'];
		$product = R::getRow("SELECT a.*,b.`name` AS `principle_name` FROM `product` a LEFT JOIN `principle` b ON a.`id_principle` = b.`id` where a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($product);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$product					= R::xdispense( 'product' );
		$product->id_principle 		= $param['principle'];
		$product->name				= $param['name'];
		$id							= R::store( $product );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'product Has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$product 				= R::load( 'product', $param['id'] );
		$product->id_principle 	= $param['principle'];
		$product->name			= $param['name'];
		$id 						= R::store( $product );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'product List Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$principle = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `principle`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($principle);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->run();