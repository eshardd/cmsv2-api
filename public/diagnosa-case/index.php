<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$diagnosa_case 	= R::getAll("SELECT * FROM `diagnosa_case` ORDER BY `id` DESC");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withAddedHeader('Access-Control-Allow-Origin', '*')->withJson($diagnosa_case);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$diagnosa_case				= R::xdispense( 'diagnosa_case' );
		$diagnosa_case->name 		= $param['name'];
		$id					= R::store( $diagnosa_case );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param						= $request->getParsedBody();
		$diagnosa_case 				= R::load( 'diagnosa_case', $param['id'] );
		$diagnosa_case->name 		= $param['name'];
		$id 						= R::store( $diagnosa_case );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'Case Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->run();