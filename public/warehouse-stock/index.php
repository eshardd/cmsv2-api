<?php
include '../configuration/index.php';
include '../configuration/auth.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/all", function (Request $request, Response $response){
	try {
		$warehouse_stock = R::getAll("SELECT a.*, b.`name` AS `warehouse_name`,
		c.`id_product` AS `id_product`,
		c.`name` AS `product_detail_name`,
		c.`code` AS `product_detail_code`,
        d.`name` AS `product_name`,
        e.`name` AS `principle_name`
		FROM `warehouse_stock` a 
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id` 
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
        LEFT JOIN `product` d ON d.`id` = c.`id_product`
        LEFT JOIN `principle` e ON d.`id_principle` = e.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($warehouse_stock);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/pd_filter/{warehouse}/{product}", function (Request $request, Response $response, $args){
	try {
		$id_product = $args['product'];
		$id_warehouse = $args['warehouse'];
		$product_detail = R::getAll("SELECT a.*, b.`name` AS `product_name`,c.`name` AS `principle_name`,
		(SELECT (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = a.`id` AND `id_warehouse` = $id_warehouse) AS `sisa`
		FROM `product_detail` a
		LEFT JOIN `product` b ON a.`id_product` = b.`id`
		LEFT JOIN `principle` c ON b.`id_principle` = c.`id`
		WHERE a.`id_product` = $id_product");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($product_detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->get("/list", function (Request $request, Response $response){
	try {
		$list_topup = R::getAll("SELECT a.*,b.`name` AS `from`,c.`name` AS `to`,e.`name` AS `created_by_name` FROM `warehouse_topup` a 
		LEFT JOIN `warehouse` b ON a.`from` = b.`id`
		LEFT JOIN `warehouse` c ON a.`to` = c.`id`
		LEFT JOIN `user` e ON a.`created_by` = e.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($list_topup);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/wh/{wh_id}/{pd_id}", function (Request $request, Response $response, $args){
	try {
		$wh_id = $args['wh_id'];
		$pd_id = $args['pd_id'];
		$warehouse_stock = R::getAll("SELECT a.*, b.`name` AS `warehouse_name`,
		c.`id_product` AS `id_product`,
		c.`name` AS `product_detail_name`,
		c.`code` AS `product_detail_code`,
        d.`name` AS `product_name`,
        e.`name` AS `principle_name`
		FROM `warehouse_stock` a 
		LEFT JOIN `warehouse` b ON a.`id_warehouse` = b.`id` 
		LEFT JOIN `product_detail` c ON a.`id_product_detail` = c.`id`
        LEFT JOIN `product` d ON d.`id` = c.`id_product`
        LEFT JOIN `principle` e ON d.`id_principle` = e.`id`
        WHERE b.`id` = $wh_id");

		$sisa = R::getRow("SELECT SUM(`in`) AS `in` , SUM(`out`) as `out`, (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = $pd_id AND `id_warehouse` = $wh_id");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'stok'=>$warehouse_stock,
			'sisa'=>$sisa
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/wh_topup/{wh_pusat}/{wh_tujuan}/{pd_id}", function (Request $request, Response $response, $args){
	try {
		$wh_pusat_id = $args['wh_pusat'];
		$wh_tujuan_id = $args['wh_tujuan'];
		$pd_id = $args['pd_id'];

		$sisa_pusat = R::getRow("SELECT SUM(`in`) AS `in` , SUM(`out`) as `out`, (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = $pd_id AND `id_warehouse` = $wh_pusat_id");
		$sisa_tujuan = R::getRow("SELECT SUM(`in`) AS `in` , SUM(`out`) as `out`, (SUM(`in`)-SUM(`out`)) AS `sisa` FROM `warehouse_stock` WHERE `id_product_detail` = $pd_id AND `id_warehouse` = $wh_tujuan_id");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'sisa_pusat'=>$sisa_pusat,
			'sisa_tujuan'=>$sisa_tujuan,
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/add', function (Request $request, Response $response){
	try{

		$param									= $request->getParsedBody();

		if(isset($param['in'])){
			$in = $param['in'];
		}else{
			$in = 0;
		}

		if(isset($param['out'])){
			$out = $param['out'];
		}else{
			$out = 0;
		}

		$warehouse_stock						= R::xdispense( 'warehouse_stock' );
		$warehouse_stock->id_warehouse 			= $param['warehouse'];
		$warehouse_stock->id_product_detail		= $param['product_detail'];
		$warehouse_stock->in					= $in;
		$warehouse_stock->out					= $out;
		$warehouse_stock->description			= $param['description'];
		$warehouse_stock->created_date			= date("Y-m-d H:i:s");
		$warehouse_stock->created_by			= $param['id_user'];
		$id										= R::store( $warehouse_stock );
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'warehouse_stock has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/transfer', function (Request $request, Response $response){
	try{

		$param									= $request->getParsedBody();

		// kurangi stok gudang utama
		$warehouse_stock						= R::xdispense( 'warehouse_stock' );
		$warehouse_stock->id_warehouse 			= 1;
		$warehouse_stock->id_product_detail		= $param['product_detail'];
		$warehouse_stock->out					= $param['quantity'];
		$warehouse_stock->description			= '[system] transfer to Warehouse '.$param['transfer_to'];
		$warehouse_stock->created_date			= date("Y-m-d H:i:s");
		$warehouse_stock->created_by			= $param['id_user'];
		$id										= R::store( $warehouse_stock );

		// Tambah Stok gudang Tujuan
		$warehouse_stock						= R::xdispense( 'warehouse_stock' );
		$warehouse_stock->id_warehouse 			= $param['transfer_to'];
		$warehouse_stock->id_product_detail		= $param['product_detail'];
		$warehouse_stock->in					= $param['quantity'];
		$warehouse_stock->description			= '[system] Terima dari Warehouse Tangerang';
		$warehouse_stock->created_date			= date("Y-m-d H:i:s");
		$warehouse_stock->created_by			= $param['id_user'];
		$id										= R::store( $warehouse_stock );

		// add topup stock list
		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `rencana_operasi`");

		if ($ai['id'] == null) {
			$new_ai = 1;
		}else{
			$new_ai = $ai['id']+1;
		}

		$topup_stok_list 						= R::xdispense( 'warehouse_topup' );
		$topup_stok_list->no_topup				= 'PMN.WHTP/'.date('Ymd').'/'.$new_ai;
		$topup_stok_list->from 					= 1;
		$topup_stok_list->to 					= $param['transfer_to'];
		$topup_stok_list->id_product_detail		= $param['product_detail'];
		$topup_stok_list->quantity				= $param['quantity'];
		$topup_stok_list->description			= '[system] Terima dari Warehouse Tangerang';
		$topup_stok_list->created_by			= $param['id_user'];
		$topup_stok_list->created_date			= date("Y-m-d H:i:s");
		$id										= R::store( $topup_stok_list );

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'warehouse_stock has been inserted'));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->post('/topup_external', function (Request $request, Response $response){
	try{

		$param									= $request->getParsedBody();

		$ai = R::getRow("SELECT MAX(`id`) as `id` FROM `warehouse_topup`");

		if ($ai['id'] == null) {$new_ai = 1;}else{$new_ai = $ai['id']+1;}
		// insert to wh_topup

		$nomor = 'PMN.WHTP/'.date('Ymd').'/'.$new_ai;

		$topup 						= R::xdispense( 'warehouse_topup' );
		$topup->no_topup			= $nomor;
		$topup->from 				= $param['from'];
		$topup->to 					= $param['to'];
		$topup->description			= '[system] Terima dari Warehouse Tangerang';
		$topup->created_by			= $param['id_user'];
		$topup->created_date		= date("Y-m-d H:i:s");
		$id							= R::store( $topup );

		foreach ($param['data'] as $key => $value) {
			$topup_detail 						= R::xdispense( 'warehouse_topup_detail' );
			$topup_detail->id_warehouse_topup	= $id;
			$topup_detail->product_detail		= $value['product_detail'];
			$topup_detail->quantity 			= $value['quantity'];
			R::store( $topup_detail );

			// kurangi stok gudang utama
			$warehouse_stock						= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse 			= $param['from'];
			$warehouse_stock->id_product_detail		= $value['product_detail'];
			$warehouse_stock->out					= $value['quantity'];
			$warehouse_stock->description			= '[system] transfer to Warehouse '.$param['to'];
			$warehouse_stock->created_date			= date("Y-m-d H:i:s");
			$warehouse_stock->created_by			= $param['id_user'];
			R::store( $warehouse_stock );

			// Tambah Stok gudang Tujuan
			$warehouse_stock						= R::xdispense( 'warehouse_stock' );
			$warehouse_stock->id_warehouse 			= $param['to'];
			$warehouse_stock->id_product_detail		= $value['product_detail'];
			$warehouse_stock->in					= $value['quantity'];
			$warehouse_stock->description			= '[system] Terima dari Warehouse Tangerang';
			$warehouse_stock->created_date			= date("Y-m-d H:i:s");
			$warehouse_stock->created_by			= $param['id_user'];
			R::store( $warehouse_stock );

		}

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('message'=>'Topup Sukses : '.$nomor));
	}catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->put('/edit', function (Request $request, Response $response){
	try{
		$param									= $request->getParsedBody();
	
		$warehouse_stock 						= R::load( 'warehouse_stock', $param['id'] );
		$warehouse_stock->in					= $param['in'];
		$warehouse_stock->out					= $param['out'];
		$warehouse_stock->description			= $param['description'];
		$id 									= R::store( $warehouse_stock );

	return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('id'=>$id, 'message'=>'warehouse_stock Update success'));
	}
	catch(Exception $e){
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/dropdown", function (Request $request, Response $response){
	try {
		$warehouse = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `warehouse`");
		/*$principle = R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `principle`");
		$product = R::getAll("SELECT `id` AS `value`,id_principle, `name` as `label` FROM `product`");*/
		$product_detail = R::getAll("SELECT a.`id` AS `value`,a.`id_product`, CONCAT(c.`name`,' - ',b.`name`, ' - ', a.`name`) AS `label` FROM `product_detail` a
		left join `product` b on a.`id_product` = b.`id`
		left JOIN `principle` c on b.`id_principle` = c.`id`");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array('warehouse'=>$warehouse,'product_detail'=>$product_detail));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/form_dropdown", function (Request $request, Response $response){
	try {
		$warehouse 		= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `warehouse`");
		$principle 		= R::getAll("SELECT `id` AS `value`, `name` as `label` FROM `principle`");
		$product 		= R::getAll("SELECT `id` AS `value`, `name` as `label`,`id_principle` FROM `product`");
		$product_detail = R::getAll("SELECT a.`id` AS `value`,a.`id_product`, a.`name` AS `label` FROM `product_detail` a");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson(array(
			'warehouse'=>$warehouse,
			'principle'=>$principle,
			'product'=>$product,
			'product_detail'=>$product_detail
		));
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/product_detail_name/{id}", function (Request $request, Response $response, $args){
	$id = $args['id'];
	try {
		$name = R::getRow("SELECT a.* FROM `product_detail` a WHERE a.`id` = $id");
		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($name);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});

$app->get("/topup_detail/{id}", function (Request $request, Response $response, $args){
	try {
		$id_topup = $args['id']; 

		$detail = R::getAll("SELECT a.*,b.`no_topup` , c.`name` AS `product_detail_name` FROM `warehouse_topup_detail` a
		LEFT JOIN `warehouse_topup` b ON a.`id_warehouse_topup` = b.`id`
		LEFT JOIN `product_detail` c ON a.`product_detail` = c.`id`
		WHERE a.`id_warehouse_topup` = $id_topup");

		return $response->withStatus(200)->withHeader('Content-type', 'application/json')->withJson($detail);
	} catch (Exception $e) {
		return $response->withStatus(400)->write($e->getMessage());
	}
});


$app->run();